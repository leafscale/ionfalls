(*
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
#
{---[ File Info ]-------------------------------------------------------------}
{                                                                             }
{ Source File: ionthemer/colors.pas                                           }
{     Version: 1.00                                                           }
{   Author(s): Chris Tusa <chris.tusa@ionfalls.org>                           }
{ Description: Color Variables handler                                        }
{                                                                             }
{-----------------------------------------------------------------------------}
*)

unit colors;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

// Define TIONColor class type for color objects
// TODO: use property to retrieve name/ansi/ftl/hex?
type
  TIONColor = class(TObject)
  private
    Fname: String;  // Name referenced in Theme Editor
    Fansi: String;  // ANSI color string
    Fhex: String;   // Hexidecimal equivalent string
    Fftl: String;   // Variable name used in IONtemplate (FTL files)
    procedure SetName(Value: String);
    procedure SetAnsi(Value: String);
    procedure SetHex(Value: String);
    procedure SetFTL(Value: String);
  public
    constructor Create(Cname : String; Cansi : String; Cftl : String; Chex : String);
    property name: String read Fname write SetName;
    property ansi: String read Fansi write SetAnsi;
    property hex: String read Fhex write SetHex;
    property ftl: String read Fftl write SetFTL;

  end;

// Define the master type of Foreground Colors as a class
type
  TFGColorvars = class(TObject)
    public
    gray         : TIONColor;
    darkgray     : TIONColor;
    red          : TIONColor;
    green        : TIONColor;
    brown        : TIONColor;
    blue         : TIONColor;
    magenta      : TIONColor;
    cyan         : TIONColor;
    white        : TIONColor;
    brightred    : TIONColor;
    brightgreen  : TIONColor;
    brightyellow : TIONColor;
    brightblue   : TIONColor;
    brightmagenta: TIONColor;
    brightcyan   : TIONColor;
    black        : TIONColor;

    constructor Create();
    function get(byname: String) :TIONColor;
  end;

// Define the master type of Background Colors as a class
type
  TBGColorvars = class(TObject)
    on_black     : TIONColor;
    on_red       : TIONColor;
    on_green     : TIONColor;
    on_yellow    : TIONColor;
    on_blue      : TIONColor;
    on_magenta   : TIONColor;
    on_cyan      : TIONColor;
    on_white     : TIONColor;
    none         : TIONColor;
  public
    constructor Create();
    function get(byname: String) :TIONColor;
  end;

  // Define the master type of Text Effects as a class
  type
    TFXvars = class(TObject)
      norm     : TIONColor;
      blink    : TIONColor;
      rapid    : TIONColor;
      reset    : TIONColor;
    public
      constructor Create();
      function get(byname: String) :TIONColor;
    end;
implementation

// Constructor for TIONColor objects
constructor TIONColor.Create(Cname:string; Cansi:string; Cftl:string; Chex:string);
begin
  name := Cname;
  ansi := Cansi;
  hex  := Chex;
  ftl  := Cftl;
end;

procedure TIONColor.SetName(Value: String);
begin
  Fname := Value;
end;

procedure TIONColor.SetAnsi(Value: String);
begin
  Fansi := Value;
end;

procedure TIONColor.SetHex(Value: String);
begin
  Fhex := Value;
end;

procedure TIONColor.SetFTL(Value: String);
begin
  Fftl := Value;
end;


// Constructor for TFGColorvars objects
constructor TFGColorvars.Create;
begin
// Define each color as we want it.
  // BLACK
  self.black         := TIONColor.create('black', '\e[0;30m', '${color.black}', '#000000');
  self.darkgray      := TIONColor.create('darkgray', '\e[1;30m','${color.darkgray}','#333333');
  self.red           := TIONColor.create('red','\e[0;31m','${color.red}','#800000' );
  self.brightred     := TIONColor.create('brightred','\e[1;31m','${color.brightred}','#FF0000');
  self.green         := TIONColor.Create('green','\e[0;32m','${color.green}','#800000');
  self.brightgreen   := TIONColor.Create('brightgreen','\e[1;32m','${color.brightgreen}','#00ff00');
  self.brown         := TIONColor.Create('brown','\e[0;33m','${color.brown}','#999900');
  self.brightyellow  := TIONColor.Create('brightyellow','\e[1;33m','${color.brightyellow}','#FFFF00');
  self.blue          := TIONColor.Create('blue','\e[0;34m','${color.blue}','#000099');
  self.brightblue    := TIONColor.Create('brightblue','\e[1;34m','${color.brightblue}','#0000FF');
  self.magenta       := TIONColor.Create('magenta','\e[0;35m','${color.magenta}','#520066');
  self.brightmagenta := TIONColor.Create('brightmagenta','\e[1;35m','${color.brightmagenta}','#cc00ff');
  self.cyan          := TIONColor.Create('cyan','\e[0;36m','${color.cyan}','#008080');
  self.brightcyan    := TIONColor.Create('brightcyan','\e[1;36m','${color.brightcyan}','#1affff');
  self.gray          := TIONColor.Create('gray','\e[0;37m','${color.gray}','#666666');
  self.white         := TIONColor.Create('white','\e[1;37m','${color.white}','#FFFFFF');
end;

// Find and return a color object by name
function TFGColorvars.get(byname: String) : TIONColor;
begin
  case byname of
    'black'          :  result :=self.black;
    'darkgray'       :  result :=self.darkgray;
    'red'            :  result :=self.red;
    'brightred'      :  result :=self.brightred;
    'green'          :  result :=self.green;
    'brightgreen'    :  result :=self.brightgreen;
    'brown'          :  result :=self.brown;
    'brightyellow'   :  result :=self.brightyellow;
    'blue'           :  result :=self.blue;
    'brightblue'     :  result :=self.brightblue;
    'magenta'        :  result :=self.magenta;
    'brightmagenta'  :  result :=self.brightmagenta;
    'cyan'           :  result :=self.cyan;
    'brightcyan'     :  result :=self.brightcyan;
    'gray'           :  result :=self.gray;
    'white'          :  result :=self.white;
  end; // case
end; // function

// Constructor for TBGColorvars objects
constructor TBGColorvars.Create;
begin
// Define each color as we want it.
  self.on_black   := TIONColor.create('on_black','\e[0;40m','${color.on_black}','#000000');
  self.on_red     := TIONColor.create('on_red','\e[0;41m','${color.on_red}','#800000');
  self.on_green   := TIONColor.create('on_green','\e[0;42m','${color.on_green}','#800000');
  self.on_yellow  := TIONColor.create('on_yellow','\e[0;43m','${color.on_yellow}','#999900');
  self.on_blue    := TIONColor.create('on_blue','\e[0;44m','${color.on_blue}','#000099');
  self.on_magenta := TIONColor.create('on_magenta','\e[0;45m','${color.on_magenta}','#520066');
  self.on_cyan    := TIONColor.create('on_cyan','\e[0;46m','${color.on_cyan}','#008080');
  self.on_white   := TIONColor.Create('on_white','\e[0;47m','${color.on_white}','#DDDDDD');
  self.none       := TIONColor.Create('none','','','');
end;

// Find and return a BG color object by name
function TBGColorvars.get(byname: String) : TIONColor;
begin
  case byname of
    'on_black'          :  result :=self.on_black;
    'on_red'            :  result :=self.on_red;
    'on_green'          :  result :=self.on_green;
    'on_yellow'         :  result :=self.on_yellow;
    'on_blue'           :  result :=self.on_blue;
    'on_magenta'        :  result :=self.on_magenta;
    'on_cyan'           :  result :=self.on_cyan;
    'on_white'          :  result :=self.on_white;
    'none'              :  result :=self.none;
  end; // case
end; // function

// Constructor for TBGColorvars objects
constructor TFXvars.Create;
begin
// Define each color as we want it.
  self.norm   := TIONColor.create('norm', '\e[0m','${color.norm}','');
  self.blink  := TIONColor.create('blink','\e[5m','${color.blink}','');
  self.rapid  := TIONColor.create('rapid','\e[6m','${color.rapid}','');
  self.reset  := TIONColor.create('reset','\e[0m','${color.reset}','');

end;

// Find and return a BG color object by name
function TFXvars.get(byname: String) : TIONColor;
begin
  case byname of
    'norm'    :  result :=self.norm;
    'blink'   :  result :=self.blink;
    'rapid'   :  result :=self.rapid;
    'reset'   :  result :=self.reset;
  end; // case
end; // function

end.


(*
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: ionthemer/editor.pas
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfals.org>
# Description: 
#
#-----------------------------------------------------------------------------
*)
unit editor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, SynEdit, Forms, Controls,
  Graphics, Dialogs, ComCtrls, MaskEdit, ExtCtrls, StdCtrls, Menus,
  SynFacilHighlighter, SynEditKeyCmds, Types, colors, LCLType;


type

  { TFormIONThemeEditor }

  TFormIONThemeEditor = class(TForm)
    ButtonInsertEffect: TButton;
    ButtonInsertVariable: TButton;
    ButtonApplyColors: TButton;
    GroupBoxTools: TGroupBox;
    ImageListToolBar1: TImageList;
    OpenDialogFTLfile: TOpenDialog;
    RadioGroupEffects: TRadioGroup;
    RadioGroupBGcolor: TRadioGroup;
    RadioGroupFGcolor: TRadioGroup;
    StatusBar: TStatusBar;
    SynEditTheme: TSynEdit;
    ToolBar1: TToolBar;
    ToolButtonFileSave: TToolButton;
    ToolButtonFileOpen: TToolButton;
    procedure ButtonApplyColorsClick(Sender: TObject);
    procedure ButtonInsertEffectClick(Sender: TObject);
    procedure ButtonInsertVariableClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure RadioGroupBGcolorSelectionChanged(Sender: TObject);
    procedure RadioGroupEffectsSelectionChanged(Sender: TObject);
    procedure RadioGroupFGcolorSelectionChanged(Sender: TObject);
    procedure SynEditThemeChange(Sender: TObject);
    procedure SynEditThemeMouseWheel(Sender: TObject; Shift: TShiftState;
      WheelDelta: Integer; MousePos: TPoint; var Handled: Boolean);
    procedure ToolButtonFileOpenClick(Sender: TObject);
    procedure ToolButtonFileSaveClick(Sender: TObject);
    procedure UpdatePreview(Sender: TObject);
    procedure SynEditThemeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private
    FcurFGcolor : String;
    FcurBGcolor : String;
    FcurEffects : String;
    FcurFilename: String;
    FSynFTL: TSynFacilSyn;
    procedure SetCurFGcolor(Avalue:String);
    procedure SetCurBGcolor(Avalue:String);
    procedure SetCurEffects(Avalue:String);
    procedure SetCurFilename(Avalue:String);
    function FindXMLdefinition() : String;
  public
    FGColors: TFGColorvars;
    BGColors: TBGColorvars;
    FXVars  : TFXvars;
    property curFGcolor : String read FcurFGcolor write SetCurFGcolor;
    property curBGcolor : String read FcurBGcolor write SetCurBGcolor;
    property curEffects : String read FcurEffects write SetCurEffects;
    property curFilename: String read FcurFilename write SetCurFilename;
  end;

var
  FormIONThemeEditor: TFormIONThemeEditor;
  XMLdefinitionfile: String;

implementation

{$R *.lfm}

{ TFormIONThemeEditor }

// This function looks for the XML definition file that defines the syntax
// highlighter feature.
function TFormIONThemeEditor.FindXMLdefinition : String;
begin
  if FileExists('/opt/ionfalls/conf/iontpl.xml') then
    result:='/opt/ionfalls/conf/iontpl.xml';
  if FileExists('./iontpl.xml') then
    result:='./iontpl.xml';

end;

// onFormCreate
procedure TFormIONThemeEditor.FormCreate(Sender: TObject);
begin
  FGColors := TFGColorvars.create;  // Create the foreground color codes
  BGColors := TBGColorvars.create;  // Create the background color codes
  FXVars   := TFXvars.create;       // Create the text effect codes
  curFGcolor := 'gray';
  curBGcolor := 'none';
  curEffects := 'norm';
  FSynFTL := TSynFacilSyn.Create(Self);  // Load the syntax highlighter engine
  FSynFTL.LoadFromFile(self.FindXMLdefinition); // Find the XML syntax defs
  SynEditTheme.Highlighter := FSynFTL; // Set the engine to the editor component
  SynEditTheme.Lines.Text := 'Open a template file to begin.';
end;


// Setter for property CurFGcolor
procedure TFormIONThemeEditor.SetCurFGcolor(Avalue:String);
begin
  FcurFGcolor := Avalue;
end;


// Setter for property CurBGcolor
procedure TFormIONThemeEditor.SetCurBGcolor(Avalue:String);
begin
  FcurBGcolor := Avalue;
end;

// Setter for property CurEffects
procedure TFormIONThemeEditor.SetCurEffects(Avalue:String);
begin
  FcurEffects := Avalue;
end;

// Setter for property CurFilename
procedure TFormIONThemeEditor.SetCurFilename(Avalue:String);
begin
  FcurFilename := Avalue;
end;

(* SynEditThemeChange
What to do when the contents of the edit buffer are changed.
*)
procedure TFormIONThemeEditor.SynEditThemeChange(Sender: TObject);
begin
  StatusBar.Panels[1].Text := 'changed';
end;

procedure TFormIONThemeEditor.SynEditThemeMouseWheel(Sender: TObject;
  Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint;
  var Handled: Boolean);
begin

end;


(* ToolButtonFileOpenClick
This procedure opens a file and loads into the SynEdit buffer.
*)
procedure TFormIONThemeEditor.ToolButtonFileOpenClick(Sender: TObject);
var
  filename : String;
begin
  if OpenDialogFTLfile.Execute then
  begin
    filename := OpenDialogFTLfile.Filename;
    if FileExists( filename ) then
      begin
        SynEditTheme.Lines.LoadFromFile( filename );
        SynEditTheme.Highlighter := FSynFTL;
        SynEditTheme.Invalidate;
        SetCurFilename(filename);
        StatusBar.Panels[7].Text := self.curFilename; // set filename in statusbar
        StatusBar.Panels[1].Text := 'opened';
      end;
  end;
end;


(* ToolButtonFileSaveClick
This procedure saves the file currently loaded in the SynEdit buffer to disc.
*)
procedure TFormIONThemeEditor.ToolButtonFileSaveClick(Sender: TObject);
var
  filename : String;
begin
  filename := self.curFilename;
  if FileExists( filename ) then
    begin
      SynEditTheme.Lines.SaveToFile( filename );
      StatusBar.Panels[1].Text := 'saved';
    end;
end;


(* ButtonApplyColorsClick
This procedure inserts the appropriate IONTPL FTL string value at the
cursor location by using the curFGcolor and curBGcolor properties.
*)
procedure TFormIONThemeEditor.ButtonApplyColorsClick(Sender: TObject);
var
  fgcolor : TIONColor;
  bgcolor : TIONColor;
  colorstr : String; //String to insert with resolved color codes
begin
  fgcolor := FGColors.get(curFGcolor);
  bgcolor := BGColors.get(curBGcolor);
  //ShowMessage('FG='+fgcolor.name+'\nfg-ftl='+fgcolor.ftl+'\nBG='+bgcolor.name+'\nbg-ftl='+bgcolor.ftl);
  if bgcolor.name <> 'none' then
    colorstr := fgcolor.ftl + bgcolor.ftl
  else
    colorstr := fgcolor.ftl;

  SynEditTheme.InsertTextAtCaret(colorstr);
end;


(* ButtonInsertEffectClick
This procedure reads in the currently selected Effect value from the
radio button group and inserts the appropriate IONTPL FTL string value at the
cursor location
*)
procedure TFormIONThemeEditor.ButtonInsertEffectClick(Sender: TObject);
var
  fxvar : TIONColor;
begin
  fxvar := FXvars.get(cureffects);
  SynEditTheme.InsertTextAtCaret(fxvar.ftl);
end;

procedure TFormIONThemeEditor.ButtonInsertVariableClick(Sender: TObject);
var
  vartext : String;
begin
  vartext := '<#if var?has_content>${var}<#/if>';
  SynEditTheme.InsertTextAtCaret(vartext);
end;


// BGcolor: Sets the class property value from the Radio Group selection
procedure TFormIONThemeEditor.RadioGroupBGcolorSelectionChanged(Sender: TObject);
begin
  // read in the radio button index
  self.SetCurBGcolor(RadioGroupBGcolor.Items[RadioGroupBGcolor.ItemIndex]);
  StatusBar.Panels[5].Text := self.curBGcolor;
end;


// Effects: Sets the class property value from the Radio Group selection
procedure TFormIONThemeEditor.RadioGroupEffectsSelectionChanged(Sender: TObject);
begin
  self.SetCurEffects(RadioGroupEffects.Items[RadioGroupEffects.ItemIndex]);
end;


procedure TFormIONThemeEditor.RadioGroupFGcolorSelectionChanged(Sender: TObject);
begin
  self.SetCurFGcolor(RadioGroupFGcolor.Items[RadioGroupFGcolor.ItemIndex]);
  StatusBar.Panels[3].Text := self.curFGcolor;
end;


procedure TFormIONThemeEditor.UpdatePreview(Sender: TObject);
begin
  //ShowText( SynEditTheme.Lines.Text );
end;

procedure TFormIONThemeEditor.SynEditThemeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if (Shift = [ssCtrl]) then
  begin
    case Key of
    VK_C: SynEditTheme.CommandProcessor(TSynEditorCommand(ecCopy), ' ', nil);
    VK_V: SynEditTheme.CommandProcessor(TSynEditorCommand(ecPaste), ' ', nil);
    VK_X: SynEditTheme.CommandProcessor(TSynEditorCommand(ecCut), ' ', nil);
    end;
  end;
end;

end.


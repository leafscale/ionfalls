/*
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: ioneditor/IonEditor.java
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Full screen editor implementation written in Java (+ JLine3)
#
#-----------------------------------------------------------------------------
*/

/*
 * Copyright (c) 2002-2017, the original author or authors. (JLine3)
 *
 * This software is distributable under the BSD license. See the terms of the
 * BSD license in the documentation provided with this software.
 *
 * http://www.opensource.org/licenses/bsd-license.php
 */

package org.jline.builtins;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
//import java.util.regex.Matcher;
//import java.util.regex.Pattern;

import org.jline.keymap.BindingReader;
import org.jline.keymap.KeyMap;
import org.jline.terminal.Attributes;
import org.jline.terminal.Attributes.ControlChar;
import org.jline.terminal.Attributes.InputFlag;
import org.jline.terminal.Attributes.LocalFlag;
import org.jline.terminal.MouseEvent;
import org.jline.terminal.Size;
import org.jline.terminal.Terminal;
import org.jline.terminal.Terminal.Signal;
import org.jline.terminal.Terminal.SignalHandler;
import org.jline.utils.AttributedString;
import org.jline.utils.AttributedStringBuilder;
import org.jline.utils.AttributedStyle;
import org.jline.utils.Display;
import org.jline.utils.InfoCmp.Capability;
import org.mozilla.universalchardet.UniversalDetector;

import static org.jline.keymap.KeyMap.KEYMAP_LENGTH;
import static org.jline.keymap.KeyMap.alt;
import static org.jline.keymap.KeyMap.ctrl;
import static org.jline.keymap.KeyMap.del;
import static org.jline.keymap.KeyMap.key;

public class IonEditor {

    // Final fields
    protected final Terminal terminal;
    protected final Display display;
    protected final BindingReader bindingReader;
    protected final Size size;
    protected final Path root;

    // Keys
    protected KeyMap<Operation> keys;

    // Configuration
    public String title = "IonEditor 1.0";
    public boolean printLineNumbers = true;
    public boolean wrapping = true;
    public boolean smoothScrolling = true;
    public boolean mouseSupport = false;
    public boolean oneMoreLine = true;
    public boolean constantCursor;
    public boolean hideFilename = true;     // Hide the filename field in the TitleBar
    public int tabs = 4;

    // Buffers : files load into a Buffer (array for multi-file) with iterator index
    protected final List<Buffer> buffers = new ArrayList<>();
    protected int bufferIndex;
    protected Buffer buffer;

    // Edit buffer is a large string
    protected final StringBuilder editBuffer = new StringBuilder();

    // Message in the footer status bar
    protected String message;
    protected String editMessage;

    // nb
    protected int nbBindings = 0;

    // Menu ShortCut keys HashMap
    protected LinkedHashMap<String, String> shortcuts;

    protected enum WriteMode {
        WRITE
    }

    protected enum WriteFormat {
        UNIX,
        DOS,
        MAC
    }

    protected class Buffer {
        String file;
        Charset charset;
        WriteFormat format = WriteFormat.UNIX;
        List<String> lines;

        int firstLineToDisplay;
        int firstColumnToDisplay;
        int offsetInLineToDisplay;

        int line;
        List<LinkedList<Integer>> offsets = new ArrayList<>();
        int offsetInLine;
        int column;
        int wantedColumn;

        // Has the file been modified?
        boolean dirty;

        protected Buffer(String file) {
            this.file = file;
        }

        void open() throws IOException {
            if (lines != null) {
                return;
            }

            lines = new ArrayList<>();
            lines.add("");
            charset = Charset.defaultCharset();
            computeAllOffsets();

            if (file == null) {
                return;
            }

            Path path = root.resolve(file);
            if (Files.isDirectory(path)) {
                setMessage("\"" + file + "\" is a directory");
                return;
            }

            try (InputStream fis = Files.newInputStream(path))
            {
                read(fis);
            } catch (IOException e) {
                setMessage("Error reading " + file + ": " + e.getMessage());
            }
        }

        void open(InputStream is) throws IOException {
            if (lines != null) {
                return;
            }

            lines = new ArrayList<>();
            lines.add("");
            charset = Charset.defaultCharset();
            computeAllOffsets();

            read(is);
        }

        void read(InputStream fis) throws IOException {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            byte[] buffer = new byte[4096];
            int remaining;
            while ((remaining = fis.read(buffer)) > 0) {
                bos.write(buffer, 0, remaining);
            }
            byte[] bytes = bos.toByteArray();

            try {
                UniversalDetector detector = new UniversalDetector(null);
                detector.handleData(bytes, 0, bytes.length);
                detector.dataEnd();
                if (detector.getDetectedCharset() != null) {
                    charset = Charset.forName(detector.getDetectedCharset());
                }
            } catch (Throwable t) {
                // Ignore
            }

            // TODO: detect format, do not eat last newline
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new ByteArrayInputStream(bytes), charset))) {
                String line;
                lines.clear();
                while ((line = reader.readLine()) != null) {
                    lines.add(line);
                }
            }
            if (lines.isEmpty()) {
                lines.add("");
            }
            computeAllOffsets();
            moveToChar(0);
        }

        void insert(String insert) {
            String text = lines.get(line);
            int pos = offsetInLine + column;
            insert = insert.replaceAll("\r\n", "\n");
            insert = insert.replaceAll("\r", "\n");
            String mod;
            if (pos == text.length()) {
                mod = text + insert;
            } else {
                mod = text.substring(0, pos) + insert + text.substring(pos);
            }
            List<String> ins = new ArrayList<>();
            int last = 0;
            int idx = mod.indexOf('\n', last);
            while (idx >= 0) {
                ins.add(mod.substring(last, idx));
                last = idx + 1;
                idx = mod.indexOf('\n', last);
            }
            ins.add(mod.substring(last));
            lines.set(line, ins.get(0));
            offsets.set(line, computeOffsets(ins.get(0)));
            for (int i = 1; i < ins.size(); i++) {
                ++line;
                lines.add(line, ins.get(i));
                offsets.add(line, computeOffsets(ins.get(i)));
            }
            moveToChar(ins.get(ins.size() - 1).length() - (text.length() - pos));
            dirty = true;
        }

        void computeAllOffsets() {
            offsets.clear();
            for (String text : lines) {
                offsets.add(computeOffsets(text));
            }
        }

        LinkedList<Integer> computeOffsets(String text) {
            int width = size.getColumns() - (printLineNumbers ? 8 : 0);
            LinkedList<Integer> offsets = new LinkedList<>();
            offsets.add(0);
            int last = 0;
            int prevword = 0;
            boolean inspace = false;
            for (int i = 0; i < text.length(); i++) {
                if (isBreakable(text.charAt(i))) {
                    inspace = true;
                } else if (inspace) {
                    prevword = i;
                    inspace = false;
                }
                if (i == last + width - 1) {
                    if (prevword == last) {
                        prevword = i;
                    }
                    offsets.add(prevword);
                    last = prevword;
                }
            }
            return offsets;
        }

        boolean isBreakable(char ch) {
            return ch == ' ';
        }

        void moveToChar(int pos) {
            offsetInLine = prevLineOffset(line, pos + 1).get();
            column = pos - offsetInLine;
        }

        void delete(int count) {
            while (--count >= 0 && moveRight(1) && backspace(1));
        }

        boolean backspace(int count) {
            while (count > 0) {
                String text = lines.get(line);
                int pos = offsetInLine + column;
                if (pos == 0) {
                    if (line == 0) {
                        bof();
                        return false;
                    }
                    String prev = lines.get(--line);
                    lines.set(line, prev + text);
                    offsets.set(line, computeOffsets(prev + text));
                    moveToChar(length(prev, tabs));
                    lines.remove(line + 1);
                    offsets.remove(line + 1);
                    count--;
                    dirty = true;
                } else {
                    int nb = Math.min(pos, count);
                    text = text.substring(0, pos - nb) + text.substring(pos);
                    lines.set(line, text);
                    offsets.set(line, computeOffsets(text));
                    moveToChar(offsetInLine + column - nb);
                    count -= nb;
                    dirty = true;
                }
            }
            return true;
        }

        boolean moveLeft(int chars) {
            boolean ret = true;
            while (--chars >= 0) {
                if (offsetInLine + column > 0) {
                    moveToChar(offsetInLine + column - 1);
                } else if (line > 0) {
                    line--;
                    moveToChar(length(getLine(line), tabs));
                } else {
                    bof();
                    ret = false;
                    break;
                }
            }
            wantedColumn = column;
            ensureCursorVisible();
            return ret;
        }

        boolean moveRight(int chars) {
            boolean ret = true;
            while (--chars >= 0) {
                int len = length(getLine(line), tabs);
                if (offsetInLine + column + 1 <= len) {
                    moveToChar(offsetInLine + column + 1);
                } else if (getLine(line + 1) != null) {
                    line++;
                    offsetInLine = 0;
                    column = 0;
                } else {
                    eof();
                    ret = false;
                    break;
                }
            }
            wantedColumn = column;
            ensureCursorVisible();
            return ret;
        }

        void moveDown(int lines) {
            cursorDown(lines);
            ensureCursorVisible();
        }

        void moveUp(int lines) {
            cursorUp(lines);
            ensureCursorVisible();
        }

        private Optional<Integer> prevLineOffset(int line, int offsetInLine) {
            if (line >= offsets.size()) {
                return Optional.empty();
            }
            Iterator<Integer> it = offsets.get(line).descendingIterator();
            while (it.hasNext()) {
                int off = it.next();
                if (off < offsetInLine) {
                    return Optional.of(off);
                }
            }
            return Optional.empty();
        }

        private Optional<Integer> nextLineOffset(int line, int offsetInLine) {
            if (line >= offsets.size()) {
                return Optional.empty();
            }
            return offsets.get(line).stream()
                    .filter(o -> o > offsetInLine)
                    .findFirst();
        }

        void moveDisplayDown(int lines) {
            int height = size.getRows() - computeHeader().size() - computeFooter().size();
            // Adjust cursor
            while (--lines >= 0) {
                int lastLineToDisplay = firstLineToDisplay;
                if (firstColumnToDisplay > 0 || !wrapping) {
                    lastLineToDisplay += height - 1;
                } else {
                    int off = offsetInLineToDisplay;
                    for (int l = 0; l < height - 1; l++) {
                        Optional<Integer> next = nextLineOffset(lastLineToDisplay, off);
                        if (next.isPresent()) {
                            off = next.get();
                        } else {
                            off = 0;
                            lastLineToDisplay++;
                        }
                    }
                }
                if (getLine(lastLineToDisplay) == null) {
                    eof();
                    return;
                }
                Optional<Integer> next = nextLineOffset(firstLineToDisplay, offsetInLineToDisplay);
                if (next.isPresent()) {
                    offsetInLineToDisplay = next.get();
                } else {
                    offsetInLineToDisplay = 0;
                    firstLineToDisplay++;
                }
            }
        }

        void moveDisplayUp(int lines) {
            int width = size.getColumns() - (printLineNumbers ? 8 : 0);
            while (--lines >= 0) {
                if (offsetInLineToDisplay > 0) {
                    offsetInLineToDisplay = Math.max(0, offsetInLineToDisplay - (width - 1));
                } else if (firstLineToDisplay > 0) {
                    firstLineToDisplay--;
                    offsetInLineToDisplay = prevLineOffset(firstLineToDisplay, Integer.MAX_VALUE).get();
                } else {
                    bof();
                    return;
                }
            }
        }

        private void cursorDown(int lines) {
            // Adjust cursor
            while (--lines >= 0) {
                if (firstColumnToDisplay > 0 || !wrapping) {
                    if (getLine(line + 1) != null) {
                        line++;
                        offsetInLine = 0;
                        column = Math.min(getLine(line).length(), wantedColumn);
                    } else {
                        bof();
                        break;
                    }
                } else {
                    String txt = getLine(line);
                    Optional<Integer> off = nextLineOffset(line, offsetInLine);
                    if (off.isPresent()) {
                        offsetInLine = off.get();
                    } else if (getLine(line + 1) == null) {
                        eof();
                        break;
                    } else {
                        line++;
                        offsetInLine = 0;
                        txt = getLine(line);
                    }
                    String curLine = txt;
                    int next = nextLineOffset(line, offsetInLine).orElseGet(curLine::length);
                    column = Math.min(wantedColumn, next - offsetInLine);
                }
            }
        }

        private void cursorUp(int lines) {
            while (--lines >= 0) {
                if (firstColumnToDisplay > 0 || !wrapping) {
                    if (line > 0) {
                        line--;
                        column = Math.min(length(getLine(line), tabs) - offsetInLine, wantedColumn);
                    } else {
                        bof();
                        break;
                    }
                } else {
                    Optional<Integer> prev = prevLineOffset(line, offsetInLine);
                    if (prev.isPresent()) {
                        offsetInLine = prev.get();
                    } else if (line > 0) {
                        line--;
                        offsetInLine = prevLineOffset(line, Integer.MAX_VALUE).get();
                        int next = nextLineOffset(line, offsetInLine).orElse(getLine(line).length());
                        column = Math.min(wantedColumn, next - offsetInLine);
                    } else {
                        bof();
                        break;
                    }
                }
            }
        }

        void ensureCursorVisible() {
            List<AttributedString> header = computeHeader();
            int rwidth = size.getColumns();
            int height = size.getRows() - header.size() - computeFooter().size();

            while (line < firstLineToDisplay
                    || line == firstLineToDisplay && offsetInLine < offsetInLineToDisplay) {
                moveDisplayUp(smoothScrolling ? 1 : height / 2);
            }

            while (true) {
                int cursor = header.size() * size.getColumns() + (printLineNumbers ? 8 : 0);
                int cur = firstLineToDisplay;
                int off = offsetInLineToDisplay;
                while (true) {
                    if (cur < line || off < offsetInLine) {
                        if (firstColumnToDisplay > 0 || !wrapping) {
                            cursor += rwidth;
                            cur++;
                        } else {
                            cursor += rwidth;
                            Optional<Integer> next = nextLineOffset(cur, off);
                            if (next.isPresent()) {
                                off = next.get();
                            } else {
                                cur++;
                                off = 0;
                            }
                        }
                    } else if (cur == line) {
                        cursor += column;
                        break;
                    } else {
                        throw new IllegalStateException();
                    }
                }
                if (cursor >= (height + header.size()) * rwidth) {
                    moveDisplayDown(smoothScrolling ? 1 : height / 2);
                } else {
                    break;
                }
            }
        }

        void eof() {
        }

        void bof() {
        }

        void resetDisplay() {
            int width = size.getColumns() - (printLineNumbers ? 8 : 0);
            column = offsetInLine + column;
            offsetInLine = (column / width) * (width - 1);
            column = column - offsetInLine;
        }

        String getLine(int line) {
            return line < lines.size() ? lines.get(line) : null;
        }

        String getTitle() {
            return file != null ? "File: " + file : "New Buffer";
        }

        List<AttributedString> computeHeader() {
        AttributedStyle headstyle = AttributedStyle.DEFAULT.background(AttributedStyle.BLUE).foreground(AttributedStyle.CYAN + AttributedStyle.BRIGHT);

            String left = IonEditor.this.getTitle();
            String middle = null;
            String right = dirty ? "Unsaved " : "        ";
            int width = size.getColumns();
            int mstart = 2 + left.length() + 1;
            int mend = width - 2 - 8;
            if (hideFilename != true) {            
              if (file == null) {
                  middle = "New Message";
              } else {
                  int max = mend - mstart;
                  String src = file;
                  if ("File: ".length() + src.length() > max) {
                      int lastSep = src.lastIndexOf('/');
                      if (lastSep > 0) {
                          String p1 = src.substring(lastSep);
                          String p0 = src.substring(0, lastSep);
                          while (p0.startsWith(".")) {
                              p0 = p0.substring(1);
                          }
                          int nb = max - p1.length() - "File: ...".length();
                          int cut;
                          cut = Math.max(0, Math.min(p0.length(), p0.length() - nb));
                          middle = "File: ..." + p0.substring(cut, p0.length()) + p1;
                      }
                      if (middle == null || middle.length() > max) {
                          left = null;
                          max = mend - 2;
                          int nb = max - "File: ...".length();
                          int cut = Math.max(0, Math.min(src.length(), src.length() - nb));
                          middle = "File: ..." + src.substring(cut, src.length());
                          if (middle.length() > max) {
                              middle = middle.substring(0, max);
                          }
                      }
                  } else {
                      middle = "File: " + src;
                  }
              }
            } else {
                middle = "";
            } // endif hideFilename == false
            
            int pos = 0;
            AttributedStringBuilder sb = new AttributedStringBuilder();
            // sb.style(AttributedStyle.INVERSE);
            sb.style(headstyle);
            sb.append("  ");
            pos += 2;

            if (left != null) {
                sb.append(left);
                pos += left.length();
                sb.append(" ");
                pos += 1;
                for (int i = 1; i < (size.getColumns() - middle.length()) / 2 - left.length() - 1 - 2; i++) {
                    sb.append(" ");
                    pos++;
                }
            }
            sb.append(middle);
            pos += middle.length();
            while (pos < width - 8 - 2) {
                sb.append(" ");
                pos++;
            }
            sb.append(right);
            sb.append("  \n");
            if (oneMoreLine) {
                return Collections.singletonList(sb.toAttributedString());
            } else {
                return Arrays.asList(sb.toAttributedString(), new AttributedString("\n"));
            }
        }

        List<AttributedString> getDisplayedLines(int nbLines) {
            AttributedStyle s = AttributedStyle.DEFAULT.foreground(AttributedStyle.BLACK + AttributedStyle.BRIGHT);
            AttributedString cut = new AttributedString("…", s);
            AttributedString ret = new AttributedString("↩", s);

            List<AttributedString> newLines = new ArrayList<>();
            int rwidth = size.getColumns();
            int width = rwidth - (printLineNumbers ? 8 : 0);
            int curLine = firstLineToDisplay;
            int curOffset = offsetInLineToDisplay;
            int prevLine = -1;
            for (int terminalLine = 0; terminalLine < nbLines; terminalLine++) {
                AttributedStringBuilder line = new AttributedStringBuilder().tabs(tabs);
                if (printLineNumbers && curLine < lines.size()) {
                    line.style(s);
                    if (curLine != prevLine) {
                        line.append(String.format("%7d ", curLine + 1));
                    } else {
                        line.append("      ‧ ");
                    }
                    line.style(AttributedStyle.DEFAULT);
                    prevLine = curLine;
                }
                if (curLine >= lines.size()) {
                    // Nothing to do
                } else if (firstColumnToDisplay > 0 || !wrapping) {
                    AttributedString disp = new AttributedString(getLine(curLine));
                    disp = disp.columnSubSequence(firstColumnToDisplay, Integer.MAX_VALUE);
                    if (disp.columnLength() >= width) {
                        line.append(disp.columnSubSequence(0, width - cut.columnLength()));
                        line.append(cut);
                    } else {
                        line.append(disp);
                    }
                    curLine++;
                } else {
                    Optional<Integer> nextOffset = nextLineOffset(curLine, curOffset);
                    if (nextOffset.isPresent()) {
                        AttributedString disp = new AttributedString(getLine(curLine));
                        line.append(disp.columnSubSequence(curOffset, nextOffset.get()));
                        line.append(ret);
                        curOffset = nextOffset.get();
                    } else {
                        AttributedString disp = new AttributedString(getLine(curLine));
                        line.append(disp.columnSubSequence(curOffset, Integer.MAX_VALUE));
                        curLine++;
                        curOffset = 0;
                    }
                }
                line.append('\n');
                newLines.add(line.toAttributedString());
            }
            return newLines;
        }

        public void moveTo(int x, int y) {
            if (printLineNumbers) {
                x = Math.max(x - 8, 0);
            }
            line = firstLineToDisplay;
            offsetInLine = offsetInLineToDisplay;
            wantedColumn = x;
            cursorDown(y);
        }

        public int getDisplayedCursor() {
            int rwidth = size.getColumns() + 1;
            int cursor = (printLineNumbers ? 8 : 0);
            int cur = firstLineToDisplay;
            int off = offsetInLineToDisplay;
            while (true) {
                if (cur < line || off < offsetInLine) {
                    if (firstColumnToDisplay > 0 || !wrapping) {
                        cursor += rwidth;
                        cur++;
                    } else {
                        cursor += rwidth;
                        Optional<Integer> next = nextLineOffset(cur, off);
                        if (next.isPresent()) {
                            off = next.get();
                        } else {
                            cur++;
                            off = 0;
                        }
                    }
                } else if (cur == line) {
                    cursor += column;
                    break;
                } else {
                    throw new IllegalStateException();
                }
            }
            return cursor;
        }

        char getCurrentChar() {
            String str = lines.get(line);
            if (column + offsetInLine < str.length()) {
                return str.charAt(column + offsetInLine);
            } else if (line < lines.size() - 1) {
                return '\n';
            } else {
                return 0;
            }
        }

        @SuppressWarnings("StatementWithEmptyBody")
        public void prevWord() {
            while (Character.isAlphabetic(getCurrentChar())
                    && moveLeft(1));
            while (!Character.isAlphabetic(getCurrentChar())
                    && moveLeft(1));
            while (Character.isAlphabetic(getCurrentChar())
                    && moveLeft(1));
            moveRight(1);
        }

        @SuppressWarnings("StatementWithEmptyBody")
        public void nextWord() {
            while (Character.isAlphabetic(getCurrentChar())
                    && moveRight(1));
            while (!Character.isAlphabetic(getCurrentChar())
                    && moveRight(1));
        }

        public void beginningOfLine() {
            column = offsetInLine = 0;
            wantedColumn = 0;
        }

        public void endOfLine() {
            column = length(lines.get(line), tabs);
            int width = size.getColumns() - (printLineNumbers ? 8 : 0);
            offsetInLine = (column / width) * (width - 1);
            column = column - offsetInLine;
            wantedColumn = column;
        }

        public void prevPage() {
            int height = size.getRows() - computeHeader().size() - computeFooter().size();
            scrollUp(height - 2);
        }

        public void nextPage() {
            int height = size.getRows() - computeHeader().size() - computeFooter().size();
            scrollDown(height - 2);
        }

        public void scrollUp(int lines) {
            cursorUp(lines);
            moveDisplayUp(lines);
        }

        public void scrollDown(int lines) {
            cursorDown(lines);
            moveDisplayDown(lines);
        }

        public void firstLine() {
            line = 0;
            offsetInLine = column = 0;
            ensureCursorVisible();
        }

        public void lastLine() {
            line = lines.size() - 1;
            offsetInLine = column = 0;
            ensureCursorVisible();
        }


    private int length(String line, int tabs) {
            return new AttributedStringBuilder().tabs(tabs).append(line).columnLength();
        }
    }

    public IonEditor(Terminal terminal, File root) {
        this(terminal, root.toPath());
    }

    public IonEditor(Terminal terminal, Path root) {
        this.terminal = terminal;
        this.root = root;
        this.display = new Display(terminal, true);
        this.bindingReader = new BindingReader(terminal.reader());
        this.size = new Size();
        bindKeys();
    }

    public void open(String... files) throws IOException {
        open(Arrays.asList(files));
    }

    public void open(List<String> files) throws IOException {
        for (String file : files) {
            buffers.add(new Buffer(file));
        }
    }

    public int run() throws IOException {
        if (buffers.isEmpty()) {
            buffers.add(new Buffer(null));
        }
        buffer = buffers.get(bufferIndex);

        Attributes attributes = terminal.getAttributes();
        Attributes newAttr = new Attributes(attributes);
        newAttr.setLocalFlags(EnumSet.of(LocalFlag.ICANON, LocalFlag.ECHO, LocalFlag.IEXTEN), false);
        newAttr.setInputFlags(EnumSet.of(InputFlag.IXON, InputFlag.ICRNL, InputFlag.INLCR), false);
        newAttr.setControlChar(ControlChar.VMIN, 1);
        newAttr.setControlChar(ControlChar.VTIME, 0);
        newAttr.setControlChar(ControlChar.VINTR, 0);
        terminal.setAttributes(newAttr);
        terminal.puts(Capability.enter_ca_mode);
        terminal.puts(Capability.keypad_xmit);
        if (mouseSupport) {
            terminal.trackMouse(Terminal.MouseTracking.Normal);
        }

        this.shortcuts = standardShortcuts();

        SignalHandler prevHandler = null;
        try {
            buffer.open();

            size.copy(terminal.getSize());
            display.clear();
            display.reset();
            display.resize(size.getRows(), size.getColumns());
            prevHandler = terminal.handle(Signal.WINCH, this::handle);

            display();

            while (true) {
                Operation op;
                switch (op = readOperation(keys)) {
                    case QUIT:
                      // 0 Cancel
                      // 1 Yes
                      // 2 NO
                        int quitcode = quit();
                        if (quitcode > 1) { return quitcode; }             
                        //if (quit()) {
                        //    return;
                        //}
                        break;
                    case WRITE:
                        //write();
                        if (save(editBuffer.toString())) {                          
                          return 1;
                        }
                        //if (quit()) { return 0; }                     
                        break;
                    case UP:
                        buffer.moveUp(1);
                        break;
                    case DOWN:
                        buffer.moveDown(1);
                        break;
                    case LEFT:
                        buffer.moveLeft(1);
                        break;
                    case RIGHT:
                        buffer.moveRight(1);
                        break;
                    case INSERT:
                        buffer.insert(bindingReader.getLastBinding());
                        break;
                    case BACKSPACE:
                        buffer.backspace(1);
                        break;
                    case DELETE:
                        buffer.delete(1);
                        break;
                    case WRAP:
                        wrap();
                        break;                        
                    case NUMBERS:
                        numbers();
                        break;
                    case SMOOTH_SCROLLING:
                        smoothScrolling();
                        break;
                    case MOUSE_SUPPORT:
                        mouseSupport();
                        break;
                    case ONE_MORE_LINE:
                        oneMoreLine();
                        break;
                    case CLEAR_SCREEN:
                        clearScreen();
                        break;
//                    case PREV_BUFFER:
//                        prevBuffer();
//                        break;
//                    case NEXT_BUFFER:
//                        nextBuffer();
//                        break;
                    case CUR_POS:
                        curPos();
                        break;
                    case PREV_WORD:
                        buffer.prevWord();
                        break;
                    case NEXT_WORD:
                        buffer.nextWord();
                        break;
                    case BEGINNING_OF_LINE:
                        buffer.beginningOfLine();
                        break;
                    case END_OF_LINE:
                        buffer.endOfLine();
                        break;
                    case FIRST_LINE:
                        buffer.firstLine();
                        break;
                    case LAST_LINE:
                        buffer.lastLine();
                        break;
                    case PREV_PAGE:
                        buffer.prevPage();
                        break;
                    case NEXT_PAGE:
                        buffer.nextPage();
                        break;
                    case SCROLL_UP:
                        buffer.scrollUp(1);
                        break;
                    case SCROLL_DOWN:
                        buffer.scrollDown(1);
                        break;
                    case HELP:
                        help("ioneditor-help.txt");
                        break;
                    case MOUSE_EVENT:
                        mouseEvent();
                        break;
                    default:
                        setMessage("Unsupported " + op.name().toLowerCase().replace('_', '-'));
                        break;
                }
                display();
            }
        } finally {
            if (mouseSupport) {
                terminal.trackMouse(Terminal.MouseTracking.Off);
            }
            terminal.puts(Capability.exit_ca_mode);
            terminal.puts(Capability.keypad_local);
            terminal.flush();
            terminal.setAttributes(attributes);
            terminal.handle(Signal.WINCH, prevHandler);
        }
    }

    private Operation readOperation(KeyMap<Operation> keymap) {
        while (true) {
            Operation op = bindingReader.readBinding(keymap);
            if (op == Operation.DO_LOWER_CASE) {
                bindingReader.runMacro(bindingReader.getLastBinding().toLowerCase());
            } else {
                return op;
            }
        }
    }

    private boolean save(String name) throws IOException {
        Path orgPath = buffer.file != null ? root.resolve(buffer.file) : null;
        Path newPath = root.resolve(name);
        boolean isSame = orgPath != null && Files.isSameFile(orgPath, newPath);
        if (!isSame && Files.exists(Paths.get(name))) {
            Operation op = getYNC("File exists, OVERWRITE ? ");
            if (op != Operation.YES) {
                return false;
            }
        }
        Path t = Files.createTempFile(newPath.getParent(), "ionedit-", ".tmp");
        try (OutputStream os = Files.newOutputStream(t, StandardOpenOption.WRITE,
                                                        StandardOpenOption.TRUNCATE_EXISTING,
                                                        StandardOpenOption.CREATE)) {
            Writer w = new OutputStreamWriter(os, buffer.charset);
            for (int i = 0; i < buffer.lines.size(); i++) {
                if (i > 0) {
                    switch (buffer.format) {
                        case UNIX:
                            w.write("\n");
                            break;
                        case DOS:
                            w.write("\r\n");
                            break;
                        case MAC:
                            w.write("\r");
                            break;
                    }
                }
                w.write(buffer.lines.get(i));
            }
            w.flush();
            Files.move(t, newPath, StandardCopyOption.REPLACE_EXISTING);
            buffer.file = name;
            buffer.dirty = false;
            setMessage("Wrote " + buffer.lines.size() + " lines");
            return true;
        } catch (IOException e) {
            setMessage("Error writing " + name + ": " + e.toString());
            return false;
        } finally {
            Files.deleteIfExists(t);
        }
    }

    private Operation getYNC(String message) {
        String oldEditMessage = editMessage;
        String oldEditBuffer = editBuffer.toString();
        LinkedHashMap<String, String> oldShortcuts = shortcuts;
        try {
            editMessage = message;
            editBuffer.setLength(0);
            KeyMap<Operation> yncKeyMap = new KeyMap<>();
            yncKeyMap.bind(Operation.YES, "y", "Y");
            yncKeyMap.bind(Operation.NO, "n", "N");
            yncKeyMap.bind(Operation.CANCEL, ctrl('C'), "C", "c");
            shortcuts = new LinkedHashMap<>();
            shortcuts.put(" Y ", "Yes");
            shortcuts.put(" N ", "No");
            shortcuts.put(" C ", "Cancel");
            display();
            return readOperation(yncKeyMap);
        } finally {
            editMessage = oldEditMessage;
            editBuffer.append(oldEditBuffer);
            shortcuts = oldShortcuts;
        }
    }

    private String getWriteMessage() {
        StringBuilder sb = new StringBuilder();
        sb.append("Filename to write ");
        switch (buffer.format) {
            case UNIX:
                break;
            case DOS:
                sb.append(" [DOS Format]");
                break;
            case MAC:
                sb.append(" [Mac Format]");
                break;
        }
        sb.append(": ");
        return sb.toString();
    }

    private LinkedHashMap<String, String> writeShortcuts() {
        LinkedHashMap<String, String> s = new LinkedHashMap<>();
        s.put("CTRL-C", "Cancel");
        return s;
    }

    private LinkedHashMap<String, String> helpShortcuts() {
        LinkedHashMap<String, String> s = new LinkedHashMap<>();
        s.put("CTRL-Q", "Back to Editor");
        return s;
    }

    private LinkedHashMap<String, String> standardShortcuts() {
        LinkedHashMap<String, String> s = new LinkedHashMap<>();
        s.put("CTRL-S", "Save");        
        s.put("CTRL-P", "Position");
        s.put("CTRL-H", "Help");
        s.put("CTRL-Q", "Quit");
        return s;
    }

    void help(String helpfile) {
        Buffer org = this.buffer;
        Buffer newBuf = new Buffer(null);
        try (InputStream is = getClass().getResourceAsStream(helpfile)) {
            newBuf.open(is);
        } catch (IOException e) {
            setMessage("ERROR: Unable to read help file.");
            return;
        }
        LinkedHashMap<String, String> oldShortcuts = this.shortcuts;
        this.shortcuts = helpShortcuts();
        boolean oldWrapping = this.wrapping;
        boolean oldPrintLineNumbers = this.printLineNumbers;
        boolean oldConstantCursor = this.constantCursor;
        this.wrapping = true;
        this.printLineNumbers = false;
        this.constantCursor = false;
        this.buffer = newBuf;
        try {
            this.message = null;
            terminal.puts(Capability.cursor_invisible);
            clearScreen();
            display();
            while (true) {
                switch (readOperation(keys)) {
                    case QUIT:
                        clearScreen();
                        return;                        
                    case FIRST_LINE:
                        buffer.firstLine();
                        break;
                    case LAST_LINE:
                        buffer.lastLine();
                        break;
                    case PREV_PAGE:
                        buffer.prevPage();
                        break;
                    case NEXT_PAGE:
                        buffer.nextPage();
                        break;
                    case UP:
                        buffer.scrollUp(1);
                        break;
                    case DOWN:
                        buffer.scrollDown(1);
                        break;
                    case CLEAR_SCREEN:
                        clearScreen();
                        break;
                    case MOUSE_EVENT:
                        mouseEvent();
                        break;
                }
                display();
            }
        } finally {
            this.buffer = org;
            this.wrapping = oldWrapping;
            this.printLineNumbers = oldPrintLineNumbers;
            this.constantCursor = oldConstantCursor;
            this.shortcuts = oldShortcuts;
            terminal.puts(Capability.cursor_visible);
        }
    }

    String computeCurPos() {
        int chari = 0;
        int chart = 0;
        for (int i = 0; i < buffer.lines.size(); i++) {
            int l = buffer.lines.get(i).length() + 1;
            if (i < buffer.line) {
                chari += l;
            } else if (i == buffer.line) {
                chari += buffer.offsetInLine + buffer.column;
            }
            chart += l;
        }

        StringBuilder sb = new StringBuilder();
        sb.append("line ");
        sb.append(buffer.line + 1);
        sb.append("/");
        sb.append(buffer.lines.size());
        sb.append(" (");
        sb.append(Math.round((100.0 * buffer.line) / buffer.lines.size()));
        sb.append("%), ");
        sb.append("col ");
        sb.append(buffer.offsetInLine + buffer.column + 1);
        sb.append("/");
        sb.append(buffer.lines.get(buffer.line).length() + 1);
        sb.append(" (");
        if (buffer.lines.get(buffer.line).length() > 0) {
            sb.append(Math.round((100.0 * (buffer.offsetInLine + buffer.column))
                    / (buffer.lines.get(buffer.line).length())));
        } else {
            sb.append("100");
        }
        sb.append("%), ");
        sb.append("char ");
        sb.append(chari + 1);
        sb.append("/");
        sb.append(chart);
        sb.append(" (");
        sb.append(Math.round((100.0 * chari) / chart));
        sb.append("%)");
        return sb.toString();
    }

    void curPos() {
        setMessage(computeCurPos());
    }
/*
    void prevBuffer() throws IOException {
        if (buffers.size() > 1) {
            bufferIndex = (bufferIndex + buffers.size() - 1) % buffers.size();
            buffer = buffers.get(bufferIndex);
            setMessage("Switched to " + buffer.getTitle());
            buffer.open();
            display.clear();
        } else {
            setMessage("No more open file buffers");
        }
    }

    void nextBuffer() throws IOException {
        if (buffers.size() > 1) {
            bufferIndex = (bufferIndex + 1) % buffers.size();
            buffer = buffers.get(bufferIndex);
            setMessage("Switched to " + buffer.getTitle());
            buffer.open();
            display.clear();
        } else {
            setMessage("No more open file buffers");
        }
    }
*/
    void setMessage(String message) {
        this.message = message;
        this.nbBindings = 25;
    }

    int quit() throws IOException {
        if (buffer.dirty) {
            Operation op = getYNC("Save before quit?  [C]ancel,  [Y]es,  [N]o  :: ");
            switch (op) {
                case CANCEL:
                    return 0;
                case NO:
                    break;
                case YES:
                    if (!save(editBuffer.toString())) {
                        this.shortcuts = standardShortcuts();
                        return 1;
                    }
            }
        }
        buffers.remove(bufferIndex);
        if (bufferIndex == buffers.size() && bufferIndex > 0) {
            bufferIndex = buffers.size() - 1;
        }
        if (buffers.isEmpty()) {
            buffer = null;
            return 2;
        } else {
            buffer = buffers.get(bufferIndex);
            buffer.open();
            display.clear();
            setMessage("Switched to " + buffer.getTitle());
            return 0;
        }
    }

    void numbers() {
        printLineNumbers = !printLineNumbers;
        resetDisplay();
        setMessage("Lines numbering " + (printLineNumbers ? "enabled" : "disabled"));
    }

    void smoothScrolling() {
        smoothScrolling = !smoothScrolling;
        setMessage("Smooth scrolling " + (smoothScrolling ? "enabled" : "disabled"));
    }

    void mouseSupport() throws IOException {
        mouseSupport = !mouseSupport;
        setMessage("Mouse support " + (mouseSupport ? "enabled" : "disabled"));
        terminal.trackMouse(mouseSupport ? Terminal.MouseTracking.Normal : Terminal.MouseTracking.Off);
    }

    void constantCursor() {
        constantCursor = !constantCursor;
        setMessage("Constant cursor position display " + (constantCursor ? "enabled" : "disabled"));
    }

    void oneMoreLine() {
        oneMoreLine = !oneMoreLine;
        setMessage("Use of one more line for editing " + (oneMoreLine ? "enabled" : "disabled"));
    }

    void wrap() {
        wrapping = !wrapping;
        resetDisplay();
        setMessage("Lines wrapping " + (wrapping ? "enabled" : "disabled"));
    }

    void clearScreen() {
        resetDisplay();
    }

    void mouseEvent() {
        MouseEvent event = terminal.readMouseEvent();
        if (event.getModifiers().isEmpty() && event.getType() == MouseEvent.Type.Released
                && event.getButton() == MouseEvent.Button.Button1) {
            int x = event.getX();
            int y = event.getY();
            int hdr = buffer.computeHeader().size();
            int ftr = computeFooter().size();
            if (y < hdr) {
                // nothing
            } else if (y < size.getRows() - ftr) {
                buffer.moveTo(x, y - hdr);
            } else {
                int cols = (shortcuts.size() + 1) / 2;
                int cw = size.getColumns() / cols;
                int l = y - (size.getRows() - ftr) - 1;
                int si = l * cols +  x / cw;
                String shortcut = null;
                Iterator<String> it = shortcuts.keySet().iterator();
                while (si-- >= 0 && it.hasNext()) { shortcut = it.next(); }
                if (shortcut != null) {
                    shortcut = shortcut.replaceAll("M-", "\\\\E");
                    String seq = KeyMap.translate(shortcut);
                    bindingReader.runMacro(seq);
                }
            }
        }
        else if (event.getType() == MouseEvent.Type.Wheel) {
            if (event.getButton() == MouseEvent.Button.WheelDown) {
                buffer.moveDown(1);
            } else if (event.getButton() == MouseEvent.Button.WheelUp) {
                buffer.moveUp(1);
            }
        }
    }

    public String getTitle() {
        return title;
    }


    void resetDisplay() {
        display.clear();
        display.resize(size.getRows(), size.getColumns());
        for (Buffer buffer : buffers) {
            buffer.resetDisplay();
        }
    }

    synchronized void display() {
        if (nbBindings > 0) {
            if (--nbBindings == 0) {
                message = null;
            }
        }

        List<AttributedString> header = buffer.computeHeader();
        List<AttributedString> footer = computeFooter();

        int nbLines = size.getRows() - header.size() - footer.size();
        List<AttributedString> newLines = buffer.getDisplayedLines(nbLines);
        newLines.addAll(0, header);
        newLines.addAll(footer);

        // Compute cursor position
        int cursor;
        if (editMessage != null) {
            cursor = editMessage.length() + editBuffer.length();
            cursor = size.cursorPos(size.getRows() - footer.size(), cursor);
        } else {
            cursor = size.cursorPos(header.size(),
                                    buffer.getDisplayedCursor());
        }
        display.update(newLines, cursor);
    }

    protected List<AttributedString> computeFooter() {
        List<AttributedString> footer = new ArrayList<>();

        AttributedStyle cmdstyle = AttributedStyle.DEFAULT.background(AttributedStyle.CYAN).foreground(AttributedStyle.YELLOW + AttributedStyle.BRIGHT);
        AttributedStyle messagestyle1 = AttributedStyle.DEFAULT.background(AttributedStyle.GREEN).foreground(AttributedStyle.WHITE + AttributedStyle.BRIGHT);
        AttributedStyle messagestyle2 = AttributedStyle.DEFAULT.background(AttributedStyle.MAGENTA).foreground(AttributedStyle.WHITE + AttributedStyle.BRIGHT);
        AttributedStyle messagestyle3 = AttributedStyle.DEFAULT.background(AttributedStyle.BLUE).foreground(AttributedStyle.WHITE + AttributedStyle.BRIGHT);

        
        if (editMessage != null) {
            AttributedStringBuilder sb = new AttributedStringBuilder();
            //sb.style(AttributedStyle.INVERSE);
            sb.style(messagestyle2);
            sb.append(editMessage);
            sb.append(editBuffer);
            for (int i = editMessage.length() + editBuffer.length(); i < size.getColumns(); i++) {
                sb.append(' ');
            }
            sb.append('\n');
            footer.add(sb.toAttributedString());
        } else if (message != null || constantCursor) {
            int rwidth = size.getColumns();
            String text = " --> " + (message == null ? computeCurPos() : message) + " ";
            int len = text.length();
            AttributedStringBuilder sb = new AttributedStringBuilder();
//            for (int i = 0; i < (rwidth - len) / 2; i++) {
//                sb.style(messagestyle3);
//                sb.append(' ');
//            }
            sb.style(messagestyle3);
            sb.append(text);
            for (int i = text.length() + editBuffer.length(); i < size.getColumns(); i++) {
                sb.append(' ');
            }

            sb.append('\n');
            footer.add(sb.toAttributedString());
        } else {
            // if no message, display a blank bar
            AttributedStringBuilder sb = new AttributedStringBuilder();
            //sb.style(AttributedStyle.INVERSE);
            sb.style(messagestyle3);
            sb.append(editBuffer);
            for (int i = editBuffer.length(); i < size.getColumns(); i++) {
                sb.append(' ');
            }
            sb.append('\n');
            footer.add(sb.toAttributedString());
        }

        Iterator<Entry<String, String>> sit = shortcuts.entrySet().iterator();
        int cols = (shortcuts.size() + 1) / 2;
        int cw = (size.getColumns() - 1) / cols;
        int rem = (size.getColumns() - 1) % cols;
        for (int l = 0; l < 2; l++) {
            AttributedStringBuilder sb = new AttributedStringBuilder();
            for (int c = 0; c < cols; c++) {
                Map.Entry<String, String> entry = sit.hasNext() ? sit.next() : null;
                String key = entry != null ? entry.getKey() : "";
                String val = entry != null ? entry.getValue() : "";
                //sb.style(AttributedStyle.INVERSE);
                sb.style(cmdstyle);
                sb.append(key);
                sb.style(AttributedStyle.DEFAULT);
                sb.append(" ");
                int nb = cw - key.length() - 1 + (c < rem ? 1 : 0);
                if (val.length() > nb) {
                    sb.append(val.substring(0, nb));
                } else {
                    sb.append(val);
                    if (c < cols - 1) {
                        for (int i = 0; i < nb - val.length(); i++) {
                            sb.append(" ");
                        }
                    }
                }
            }
            sb.append('\n');
            footer.add(sb.toAttributedString());
        }

        return footer;
    }

    protected void handle(Signal signal) {
        if (buffer != null) {
            size.copy(terminal.getSize());
            buffer.computeAllOffsets();
            buffer.moveToChar(buffer.offsetInLine + buffer.column);
            resetDisplay();
            display();
        }
    }

    protected void bindKeys() {
        keys = new KeyMap<>();
        keys.setUnicode(Operation.INSERT);

        for (char i = 32; i < KEYMAP_LENGTH; i++) {
            keys.bind(Operation.INSERT, Character.toString(i));
        }
        keys.bind(Operation.BACKSPACE, del());
        for (char i = 'A'; i <= 'Z'; i++) {
            keys.bind(Operation.DO_LOWER_CASE, alt(i));
        }

        // Help
        keys.bind(Operation.HELP, ctrl('H'), key(terminal, Capability.key_f1));
        // Save and exit
        keys.bind(Operation.WRITE, ctrl('S'), key(terminal, Capability.key_f2));
        // Quit (ask to save or abandon)
        keys.bind(Operation.QUIT, ctrl('Q'), key(terminal, Capability.key_f9));
        // Current Cursor Position
        keys.bind(Operation.CUR_POS, ctrl('P'), key(terminal, Capability.key_f8));
        // Force screen refresh
        keys.bind(Operation.CLEAR_SCREEN, ctrl('R'));
        // Toggle Line Numbers
        keys.bind(Operation.NUMBERS, ctrl('n'));

        // Cursor Navigation
        keys.bind(Operation.UP, key(terminal, Capability.key_up));
        keys.bind(Operation.DOWN, key(terminal, Capability.key_down));
        keys.bind(Operation.RIGHT, key(terminal, Capability.key_right));
        keys.bind(Operation.LEFT, key(terminal, Capability.key_left));
        keys.bind(Operation.NEXT_PAGE, key(terminal, Capability.key_npage));
        keys.bind(Operation.PREV_PAGE, key(terminal, Capability.key_ppage));
        keys.bind(Operation.BEGINNING_OF_LINE, key(terminal, Capability.key_home));
        keys.bind(Operation.END_OF_LINE, key(terminal, Capability.key_end));
        keys.bind(Operation.NEXT_WORD, ctrl(']'), ctrl('}'));
        keys.bind(Operation.PREV_WORD, ctrl('['), ctrl('{'));
        keys.bind(Operation.FIRST_LINE, key(terminal, Capability.key_shome));
        keys.bind(Operation.LAST_LINE, key(terminal, Capability.key_send));
        keys.bind(Operation.SCROLL_UP, alt('-'), alt('_'));
        keys.bind(Operation.SCROLL_DOWN, alt('+'), alt('='));
        keys.bind(Operation.INSERT, ctrl('I'), ctrl('M'));
      
        keys.bind(Operation.MOUSE_EVENT, key(terminal, Capability.key_mouse));
        
    }

    protected enum Operation {
        DO_LOWER_CASE,

        QUIT,
        WRITE,
        HELP,

        NUMBERS,
        SMOOTH_SCROLLING,
        MOUSE_SUPPORT,
        ONE_MORE_LINE,
        CLEAR_SCREEN,

        UP,
        DOWN,
        LEFT,
        RIGHT,

        INSERT,
        BACKSPACE,
        WRAP,

        NEXT_PAGE,
        PREV_PAGE,
        SCROLL_UP,
        SCROLL_DOWN,
        NEXT_WORD,
        PREV_WORD,
        BEGINNING_OF_LINE,
        END_OF_LINE,
        FIRST_LINE,
        LAST_LINE,
        CUR_POS,
        CANCEL,
        YES,
        NO,
        DELETE,

        CUT,
        REPLACE,
        MARK,
        COPY,
        BEGINNING_OF_PARAGRAPH,
        END_OF_PARAGRAPH,
        COUNT,
        SMART_HOME_KEY,
        TABS_TO_SPACE,
        UNCUT,
//        MAC_FORMAT,
//        DOS_FORMAT,

        MOUSE_EVENT
    }

}

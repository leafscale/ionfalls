
===< IonEditor >===============================================================

IonFalls Message Editor is a simple text editor for composing messages. 
_______________________________________________________________________________

MAIN FUNCTIONS
 
 ( CTRL-H ) or (F1)  :  Display this help text
 ( CTRL-S ) or (F2)  :  Save the message and quit
 ( CTRL-P ) or (F8)  :  Display the current coordinates of the cursor
 ( CTRL-Q ) or (F9)  :  Quit the Editor  (option to abort or save)
 ( CTRL-N )          :  Toggle line numbers displayed in left margin
 ( CTRL-R )          :  Refresh display (for buggy terminals)
_______________________________________________________________________________

NAVIGATION KEYS

 ( Arrow Keys )      :  Move the cursor one character in the arrow direction.
 ( Page UP    )      :  Move the cursor up to previous page
 ( Page DOWN  )      :  Move the cursor down to next page
 ( HOME       )      :  Jump cursor to start of current line
 ( END        )      :  Jump cursor to end of current line
 ( SHIFT-HOME )      :  Jump cursor to start line of the message
 ( SHIFT-END  )      :  Jump cursor to end line of the message
 ( CTRL-]     )      :  Skip forward to the start of the next word
 ( CTRL-[     )      :  Skip back to the start of the previous word
 ( ALT -      )      :  Scroll Down
 ( ALT +      )      :  Scroll Up
_______________________________________________________________________________

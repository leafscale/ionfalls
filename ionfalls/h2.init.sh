#!/bin/bash
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /h2.init.sh
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: BASH Shell script to start/stop H2 database.
#
#-----------------------------------------------------------------------------
#

# The install path or symlink to the Telegard installation bbs directory
IONFALLS_HOME=/opt/ionfalls

H2_VERSION=1.4.197
H2_HOME=$IONFALLS_HOME/bbs/class
H2_JAR=$H2_HOME/h2-$H2_VERSION.jar


echo Using Embedded Database Engine in $IONFALLS_HOME
case $1 in
  start) # Starts only the database server
    cd $H2_HOME
    echo "Starting H2 Database Server"
    java -cp $H2_JAR org.h2.tools.Server -tcp -baseDir $IONFALLS_HOME/bbs/db &
  ;;
  all)  # Starts ALL services
    cd $H2_HOME
    echo "Starting H2 Database Server"
    java -cp $H2_JAR org.h2.tools.Server -tcpDaemon -baseDir $IONFALLS_HOME/bbs/db &
  ;;	  
  stop) # Stops the server
    echo "Shutting down H2 Database Server"
    java -cp $H2_JAR org.h2.tools.Server -tcpShutdown "tcp://localhost"
  ;;
  web) # Starts only the web admin interface
    echo "Starting H2 Web Server Admin Tool :: [CTRL-C] to terminate..."
    java -cp $H2_JAR org.h2.tools.Server -web -baseDir $IONFALLS_HOME/bbs/db
    #echo "NOTE: The webserver administration tool must be"
    #echo "      shutdown from either the browser interface"
    #echo "      or by killing the PID of the service."
  ;;
  webremote) # Starts the web admin interface with remote access
    echo "Starting H2 Web Server Admin Tool :: [CTRL-C] to terminate..."
    echo "                    *** !!! WARNING !!!! ***"
    echo ""
    echo "THIS MODE PERMITS REMOTE ACCESS TO YOUR MASTER DATABASE FROM THE INTERNET!"
    echo "DO NOT LEAVE THE ADMIN WEB SERVER RUNNING!!          YOU HAVE BEEN WARNED!"
    echo ""
    java -cp $H2_JAR org.h2.tools.Server -webAllowOthers
  ;;
  backup) # Creates a backup
    echo "Taking backup of H2 Database (opentg)"
    java -cp $H2_JAR org.h2.tools.Backup -file "$IONFALLS_HOME/bbs/db/backup.zip" -dir "$IONFALLS_HOME/bbs/db" -db "ionfalls"
  ;;
  sqldump)
    java -cp $H2_JAR org.h2.tools.Script -url jdbc:h2:tcp://localhost/ionfalls -user '' -script $IONFALLS_HOME/sqldump.zip -options compression zip
  ;;
  *)
    echo "Syntax is: h2 [start | stop | web | webremote | all | backup | sqldump]"
  ;;
esac

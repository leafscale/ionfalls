=begin
POC code to test various alternatives to 'RUBY' i/o functions which have proven to be buggy when trying to get one character at a time.
=end

require 'java'

module JavaLang
    include_package "java.lang"
end

# Imports java.io as Ruby::JavaIO
module JavaIO
    include_package "java.io"
end

# Imports java.util as Ruby::JavaUtil
module JavaUtil
    include_package "java.util"
end

module JavaCharset
 include_package "java.nio.charset"
end


# Import the ApacheCommonsLang Library
#require 'class/commons-lang3.jar'
#module ApacheCommonsLang
#  include_package "org.apache.commons.lang3"
#end


=begin
require './commons-io-2.6.jar'
module ApacheCommonsIO
  include_package "org.apache.commons.io"
end

#ac = ApacheCommonsIO
#acu = ac::IOUtils
# NEW ApacheCommons way of getting a character
#jsi = JavaLang::System.in    # InputStream
#br = JavaIO::BufferedReader
#jisr = JavaIO::InputStreamReader
#getch = acu.toBufferedReader(jisr.new(jsi, 'UTF-16'))
#getch = acu.to_string(jisr.new(jsi, 'UTF-8'))
#char = getch.read
# WORKS
## Test is a byte read in from Java::System.in passed through apache commons ioutil read_fully routine
# test = acu.read_fully(jsi, 1)  
# test.to_ary[0]    # return as the character code number
=end


require './lanterna-3.0.1.jar'
module Lanterna
 include_package "com.googlecode.lanterna"
end
module LanternaInput
 include_package "com.googlecode.lanterna.input"
end
module LanternaTerminal
 include_package "com.googlecode.lanterna.terminal"
 include_package "com.googlecode.lanterna.terminal.ansi"
end
# Setup a terminal
lt = 
#term = LanternaTerminal::DefaultTerminalFactory.new.create_terminal  # Create an instance of DefaultTerminalFactory as term


tccb = LanternaTerminal::UnixLikeTerminal::CtrlCBehaviour::TRAP
$term = LanternaTerminal::UnixTerminal.new(JavaLang::System.in, JavaLang::System.out, JavaCharset::Charset.forName("UTF-8"), tccb)
$term.enterPrivateMode         # Enter PrivateMode for terminal screen


def getkeypress
pressed = false
while pressed == false do
  kp = $term.pollInput        # Get Keypress from terminal pollInput method
  if kp != nil
    keytype = kp.getKeyType  # Retrieve the keytype
    pressed = true
  end
end

puts "Keytype of : "+ keytype.name
case keytype.name
  when "Character"
    ks = kp.getCharacter.chr
    puts "Character of : " + ks.to_s
  when "Backspace"
    puts "Backspace"  
  end
end


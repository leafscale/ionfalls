string = "R\u00E9sum\u00E9"

tlc = "\u250c"   # Char of ┌
trc = "\u2510"   # Char of ┐
blc = "\u2514"   # Char of └ 
brc = "\u2518"   # Char of ┘
hline = "\u2500" # Char of ─
vline = "\u2502" # Char of │

row1 = tlc + (hline * 77) + trc 
row2 = vline + "OpenTG - Unicode drawing test" + (" " * 48) + vline
row3 = blc + (hline * 77) + brc 

puts row1
puts row2
puts row3

puts "Re-defined with character in the string, instead of unicode hex notation"

tlc = "┌"
trc = "┐"
blc = "└"
brc = "┘"
hline = "─"
vline = "│"

row1 = tlc + (hline * 77) + trc
row2 = vline + " OpenTG - Unicode drawing test 2" + (" " * 45) + vline
row3 = blc + (hline * 77) + brc

puts row1
puts row2
puts row3



=begin
open("transcoded.txt", "w:ISO-8859-1") do |io|
  io.write(string)
end

puts "raw text:"
p File.binread("transcoded.txt")
puts

open("transcoded.txt", "r:ISO-8859-1:UTF-8") do |io|
  puts "transcoded text:"
  p io.read
end
=end

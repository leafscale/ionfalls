# Imports java.io as Ruby::JavaIO
module JavaIO
    include_package "java.io"
end

# Import the ApacheCommons-Exec Library
require './commons-exec-1.3.jar'
module ApacheCommonsExec
  include_package "org.apache.commons.exec"
  include_package "org.apache.commons.exec.launcher"
end


require './jline-3.9.1-SNAPSHOT.jar'
module JLine
  include_package 'org.jline.builtins'
  include_package 'org.jline.terminal'
  include_package 'org.jline.utils'
end

#$nano = JLine::NanoRestricted.new($term, File.open('/tmp/samplecontent.txt', "w+"))

subject = "Test Message Posting"
filename = JavaIO::File.new('/tmp/samplecontent.txt')

$term = JLine::TerminalBuilder.builder().system(true).signalHandler(JLine::Terminal::SignalHandler.SIG_IGN).build()
$ionedit = JLine::IonEditor.new($term, filename)
$ionedit.printLineNumbers=false
$ionedit.title="IonEdit :: #{subject}"
$ionedit.open(filename.toString)
$ionedit.run


#eh = JLine::ExecHelper
#eh.exec(true, "/bin/rnano")

#
# POC Code to run the NANO editor via FFI from Jruby
#
##
## 20180925 : Testing for MacOS
require 'ffi'
module JExec
    extend FFI::Library
    ffi_lib("c")
    attach_function :execvp, [:string, :pointer], :int
end

nano_binary_macos = "/usr/bin/nano"
nano_binary_linux = "/bin/rnano"
nano_switch = "-R"

tmpfile_macos = "/tmp/foo"
tmpfile_linux = "/tmp/foo"


 strptrs = []
  strptrs << FFI::MemoryPointer.from_string("#{nano_binary_macos}")
  strptrs << FFI::MemoryPointer.from_string("#{tmpfile_macos}")
  strptrs << FFI::MemoryPointer.from_string("-t")
  strptrs << nil

  # Now load all the pointers into a native memory block
  argv = FFI::MemoryPointer.new(:pointer, strptrs.length)
  strptrs.each_with_index do |p, i|
   argv[i].put_pointer(0,  p)
  end

  JExec.execvp("#{nano_binary_macos}", argv)


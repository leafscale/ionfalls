#!/bin/bash
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2008-2019 - Chris Tusa & the IonFalls project               *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: runtime.sh
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: BASH Shell script which sets the environment variables.
#
# 
# To use this file, from your bash shell, enter:  'source ./runtime.sh'
#
#-----------------------------------------------------------------------------
#
IONFALLS_HOME=/opt/ionfalls

JAVA_HOME=$IONFALLS_HOME/contrib/jdk

JRUBY_HOME=$IONFALLS_HOME/contrib/jruby

PATH=$JAVA_HOME/bin:$JRUBY_HOME/bin:$PATH

=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: bbs/db/migrations/001_create_tables.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@telegard.org>
# Description: Database migration file for creating the table structure.
#
#-----------------------------------------------------------------------------
=end

Sequel.migration do
  change do

#============================================================================
# Table: Users
#============================================================================
    create_table(:users) do
    primary_key  :id
    boolean      :islocked
    String      :login,          :size=>16, :unique=>true
    String      :firstname,      :size=>25
    String      :lastname,       :size=>25
    String      :password
    String      :address1
    String      :address2
    String      :city,           :size=>25
    String      :state,          :size=>5
    String      :postal,         :size=>15
    String      :country,        :size=>5
    String      :phone,          :size=>18
    String      :gender,         :size=>1
    String      :email,          :size=>60
    String      :custom1,        :size=>80
    String      :custom2,        :size=>80
    String      :custom3,        :size=>80
    String      :sysopnote,      :text=>true
    String      :pwhint_question
    String      :pwhint_answer
    Date        :bday
    Date        :pwexpires
    Fixnum      :group_id
    Fixnum      :timebank
    Fixnum      :total_files_up
    Fixnum      :total_files_down
    Fixnum      :total_messages
    Fixnum      :login_failures
    Fixnum      :login_total
    TimeStamp   :login_last
    TimeStamp   :created

    # Define Users' Preferences
    boolean     :pref_fastlogin,  :default => false
    Fixnum      :pref_term_height,:default => 24
    Fixnum      :pref_term_width, :default => 80
    boolean     :pref_term_pager, :default => true
    boolean     :pref_term_color, :default => true
    boolean     :pref_show_menus, :default => true
    String      :pref_editor,     :default => 'ioneditor'
    end

#============================================================================
# Table: Groups
#============================================================================
    create_table(:groups) do
      primary_key  :id
      varchar      :name
      varchar      :sysopnote
      TimeStamp    :created
      integer      :level
      integer      :dailytimelimit
      integer      :max_timedeposit
      integer      :max_timewithdraw
      integer      :max_downloads
      integer      :max_downbytes
      integer      :max_uploads
      integer      :max_upbytes
      integer      :max_posts
      boolean      :allow_login
      boolean      :allow_msgareas
      boolean      :allow_readpost
      boolean      :allow_writepost
      boolean      :allow_pagesysop
      boolean      :allow_chatrooms
      boolean      :allow_fileareas
      boolean      :allow_download
      boolean      :allow_upload
      boolean      :allow_extprogs
      boolean      :is_admin
      boolean      :admin_system
      boolean      :admin_announcements
      boolean      :admin_files
      boolean      :admin_msgs
      boolean      :admin_users
      boolean      :admin_groups
      boolean      :admin_chat
      boolean      :admin_extprogs
    end

#============================================================================
# Table: Sessions
#============================================================================
    create_table(:sessions) do
    # User pointers
    primary_key  :id           # Session ID.
    integer      :user_id      # UID of user
    String       :username     # Alias of User
    integer      :group_id     # Group id for RBAC
    integer      :caller_id    # User's callerid in history
    integer      :level        # Permission Level

    # User Tracking
    integer      :filearea     # ID of user's current filearea
    integer      :msgarea      # ID of user's current msgarea
    integer      :chatroom     # ID of user's current chatroom
    String       :current_area # String defining user's current location
    TimeStamp    :expires      # Time session expires
    TimeStamp    :created      # Time session created

    # User preferences
    boolean      :pref_show_menus # User has menus toggled on/off
    boolean      :pref_term_pager # User has pager toggled on/off
    String       :pref_editor     # User selected editor
    end

#============================================================================
# Table: Callhistory
#============================================================================
    create_table(:callhistory) do
    primary_key  :id
    integer      :user_id
    String       :alias       # Added for convenience.        
    Timestamp    :time_login
    TimeStamp    :time_logout
    end

#============================================================================
# Table: Msgareas
#============================================================================
    create_table(:msgareas) do
    primary_key   :id                   # ID
    String        :name                 # Name of Area
    text          :description          # Friendly Description
    integer       :minlevel_read        # Minimum Group Level allowed read access
    integer       :minlevel_write       # Minimum Group Level allowed write access
    boolean       :enabled              # Area is enabled
    TimeStamp     :created              # Timestamp of area creation
    end

#============================================================================
# Table: Msgs
#============================================================================
    create_table(:msgs) do
    primary_key  :id              # Message ID
    integer      :msgarea_id      # Message Area ID
    integer      :from_id         # ID of sender
    integer      :to_id           # ID of destination if local
    boolean      :read            # ? Message Read or Unread
    String       :from            # From Header
    String       :to              # To Header
    String       :cc              # CC Header
    String       :bcc             # BCC Header
    String       :subject         # Subject Header
    text         :body            # Message
    TimeStamp    :composed        # Message composed timestamp
    TimeStamp    :received        # Message received by this system timestamp
    Array        :read_by         # Array of User_Id's that have read this article.
    end

#============================================================================
# Table: Fileareas
#============================================================================
    create_table(:fileareas) do
    primary_key  :id              # ID
    varchar      :name            # File Area Short Name
    Text         :description     # Short Description of Area
    Boolean      :read            # File Area is "READ" by users: list or download
    Boolean      :write           # File Area is "WRITE" by users: delete or upload
    Boolean      :enabled         # Area is enabled    
    integer      :minlevel_read   # Minimum Group Level allowed read access
    integer      :minlevel_write  # Minimum Group Level allowed write access
    varchar      :path            # Filesystem path to filestore
    TimeStamp    :created         # File Area Creation time
    end

#============================================================================
# Table: Files
#============================================================================
    create_table(:files) do
    primary_key  :id              # File ID
    integer      :ionfilearea_id   # Area ID
    integer      :owner_id        # File Owner (maintainer)
    integer      :uploaded_by     # User who submitted file
    integer      :approved_by     # User who approved file (if any)
    boolean      :enabled         # File available for download?
    String       :filename        # Filename
    String       :name            # Friendly Short Name
    text         :description     # Long description
    text         :checksum        # Some Checksum Value (MD5/SHA,etc)
    String       :version         # Version of file (optional)
    String       :vendor          # Vendor of the file (optional)
    String       :license         # License file distributed under (optional)
    String       :url             # URL for more information (optional)
    integer      :size            # Size of file in bytes on storage
    TimeStamp    :mtime           # File's mtime on storage
    TimeStamp    :created_at      # Time file added to DB
    TimeStamp    :modified_at     # Time file modified in DB
    end

#============================================================================
# Table: Bbslist
#============================================================================
    create_table(:bbslist) do
    primary_key  :id
    varchar      :bbsname
    Text         :description
    varchar      :sysopname
    varchar      :bbsurl
    varchar      :homepage
    varchar      :submitted_by
    TimeStamp    :created
    end

#============================================================================
# Table: Chatrooms
#============================================================================
    create_table(:chatrooms) do
    primary_key  :id
    varchar      :name
    varchar      :description
    integer      :minlevel
    boolean      :enabled
    TimeStamp    :created
    end

#============================================================================
# Table: Extprogs
#============================================================================
    create_table(:extprogs) do
    primary_key  :id
    varchar      :name
    varchar      :description
    varchar      :command
    TimeStamp    :created
    end

#============================================================================


#============================================================================
# Table: Announcement
#============================================================================
    create_table(:announcements) do
    primary_key  :id
    varchar      :title
    Text         :body
    varchar      :submitted_by
    integer      :priority
    TimeStamp    :created
    boolean      :expires,  :default => false
    Date         :expiration
    end

#============================================================================
  end #// change do
end #// migration do

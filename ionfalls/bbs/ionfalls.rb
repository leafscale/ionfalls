#!/usr/bin/env jruby
=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: bbs/ionfalls.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Main Runtime
#
#-----------------------------------------------------------------------------
=end

# => Check if the program was called using the correct Ruby Interpreter
if RUBY_VERSION >= "2.5.0"
  unless defined? JRUBY_VERSION
    puts "IonFalls requires JRuby. Please visit ( http://www.jruby.org/ )."
    exit 255
  end
else
  puts "IonFalls requires JRuby interpreter compatible with Ruby '2.5.0' or later."
end


# => Prevent CTRL-C or SIGINT (2) Interrupt whilst running to prevent data corruption
# => We will want to handle this to perform a "CLEAN" shutdown.
Signal.trap("INT") do
  puts "CTRL-C (SIGINT) detected... ignoring"
  #exit 200
end

# => Prevent CTRL-Z or SIGTSTP (18) whilst running to prevent data corruption
# => We will want to handle this to perform a "CLEAN" shutdown.
Signal.trap("TSTP") do
end

# Deal with SIGHUP signal 1 from OS
Signal.trap("HUP") do
  puts "Ignoring HUP signal from OS."
end

# Handle SIGTERM signal 15 from OS
Signal.trap("TERM") do
  print "Termination signal received from OS..."
  Ionfalls.goodbye_fast
end


# Quit if the program runs as 'root'.
currentuser = `whoami`
if currentuser.chomp == "root"
puts "For security reasons, this program will not run as 'root' exiting..."
  exit 100
end

$LOAD_PATH.unshift('.')

# Detect Platform information and store into hash.
require 'java'
system        = java.lang.System
$jvminfo = {
'os_type'      => system.getProperty( 'os.name' ),
'os_arch'      => system.getProperty( 'os.arch'),
'os_vers'      => system.getProperty( 'os.version'),
'java_vend'    => system.getProperty( 'java.vendor'),
'java_vers'    => system.getProperty( 'java.version'),
'java_home'    => system.getProperty( 'java.home'),
'java_vm_vend' => system.getProperty( 'java.vm.vendor'),
'java_vm_vers' => system.getProperty( 'java.vm.version'),
'java_vm_name' => system.getProperty( 'java.vm.name'),
}


require 'optparse'
def getoptions(args)

    options = {}

    opts = OptionParser.new do |opts|
    opts.banner = "IonFalls :: (https://www.ionfalls.org) :: Copyright (c) 2008-2019, Chris Tusa.\n See LICENSE file \nUsage: "
    opts.separator ""
    
    opts.on("-g", "--genconfig", "Build sample configuration.") do |g|
      options[:genconfig] = g
    end

    opts.on("-l", "--login", "Perform Local Login to system.") do |l|
      options[:login] = l
    end

    opts.on("-a", "--admin", "Run the administration menu") do |a|
      options[:admin] = a
    end
    
    opts.on("-p", "--poc", "Run developer POC code.") do |p|
      options[:poc] = p
    end

    opts.on("-v", "--version", "Print version and exit.") do |v|
      options[:version] = v
    end

    opts.on_tail("-h", "--help", "Show this help message.") do |h|
      puts opts; return 0 
    end
  end  # end opts do

  begin opts.parse!
    rescue OptionParser::InvalidOption => invalidcmd
      puts "Invalid command options specified: #{invalidcmd}"
      puts opts
      return 1
    rescue OptionParser::ParseError => error
      puts error
  end # end begin

  # DEFAULT BEHAVIOR
  if options.empty? == true
    options[:login] = true
  end
  options
end # end def

cmdswitch = getoptions(ARGV)

if cmdswitch.size > 1
  puts "note: #{$0} can only accept a single argument switch."
else

  # Generates a default or sample configuration file. Does NOT override an actual config.
  if cmdswitch[:genconfig]
    sample = 'conf/ionfalls.conf.yaml.sample'
    unless File.exist?(sample)
      require "lib/ionconfig.rb"      
      Ionconfig.makedefault(sample)
      puts "Sample config created: #{sample}"
    else
      puts "File already exists, aborting."      
    end
  end

  # Run some POC code defined in this block
  if cmdswitch[:poc]
    require 'lib/ionfalls'
    Ionfalls.init
    # Enter the proof of concept code here:
    #
    puts "POC Test: Inputform - text type"
    testvalue = Ionio::Input.inputform(length=25, formtype='text')
    puts "testvalue = #{testvalue}"
  end #/poc

  # Admin Menu - prompt for sysop pwd first
  if cmdswitch[:admin]
    require 'lib/ionfalls'
    Ionfalls.init
    require 'lib/ionlogin'
    $term = Ionio.createterminal
    Ionio::ansiclear
    adminpwd = Ionlogin::passwordprompt
    adminuser= User.select(:login).where(:id => 1).first.login
    auth = User.authorize(adminuser, adminpwd)     
    if auth[:result] == "true"            
      require 'lib/controllers/admin'      
      AdminController.new.menu
    else
      puts "Authorization failed."
    end
    exit 0
  end #/admin
  
  # Local login
  if cmdswitch[:login]
    require 'lib/ionfalls'
    Ionfalls.init
    require 'lib/ionlogin'
    $term = Ionio.createterminal
    
=begin
    # Create a Lantern Terminal of type "UnixTerminal" and trap "CTRL-C"
    # Start a private mode terminal session
#    $term.enterPrivateMode    
    # Clear the screen
#    $term.clearScreen
=end
    
    Ionio::ansiclear
    validuser = Ionlogin::auth
    if $session && validuser == true
      AnnouncementController.new.read
      MainController.new.menu
      # If we have reached this point, the main controller has exited.
      # Terminate the session in case nothing else has done so
      $session.destroy if $session
      $session = nil if $session
=begin
      $term.clearScreen
      $term.exitPrivateMode
      $term.destroy
=end
    end    
    exit 0
  end

  # -> Switch: -v  :  Display version number and exit immediatly.
  if cmdswitch[:version]
    require 'lib/ionconstants'
    puts IONFALLS_VERSION
    require 'pp'
    pp $jvminfo.inspect
    exit 0
  end
end


${color.lightcyan?right_pad(30)}Listing of File Areas

${color.brightblue}Area  Name                       Description
${color.darkgray}----  -------------------------  --------------------------------------------------
<#if areas?has_content><#list areas as a>
${color.brightgreen}${a.id?string?right_pad(5)} ${color.brightcyan}${a.name?capitalize?right_pad(26)} ${color.white}${a.description}
</#list>${color.reset}</#if>

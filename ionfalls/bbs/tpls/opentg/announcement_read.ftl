
<#if a?has_content>
${color.darkgray}___|${color.black}${color.on_white} ${a.priority?string?right_pad(3)}${color.on_black}${color.darkgray}|______________________________________________________________________${color.norm}
${color.brightyellow}${a.title}
${color.brightblue}${a.created?string("MM/dd/yyyy HH:mm")} by ${a.submitted_by} <#if a.expires>expires on ${a.expiration?string("MM/dd/yyyy")}</#if>
${color.reset}${a.body}
${color.reset}</#if>

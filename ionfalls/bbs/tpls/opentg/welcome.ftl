
------------------------------------------------------------------------------
${color.white}Welcome, ${color.lightblue}${username}${color.white}. You have entered ${color.lightcyan}${bbs.name}${color.reset}
${color.gray}(${bbs.tagline})${color.reset}
------------------------------------------------------------------------------
<#if timetoday?has_content>You have logged ${timetoday}/${grouplimit} minutes today.</#if>
<#if logintoday?has_content>You have made ${logintoday} connections today</#if>
System time is currently ${curtime}
This session will expire on: ${expires}.
You have ${timeremain} minutes remaining.
------------------------------------------------------------------------------

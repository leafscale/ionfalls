
${color.reset}
${color.lightcyan?right_pad(30)}Who's Online

${color.blue}User             Login Time                Location
${color.darkgray}---------------- ------------------------- ------------------
<#if who?has_content><#list who as w>
${color.lightgreen}${w.username?right_pad(16)} ${color.cyan}${w.created?datetime?string?right_pad(25)} <#if w.current_area?has_content>${color.brightyellow}${w.current_area?string}</#if>
</#list>${color.reset}</#if>

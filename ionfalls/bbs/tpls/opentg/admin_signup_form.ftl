${color.cls}
${color.brightcyan}${""?left_pad(2)}IonFalls Administration :: Signup Form Customization

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Extra Data Fields ${color.darkgray}]${""?right_pad(53,"-")}
 ${color.cyan}(${color.brightyellow}A${color.cyan}) ${color.gray}Address : ${color.white}${signup.ask_address?then('Ask', 'Skip')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}D${color.cyan}) ${color.gray}Phone   : ${color.white}${signup.ask_phone?then('Ask', 'Skip')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}G${color.cyan}) ${color.gray}Custom 1: ${color.white}${signup.ask_custom1?then('Ask', 'Skip')?right_pad(5)}${color.darkgray}
 ${color.cyan}(${color.brightyellow}B${color.cyan}) ${color.gray}Postal  : ${color.white}${signup.ask_postal?then('Ask', 'Skip')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}E${color.cyan}) ${color.gray}Gender  : ${color.white}${signup.ask_gender?then('Ask', 'Skip')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}H${color.cyan}) ${color.gray}Custom 2: ${color.white}${signup.ask_custom2?then('Ask', 'Skip')?right_pad(5)}${color.darkgray}
 ${color.cyan}(${color.brightyellow}C${color.cyan}) ${color.gray}Country : ${color.white}${signup.ask_country?then('Ask', 'Skip')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}F${color.cyan}) ${color.gray}Birthday: ${color.white}${signup.ask_bday?then('Ask', 'Skip')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}I${color.cyan}) ${color.gray}Custom 3: ${color.white}${signup.ask_custom3?then('Ask', 'Skip')?right_pad(5)}${color.darkgray}
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Custom Questions ${color.darkgray}]${""?right_pad(54,"-")}
 ${color.cyan}(${color.brightyellow}1${color.cyan}) ${color.gray}1: <#if signup.custom1?has_content>${color.white}${signup.custom1}</#if>
 ${color.cyan}(${color.brightyellow}2${color.cyan}) ${color.gray}2: <#if signup.custom2?has_content>${color.white}${signup.custom2}</#if>
 ${color.cyan}(${color.brightyellow}3${color.cyan}) ${color.gray}3: <#if signup.custom3?has_content>${color.white}${signup.custom3}</#if>
${color.darkgray}${""?right_pad(79,"-")}
 ${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Go Back${color.reset}

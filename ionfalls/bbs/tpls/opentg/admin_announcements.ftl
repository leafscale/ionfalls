${color.cls}${color.brightcyan}${color.on_blue}${""?left_pad(2)}IonFalls Administration :: Announcements${""?right_pad(37)}${color.reset}

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Summary Information ${color.darkgray}]${""?right_pad(54,"-")}
 ${color.cyan}(${color.brightyellow}T${color.cyan}) ${color.gray}Title   : ${color.white}${announcement.title}
 ${color.cyan}(${color.brightyellow}!${color.cyan}) ${color.gray}Expires?: ${announcement.expires?then("Yes","No")?right_pad(18)}<#if announcement.expires == true>${color.cyan}(${color.brightyellow}D${color.cyan}) ${color.gray}Expires Date: ${color.white}${announcement.expiration?date?right_pad(18)}</#if>
 ${color.cyan}(${color.brightyellow}P${color.cyan}) ${color.gray}Priority: ${announcement.priority?right_pad(18)}${color.gray}Author  : ${announcement.submitted_by}
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Announcement Body   ${color.darkgray}]${""?right_pad(54,"-")}
${color.white}${announcement.body}

 ${color.gray}Created Date: ${color.white}${announcement.created?datetime}
${color.darkgray}${""?right_pad(79,"-")}
${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Exit   ${color.cyan}(${color.brightyellow}[${color.cyan}) ${color.gray}Previous   ${color.cyan}(${color.brightyellow}]${color.cyan}) ${color.gray}Next   ${color.cyan}(${color.brightyellow}*${color.cyan}) ${color.gray}Delete   ${color.cyan}(${color.brightyellow}+${color.cyan}) ${color.gray}Add   ${color.cyan}(${color.brightyellow}E${color.cyan}) ${color.gray}Edit${color.reset}

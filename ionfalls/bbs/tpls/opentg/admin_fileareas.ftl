${color.cls}${color.brightcyan}${color.on_blue}${""?left_pad(2)}IonFalls Administration :: File Area Manager${""?right_pad(35)}${color.reset}

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Area Information ${color.darkgray}]${""?right_pad(54,"-")}
 ${color.cyan}(${color.brightyellow}A${color.cyan}) ${color.gray}Area Name  : ${color.white}${area.name?right_pad(30)}${color.gray}ID: ${color.white}${area.id?string?right_pad(10)} ${color.cyan}(${color.brightyellow}!${color.cyan}) ${color.gray}Enabled: <#if area.enabled == false>${color.red}No<#elseif area.enabled == true>${color.green}Yes</#if>
 ${color.cyan}(${color.brightyellow}B${color.cyan}) ${color.gray}Description: ${color.white}${area.description}
 ${color.cyan}(${color.brightyellow}C${color.cyan}) ${color.gray}Read Level : ${color.white}${area.minlevel_read?string.computer?right_pad(15)} ${color.cyan}(${color.brightyellow}D${color.cyan}) ${color.gray}Write Level: ${color.white}${area.minlevel_write?string.computer}
 ${color.cyan}(${color.brightyellow}E${color.cyan}) ${color.gray}Readable   : ${color.white}${area.read?then("Yes","No")?right_pad(15)} ${color.cyan}(${color.brightyellow}F${color.cyan}) ${color.gray}Writable   : ${color.white}${area.write?then("Yes","No")}

 ${color.cyan}(${color.brightyellow}P${color.cyan}) ${color.gray}Files Path : ${color.white}${area.path}

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Statistics ${color.darkgray}]${""?right_pad(60,"-")}
${color.gray}Created  : ${color.white}${area.created?date}

 ${color.darkgray}${""?right_pad(79,"-")}
${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Exit   ${color.cyan}(${color.brightyellow}[${color.cyan}) ${color.gray}Previous   ${color.cyan}(${color.brightyellow}]${color.cyan}) ${color.gray}Next   ${color.cyan}(${color.brightyellow}*${color.cyan}) ${color.gray}Delete   ${color.cyan}(${color.brightyellow}+${color.cyan}) ${color.gray}Add${color.reset}

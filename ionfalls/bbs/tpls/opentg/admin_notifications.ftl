${color.cls}
${color.brightcyan}${""?left_pad(2)}IonFalls Administration :: Notification Settings

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Who to Notify ${color.darkgray}]${""?right_pad(57,"-")}
 ${color.cyan}(${color.brightyellow}A${color.cyan}) ${color.gray}User     : ${color.white}${notify.notify_user?then('Yes', 'No')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}C${color.cyan}) ${color.gray}User Name  : ${color.white}${notify.user_name}
 ${color.cyan}(${color.brightyellow}B${color.cyan}) ${color.gray}Group    : ${color.white}${notify.notify_group?then('Yes', 'No')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}D${color.cyan}) ${color.gray}Group Name : ${color.white}${notify.group_name}
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Event Triggers ${color.darkgray}]${""?right_pad(56,"-")}
 ${color.cyan}(${color.brightyellow}E${color.cyan}) ${color.gray}on_signup: ${color.white}${notify.on_signup?then('Yes', 'No')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}F${color.cyan}) ${color.gray}on_bbslist: ${color.white}${notify.on_bbslist?then('Yes', 'No')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}G${color.cyan}) ${color.gray}on_lockout: ${color.white}${notify.on_lockout?then('Yes', 'No')?right_pad(5)}${color.darkgray}
 ${color.cyan}(${color.brightyellow}H${color.cyan}) ${color.gray}on_upload: ${color.white}${notify.on_upload?then('Yes', 'No')?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}I${color.cyan}) ${color.gray}<reserved>: ${color.white}${""?right_pad(10)}${color.darkgray}| ${color.cyan}(${color.brightyellow}J${color.cyan}) ${color.gray}<reserved>: ${color.white}${""?right_pad(5)}${color.darkgray}
${color.darkgray}${""?right_pad(79,"-")}
 ${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Go Back${color.reset}

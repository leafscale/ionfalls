${color.cls}${color.brightcyan}${color.on_blue}${""?left_pad(2)}IonFalls Administration :: Chat Room Manager${""?right_pad(34)}${color.reset}

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Room Information ${color.darkgray}]${""?right_pad(54,"-")}
 ${color.cyan}(${color.brightyellow}A${color.cyan}) ${color.gray}Room Name  : ${color.white}${room.name?right_pad(30)}${color.gray}ID: ${color.white}${room.id?string?right_pad(10)} ${color.cyan}(${color.brightyellow}!${color.cyan}) ${color.gray}Enabled: <#if room.enabled == false>${color.red}No<#elseif room.enabled == true>${color.green}Yes</#if>
 ${color.cyan}(${color.brightyellow}B${color.cyan}) ${color.gray}Description: ${color.white}${room.description}
 ${color.cyan}(${color.brightyellow}C${color.cyan}) ${color.gray}Join Level : ${color.white}${room.minlevel?string.computer?right_pad(15)}
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Statistics ${color.darkgray}]${""?right_pad(60,"-")}
${color.gray}Created  : ${color.white}${room.created?date}

 ${color.darkgray}${""?right_pad(79,"-")}
${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Exit   ${color.cyan}(${color.brightyellow}[${color.cyan}) ${color.gray}Previous   ${color.cyan}(${color.brightyellow}]${color.cyan}) ${color.gray}Next   ${color.cyan}(${color.brightyellow}*${color.cyan}) ${color.gray}Delete   ${color.cyan}(${color.brightyellow}+${color.cyan}) ${color.gray}Add${color.reset}

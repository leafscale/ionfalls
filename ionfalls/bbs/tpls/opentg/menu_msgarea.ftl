

${color.lightcyan?left_pad(30)}Message Area Menu
${color.white}
${color.brightmagenta?left_pad(10)}A${color.blue}) ${color.brightblue}Area Listing  ${color.brightcyan?right_pad(5)}  ${color.brightmagenta}I${color.blue}) ${color.brightblue}Area Information${color.brightcyan?right_pad(5)}  ${color.brightmagenta}R${color.blue}) ${color.brightblue}Read & Post
${color.brightmagenta?left_pad(10)}J${color.blue}) ${color.brightblue}Jump to Area  ${color.brightcyan?right_pad(5)}  ${color.brightmagenta}L${color.blue}) ${color.brightblue}List Messages  ${color.brightcyan?right_pad(5)}   ${color.brightmagenta}S${color.blue}) ${color.brightblue}Search All Areas
${color.brightmagenta?left_pad(10)}[${color.blue}) ${color.brightblue}Previous Area ${color.brightcyan?right_pad(5)}  ${color.brightmagenta}]${color.blue}) ${color.brightblue}Next Area      ${color.brightcyan?right_pad(5)}   ${color.brightmagenta}X${color.blue}) ${color.brightblue}Return to Main
${color.blue}[${color.green}${areanum}${color.darkgray}:${color.brightyellow}${areaname} ${color.darkgray}(${color.white}${msgcount} msgs${color.darkgray}) ${color.yellow}${areadesc}${color.blue}]

${color.cls}
${color.brightcyan}${""?left_pad(2)}IonFalls Administration :: Main Menu

${color.blue}System Name  : ${color.white}${bbs.name}
${color.blue}System Status: <#if bbs.enabled == true>${color.brightgreen}Enabled<#elseif bbs.enabled == false>${color.white}${color.on_red}Disabled<#else>${color.brightmagenta}${color.on_black}${color.blink}Configuration Error</#if>${color.reset}

 ${color.cyan}(${color.brightyellow}A${color.cyan})${color.gray}nnouncements ${""?right_pad(15)}${color.cyan}(${color.brightyellow}B${color.cyan})${color.gray}BS Settings  ${""?right_pad(5)}
 ${color.cyan}(${color.brightyellow}U${color.cyan})${color.gray}ser Manager  ${""?right_pad(15)}${color.cyan}(${color.brightyellow}G${color.cyan})${color.gray}roups/Roles  ${""?right_pad(5)}
 ${color.cyan}(${color.brightyellow}M${color.cyan})${color.gray}essage Areas ${""?right_pad(15)}${color.cyan}(${color.brightyellow}F${color.cyan})${color.gray}ile Areas    ${""?right_pad(5)}
 ${color.cyan}(${color.brightyellow}C${color.cyan})${color.gray}hat Rooms    ${""?right_pad(15)}
 ${color.cyan}(${color.brightyellow}H${color.cyan})${color.gray}elp          ${""?right_pad(15)}
 ${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Exit${color.reset}

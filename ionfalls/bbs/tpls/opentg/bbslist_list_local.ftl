

${color.brightcyan?right_pad(20)}${bbs.name}'s Directory

<#if bbslist?has_content><#list bbslist as b>
${color.darkgray}___|${color.black}${color.on_white} ${b.id?string?right_pad(3)}${color.on_black}${color.darkgray}|______________________________________________________________________${color.norm}
${color.brightblue}BBS Name   : ${color.white}${b.bbsname}
${color.brightblue}Submitted  : ${color.reset}${color.gray}${b.created?string("MM/dd/yyyy HH:mm")} by ${b.submitted_by}
${color.brightblue}Sysop Name : ${color.reset}${color.gray}${b.sysopname}
${color.brightblue}Homepage   : ${color.reset}${color.gray}${b.homepage}
${color.brightblue}Connect URL: ${color.reset}${color.gray}${b.bbsurl}
${color.brightblue}Description: ${color.reset}${color.gray}${b.description}
</#list>${color.reset}</#if>


${color.red}NOTE: ${color.brightred}Deleting a group can not be undone.

${color.white}Are you sure you want to delete this group? ${color.brightblue}(Y/N):

${color.red}NOTE: ${color.brightred}Deleting a messagearea can not be undone.

${color.white}Are you sure you want to delete this area? ${color.brightblue}(Y/N):

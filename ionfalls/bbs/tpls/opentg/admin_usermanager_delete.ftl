${color.red}NOTE: ${color.brightred}Deleting a user can not be undone.
Consider using the Lockout feature to disable the account.

${color.white}Are you sure you want to delete this user? ${color.brightblue}(Y/N):

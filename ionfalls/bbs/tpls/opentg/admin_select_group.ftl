${color.reset}
${color.lightcyan?right_pad(30)}Groups

<#if groups?has_content><#list groups as g>${color.cyan}(${color.brightyellow}${groups?seq_index_of(g)}${color.cyan}) ${color.gray}${g}
</#list></#if>

${color.brightblue}Select: ${color.reset}

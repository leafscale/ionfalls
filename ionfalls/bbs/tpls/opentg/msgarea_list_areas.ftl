
${color.lightcyan?right_pad(30)}Listing of Message Areas

${color.brightblue}Area  Name                       Description
${color.darkgray}----  -------------------------  ----------------------------------------
<#if areas?has_content><#list areas as a>
${color.brightgreen}${a.id?string?right_pad(5)} ${color.brightcyan}${a.name?upper_case?right_pad(26)} ${color.white}${a.description?right_pad(40)?substring(0,40)}
</#list>${color.reset}</#if>

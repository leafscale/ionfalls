Many systems may implement a daily time limit that restricts a user login
to using a fixed amount of time per period (typically a single day). Once
a user's time expires, they will be disconnected and future attempts to
reconnect will be terminated with an out-of-time notice.

The TimeBank feature provides the ability for a user to store session time
from the remaining amount into a deposit account. This amount can be
withdrawn at a later date to extend a session. Users may also be able to
grant time from their bank to other users if permissions allow.


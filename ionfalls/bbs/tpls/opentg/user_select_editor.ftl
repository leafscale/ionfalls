
${color.white}Select a default editor to be used when writing messages or long input fields.

${color.brightmagenta}1${color.blue}) ${color.brightblue}IonEditor     ${color.darkgray}-${color.reset} full-sceen editor for IonFalls.

 ${color.brightmagenta}0${color.blue}) ${color.brightblue}AnsiEdit ${color.darkgray}-${color.reset} AnsiEdit is a simple, one-line-at-a-time (OLAAT) editor
    ${color.brightyellow}(ionedit)       ${color.reset}Although it does not support full screen editing, it is
                   ${color.reset}intended to be compatible with ANSI terminals. This is deprecated and should not be used except in rare cases.

Select [0-1]:

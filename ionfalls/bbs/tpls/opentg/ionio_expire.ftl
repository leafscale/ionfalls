
${color.brightyellow}Choose an expiration date for this item
 ${color.blue}(${color.brightmagenta}1${color.blue}) ${color.brightblue}7 days
 ${color.blue}(${color.brightmagenta}2${color.blue}) ${color.brightblue}30 days
 ${color.blue}(${color.brightmagenta}3${color.blue}) ${color.brightblue}90 days
 ${color.blue}(${color.brightmagenta}4${color.blue}) ${color.brightblue}Custom${color.reset}
${color.reset}
${color.lightcyan?right_pad(30)}Available Themes

<#if tpls?has_content><#list tpls as t>${color.cyan}(${color.brightyellow}${tpls?seq_index_of(t)}${color.cyan}) ${color.gray}${t?right_pad(2)}
</#list></#if>

${color.brightblue}Select: ${color.reset}

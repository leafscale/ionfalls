
            ${color.lightcyan}${user.login}'s Account Summary

${color.brightyellow}${user.firstname} ${user.lastname} ${color.brown}from ${color.brightyellow}${user.city} ${user.state}, ${user.country}${color.norm}.

${color.brightblue}Member Since    : ${color.white}${user.created?datetime?string}
${color.brightblue}RBAC Group      : ${color.white}${groupname?string}
${color.brightblue}Password Expires: ${color.white}${user.pwexpires?datetime?string}
${color.brightblue}Total Visits    : ${color.white}${user.login_total?string}
${color.brightblue}Total Uploads   : ${color.white}${user.total_files_up?string}
${color.brightblue}Total Downloads : ${color.white}${user.total_files_down?string}
${color.brightblue}Total Msg Posts : ${color.white}${user.total_messages?string}

 ${color.brightmagenta}X${color.blue}) ${color.brightblue}Return to Main Menu${color.reset}


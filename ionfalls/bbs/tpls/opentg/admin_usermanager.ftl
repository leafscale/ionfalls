${color.cls}${color.brightcyan}${color.on_blue}${""?left_pad(2)}IonFalls Administration :: User Manager${""?right_pad(38)}${color.reset}

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}User Information ${color.darkgray}]${""?right_pad(54,"-")}
 ${color.cyan}(${color.brightyellow}A${color.cyan}) ${color.gray}Login Alias: ${color.white}${user.login?right_pad(18)}${color.gray}ID: ${color.white}${user.id?right_pad(9)}  ${color.cyan}(${color.brightyellow}B${color.cyan}) ${color.gray}Group: ${color.white}${groupname} ${color.gray}(${user.group_id})
 ${color.cyan}(${color.brightyellow}C${color.cyan}) ${color.gray}Real Name  : <#assign realname = ["${user.firstname}", "${user.lastname}"]>${realname?join(" ")?right_pad(33)}${color.cyan}(${color.brightyellow}!${color.cyan}) ${color.gray}Acct Locked: ${user.islocked?then("Yes","No")}
 ${color.cyan}(${color.brightyellow}D${color.cyan}) ${color.gray}Email Addr : <#if user.email??>${user.email?right_pad(45)}<#else>${""?right_pad(45)}</#if>
<#if signup.ask_address> ${color.cyan}(${color.brightyellow}E${color.cyan}) ${color.gray}Address    : ${color.white}<#if user.address1??>${user.address1}</#if>
${""?right_pad(17)} <#if user.address2??>${user.address2}</#if>
</#if> ${color.cyan}(${color.brightyellow}F${color.cyan}) ${color.gray}City       : ${color.white}<#if user.city??>${user.city?right_pad(33)}</#if>${color.cyan}(${color.brightyellow}G${color.cyan}) ${color.gray}State: ${color.white}<#if user.state??>${user.state?right_pad(5)}</#if>
<#if signup.ask_country> ${color.cyan}(${color.brightyellow}H${color.cyan}) ${color.gray}Country    : ${color.white}<#if user.country??>${user.country?right_pad(18)}</#if></#if><#if signup.ask_postal>${color.cyan}(${color.brightyellow}I${color.cyan}) ${color.gray}Postal: ${color.white}<#if user.postal??>${user.postal}</#if></#if>
<#if signup.ask_phone> ${color.cyan}(${color.brightyellow}J${color.cyan}) ${color.gray}Phone      : ${color.white}<#if user.phone??>${user.phone?right_pad(18)}</#if></#if><#if signup.ask_gender>${color.cyan}(${color.brightyellow}K${color.cyan}) ${color.gray}Gender: ${color.white}<#if user.gender??>${user.gender?right_pad(2)}</#if></#if><#if signup.ask_bday> ${color.cyan}(${color.brightyellow}L${color.cyan}) ${color.gray}Birthday: ${color.white}<#if user.bday??>${user.bday?date?string.iso}</#if></#if>
<#if signup.ask_custom1> ${color.cyan}(${color.brightyellow}M${color.cyan}) ${color.gray}Custom 1   : <#if signup.custom1??>${signup.custom1}</#if>
<#if user.custom1??>${color.white}${user.custom1}</#if>
</#if><#if signup.ask_custom2> ${color.cyan}(${color.brightyellow}N${color.cyan}) ${color.gray}Custom 2   : <#if signup.custom2??>${signup.custom2}</#if>
<#if user.custom2??>${color.white}${user.custom2}</#if>
</#if><#if signup.ask_custom3> ${color.cyan}(${color.brightyellow}O${color.cyan}) ${color.gray}Custom 3   : <#if signup.custom3??>${signup.custom3}</#if>
<#if user.custom3??>${color.white}${user.custom3}</#if>
</#if>${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}User's Preferences ${color.darkgray}]${""?right_pad(52,"-")}
 ${color.cyan}(${color.brightyellow}P${color.cyan}) ${color.gray}Fast Login : ${color.white}${user.pref_fastlogin?then("On","Off")?right_pad(8)} ${color.cyan}(${color.brightyellow}Q${color.cyan}) ${color.gray}Show Menus : ${color.white}${user.pref_show_menus?then("On","Off")?right_pad(8)} ${color.cyan}(${color.brightyellow}R${color.cyan}) ${color.gray}Pager: ${color.white}${user.pref_term_pager?then("On","Off")?right_pad(8)}
 ${color.cyan}(${color.brightyellow}S${color.cyan}) ${color.gray}Term Height: ${color.white}${user.pref_term_height?string?right_pad(8)} ${color.cyan}(${color.brightyellow}T${color.cyan}) ${color.gray}Term Width : ${color.white}${user.pref_term_width?string?right_pad(8)} ${color.cyan}(${color.brightyellow}U${color.cyan}) ${color.gray}Color: ${color.white}${user.pref_term_color?then("On","Off")?right_pad(8)}
 ${color.cyan}(${color.brightyellow}V${color.cyan}) ${color.gray}Timebank   : ${color.white}${user.timebank?string?right_pad(8)} ${color.cyan}(${color.brightyellow}W${color.cyan}) ${color.gray}Text Editor: ${color.white}${user.pref_editor}
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Statistics ${color.darkgray}]${""?right_pad(60,"-")}
 ${color.blue}Files U/L: ${color.brightmagenta}${user.total_files_up?string?right_pad(5)} ${color.blue}Login Total: ${color.brightmagenta}${user.login_total?right_pad(8)} ${color.blue}Passwd Exp : ${color.brightmagenta}${user.pwexpires?date?string.iso}
 ${color.blue}Files D/L: ${color.brightmagenta}${user.total_files_down?string?right_pad(5)} ${color.blue}Login Fails: ${color.brightmagenta}${user.login_failures?string?right_pad(8)} ${color.blue}Msg Posts  : ${color.brightmagenta}${user.total_messages?string}
 ${color.blue}Created  : ${color.brightmagenta}${user.created?datetime?right_pad(27)} ${color.blue}Last Login : ${color.brightmagenta}${user.login_last?datetime}
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Sysop Note ${color.darkgray}]${""?right_pad(60,"-")}
${color.gray}<#if user.sysopnote??>${user.sysopnote}</#if>
${color.darkgray}${""?right_pad(79,"-")}
${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Exit   ${color.cyan}(${color.brightyellow}[${color.cyan}) ${color.gray}Previous   ${color.cyan}(${color.brightyellow}]${color.cyan}) ${color.gray}Next   ${color.cyan}(${color.brightyellow}*${color.cyan}) ${color.gray}Delete   ${color.cyan}(${color.brightyellow}+${color.cyan}) ${color.gray}Add   ${color.cyan}(${color.brightyellow}#${color.cyan}) ${color.gray}Note${color.reset}

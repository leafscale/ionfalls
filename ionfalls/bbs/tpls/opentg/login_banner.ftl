${color.darkgrey}-=-=-=-=-=-=| ${color.lightblue}IonFalls ${color.lightblue}BBS ${color.blue}(${color.cyan}IonFalls.org${color.blue}) ${color.darkgrey}|=-=-=-=-=-=-${color.norm}

<#if bbsname?has_content>${color.lightyellow}${bbsname}${color.norm}</#if>
<#if bbstagline?has_content>${color.lightcyan}${bbstagline}${color.norm}</#if>

<#if allownew == "true">
${color.white}Enter your login name or '${color.lightmagenta}NEW${color.norm}' to signup.
<#else>
${color.white}Enter your login name.
</#if>
${color.reset}


${color.white}                        IonFalls Bulletin Board System
${color.reset}         Copyright (c) 2008-2018, Chris Tusa & The IonFalls Project
${color.brightyellow}                          http://www.ionfalls.org

${color.darkgray}================================================================================
${color.cyan}IonFalls was made possible by open-source software:

  ${color.lightcyan}* ${color.lightblue}JRuby${color.darkgray}.................(${color.brightyellow}http://jruby.org${color.darkgray})
  ${color.lightcyan}* ${color.lightblue}Apache Commons${color.darkgray}........(${color.brightyellow}http://commons.apache.org${color.darkgray})
  ${color.lightcyan}* ${color.lightblue}FreeMarker${color.darkgray}............(${color.brightyellow}http://freemarker.org${color.darkgray})
  ${color.lightcyan}* ${color.lightblue}Sequel ORM${color.darkgray}............(${color.brightyellow}http://sequel.rubyforge.org${color.darkgray})
  ${color.lightcyan}* ${color.lightblue}H2 Database${color.darkgray}...........(${color.brightyellow}http://h2database.com${color.darkgray})
  ${color.lightcyan}* ${color.lightblue}JaSypt${color.darkgray}................(${color.brightyellow}http://jasypt.org${color.darkgray})
  ${color.lightcyan}* ${color.lightblue}OpenJDK${color.darkgray}................(${color.brightyellow}http://openjdk.java.net${color.darkgray})


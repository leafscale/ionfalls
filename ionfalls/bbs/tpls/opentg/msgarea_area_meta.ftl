
     ${color.brightblue}Area ID: ${color.brightgreen}${area.id?string}
     ${color.brightblue}   Name: ${color.white}${area.name}
     ${color.brightblue}Created: ${color.brightcyan}${area.created?string("EEE MMM dd HH:mm:ss")}
${color.brightblue}  # Messages: ${color.brightmagenta}${msgcount?string?right_pad(10)}
${color.brightblue} Description: ${color.white}${area.description}


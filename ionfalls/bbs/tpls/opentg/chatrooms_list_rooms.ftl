
${color.lightcyan?right_pad(30)}Chatrooms

${color.brightblue}Name                       Description
${color.darkgray}-------------------------  ----------------------------------------
<#if rooms?has_content><#list rooms as r>
${color.brightgreen}${color.brightcyan}${r.name?upper_case?right_pad(26)} ${color.white}${r.description?right_pad(40)?substring(0,40)}
</#list>${color.reset}</#if>

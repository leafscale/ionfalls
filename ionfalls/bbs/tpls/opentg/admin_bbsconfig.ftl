${color.cls}${color.brightcyan}${color.on_blue}${""?left_pad(2)}IonFalls Administration :: BBS Configuration${""?right_pad(33)}${color.reset}

${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}General Settings ${color.darkgray}]${""?right_pad(54,"-")}
 ${color.cyan}(${color.brightyellow}A${color.cyan}) ${color.gray}System enabled : <#if bbs.enabled == true>${color.brightgreen}Enabled<#elseif bbs.enabled == false>${color.white}${color.on_red}Disabled<#else>${color.brightmagenta}${color.on_black}${color.blink}Configuration Error</#if>${color.reset}
 ${color.cyan}(${color.brightyellow}B${color.cyan}) ${color.gray}System name    : ${color.white}${bbs.name}
 ${color.cyan}(${color.brightyellow}C${color.cyan}) ${color.gray}System tagline : ${color.white}${bbs.tagline}
 ${color.cyan}(${color.brightyellow}D${color.cyan}) ${color.gray}System theme   : ${color.white}${bbs.theme}
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Login Settings ${color.darkgray}]${""?right_pad(12,"-")}${color.darkgray}${""?left_pad(8,"-")}[ ${color.brightblue}New User Signup ${color.darkgray}]${""?right_pad(17,"-")}
 ${color.cyan}(${color.brightyellow}E${color.cyan}) ${color.gray}Login attempts : ${color.white}${login.attempts?right_pad(15)}${color.darkgray}| ${color.cyan}(${color.brightyellow}I${color.cyan}) ${color.gray}Default group  : ${color.white}${signup.default_group}
 ${color.cyan}(${color.brightyellow}F${color.cyan}) ${color.gray}Lockout after  : ${color.white}${login.lockout?right_pad(15)}${color.darkgray}| ${color.cyan}(${color.brightyellow}J${color.cyan}) ${color.gray}Customize Form
 ${color.cyan}(${color.brightyellow}G${color.cyan}) ${color.gray}Use login hints: ${color.white}${login.usehint?then('Yes', 'No')?right_pad(15)}${color.darkgray}|
 ${color.cyan}(${color.brightyellow}H${color.cyan}) ${color.gray}Allow new users: ${color.white}${login.allownew?then('Yes', 'No')?right_pad(15)}${color.darkgray}|
${color.darkgray}${""?left_pad(5,"-")}[ ${color.brightblue}Features ${color.darkgray}]${""?right_pad(18,"-")}${color.darkgray}${""?left_pad(8,"-")}[ ${color.brightblue}Editors ${color.darkgray}]${""?right_pad(25,"-")}
 ${color.cyan}(${color.brightyellow}K${color.cyan}) ${color.gray}Announcements  : ${color.white}${feature.announcements?then('On', 'Off')?right_pad(15)}${color.darkgray}| ${color.cyan}(${color.brightyellow}O${color.cyan}) ${color.gray}Default editor : ${color.white}${editors.default_editor}
 ${color.cyan}(${color.brightyellow}L${color.cyan}) ${color.gray}BBS Listings   : ${color.white}${feature.bbslist?then('On', 'Off')?right_pad(15)}${color.darkgray}| ${color.cyan}(${color.brightyellow}P${color.cyan}) ${color.gray}Allow ansiedit : ${color.white}${editors.allow_ansiedit?then('Yes','No')}
 ${color.cyan}(${color.brightyellow}M${color.cyan}) ${color.gray}Time Bank      : ${color.white}${feature.timebank?then('On', 'Off')?right_pad(15)}${color.darkgray}|
 ${color.cyan}(${color.brightyellow}N${color.cyan}) ${color.gray}Time Bank Limit: ${color.white}${limits.timebank_max?string.computer?right_pad(15)}${color.darkgray}|
${color.darkgray}${""?right_pad(79,"-")}
 ${color.cyan}(${color.brightyellow}Q${color.cyan}) ${color.gray}Notifications
 ${color.cyan}(${color.brightyellow}X${color.cyan})${color.gray} <- Exit${color.reset}

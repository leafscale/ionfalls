=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: lib/iondatabase.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Database Connectivity
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Iondatabase (Database)
Iondatabase provides the database connectivity and initialization routines.

== Embedded
IonFalls includes an embedded database engine. H2 is the included database, 
and is provided as a JAR file with the distribution. 
see (http://h2database.com).

=end

module Iondatabase
  require 'rubygems'
  require 'sequel'
#  Sequel::Model.plugin(:schema)  # This plugin was depreacted in gem Sequel >= v4.5
  Sequel::Model.plugin(:skip_create_refresh)
  
  # Create Database Connection based on type of either H2 Embedded or External Remote.
  def Iondatabase.connect(db)
    dbpwd = Security::ConfigPassword.new.decrypt(db['pass'])
    # DEPRECATION WARNING: Remote option will go away in favor of H2
      # -> Try to load Java support
      begin
        require 'java'
      rescue LoadError
        raise "FATAL: Unable to load JAVA hooks. Missing JRuby >= 9.2.0"
      end
      # -> Try to load H2 Database from JAR.
      # (instead of using the GEM for jdbc-h2 which tends to be outdated)
      begin
        require 'class/h2-1.4.197.jar'          # This loads the entire DB engine from jar
        Java::org.h2.Driver             # This loads the native JDBC driver
      rescue LoadError
        raise "ERROR: unable to load embedded database engine."
      end
      # -> Try to connect to H2 Database from Sequel.
      begin
        Sequel.connect("jdbc:h2:tcp://localhost/ionfalls", :user => "#{db['user']}", :password=>dbpwd)
      rescue Sequel::DatabaseConnectionError => e
        raise "ERROR: Unable to connect to embedded database. (#{e})"
      rescue Java::NativeException => e
        raise "ERROR: Unable to connect to embedded database. (#{e})"
      end
  end #/def connect


  # => Initialize the database (reserved)
  def Iondatabase.initdb
    # The database is initialized using Sequel migrations (see Rakefile)
  end #/dev init
  
end # /module

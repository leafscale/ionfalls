=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionlogin.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Handlers for Login/Logout
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Ionlogin (Login/Logout Routines)
Ionlogin handles the procedures for logging into Telegard.
== Configuration Variables $cfg['login']
login:
  attempts: 3
  lockout: 5
  usehint: true
  allownew: true

Where these settings mean: 

attempts:integer - number of failed attempts to allow before disconnecting.
lockout: integer - number of failed attempts for a single user before setting account to is_locked = true.
usehint: boolean - whether to prompt user for password hint on last attempt before lockout.
allownew:boolean - will the login routine accept new users?  

=end

module Ionlogin

  require 'lib/iontemplate'
  require 'bcrypt'
  @logincfg = $cfg['login']
  @bbsinfo = $cfg['bbs']

  # Displays the template for the login banner.
  def Ionlogin::banner
    Iontemplate::display('login_banner.ftl', {"allownew" => @logincfg['allownew'].to_s, "bbsname" => @bbsinfo['name'].to_s, "bbstagline" => @bbsinfo['tagline']})
  end

  # Username prompt - displays template, calls highline ask and returns username.
  def Ionlogin::userprompt
    Iontemplate::display('login_prompt_user.ftl')
    username = Ionio::Input::loginform
    if username.upcase == "NEW"
      if @logincfg['allownew'] == true
        username = self.signup
      else
        Iontemplate::display('login_new_disabled.ftl')
        return nil
      end
    end
    # check if the username exists in the DB so as not to waste resources on nonexistent accounts.
    if User.exists?(username) == true
      return username
    else
      
      return nil
    end
  end

  # Password prompt - displays template, calls highline ask and returns crypted password.
  def Ionlogin::passwordprompt
    t = Iontemplate::Template.new('login_prompt_password.ftl')
    t.render()
    cleartxt = Ionio::Input::passwordform
    return cleartxt
  end

  # Encrypt the clear text password
  def Ionlogin::cryptpassword(cleartxt)
    password = BCrypt::Password.create(cleartxt)
    return password
  end

  def Ionlogin::askhint(login)
    question = User[:login => login].pwhint_question
    t = Iontemplate::Template.new('login_prompt_pwhint.ftl')
    t.render("pwhint_question"=>question)
    answer = Ionio::Input::inputform(75)
    dbanswer   = User[:login => login].pwhint_answer

    if answer == dbanswer
      puts "VALIDATED - CLEARING LOGIN FAILURES"
      User.clearfailed(login)
      return true
    else
      puts "INCORRECT"
      return false
    end

  end

  # This function provides the entire login routine logic. Configuration variables apply.
  def Ionlogin::auth
    self.banner
    attempt = 0
    while attempt < @logincfg['attempts'] do
      login = self.userprompt
      unless login.nil?
        password = self.passwordprompt
        thistry = User.authorize(login, password)
        case thistry[:result]
          # If user is validated and allowed, then return true and create a session.
          when "true"
            User.countlogin(login)
            User.clearfailed(login)
            user = User[:login => login]
            
            # Create the global group variable for RBAC
            $group = Group[:id => user.group_id]

            # Create caller history entry & update session
            $callerid = Ioncallhistory.create(
                    :user_id => user.id,
                    :alias  => user.login,
                    :time_login => Time.now
                    )

            timeremain = ($group.dailytimelimit - $callerid.thisuser_time_today)

            $session = Session.new( 
                            :user_id     => user.id, 
                            :username    => user.login,  
                            :group_id    => user.group_id,
                            :level       => $group.level,
                            :created     => Time.now,
                            :caller_id   => $callerid.id,
                            :filearea    => nil,
                            :msgarea     => nil,
                            :chatroom    => nil,
                            :expires     => Time.now + timeremain.minutes,
                            # User preferences go into session so we dont have to query the DB every time.
                            :pref_show_menus => user.pref_show_menus,
                            :pref_term_pager => user.pref_term_pager,
                            :pref_editor     => user.pref_editor 
                            )
            if timeremain <= 0
              Iontemplate::display('login_dailylimit.ftl')
              Telegard::goodbye_fast
            else
              Iontemplate::display('welcome.ftl', {
                      'timetoday' => $callerid.thisuser_time_today.to_s,
                      'grouplimit'=> $group.dailytimelimit.to_s,
                      'logintoday'=> $callerid.thisuser_logincount_today.to_s,
                      'curtime'   => Time.now.to_s,
                      'expires'   => $session.expires.to_s,
                      'timeremain'=> timeremain })

            end
            return true
            break
          when "false"
            attempt += 1
            Iontemplate::display('login_failed.ftl', {"attempt" => "#{attempt.to_s}", "maxattempts" => "#{@logincfg['attempts'].to_s}","failures"=> thistry[:failures].to_s})
            # Test if user.login_failures is equal to 1 less than the configured lockout.
            # If so, prompt user for PWHINT if configured
            if thistry[:failures] == @logincfg['lockout'] && @logincfg['usehint'] == true
              self.askhint(login)
            end
          # -> Chances are, we will NEVER get to this ' when "invalid" ' clause. But if for some reason we do, handle it.
          when "invalid"
            attempt += 1
            print "\n"
            Iontemplate::display('login_invalid.ftl', tvars = {"attempt" => "#{attempt.to_s}", "maxattempts" => "#{@logincfg['attempts'].to_s}"})
          when "locked"
            print "\n"
            Iontemplate::display('login_locked_user.ftl', {"attempt" => "#{attempt.to_s}", "maxattempts" => "#{@logincfg['attempts'].to_s}"})
        end #/case
      else # If login is NIL, its an invalid login. Do some stuff here  (Same as case when invalid above)
        attempt += 1
        print "\n"
        Iontemplate::display('login_invalid.ftl', {"attempt" => "#{attempt.to_s}", "maxattempts" => "#{@logincfg['attempts'].to_s}", "allownew" => "#{@logincfg['allownew']}"})
      end #/unless login.nil?
    end #/while
    # Return nil as a failsafe.
    return nil
  end # end def auth

  # Signup routine for New Users
  def Ionlogin::signup
    require 'lib/ionlogin/signup'
    cfg = $cfg['signup']
    Ionio::ansiclear
    Iontemplate::display('signup_instructions.ftl', {"bbs"=>$cfg['bbs']})
    unless Ionio::Input::inputyn('signup_askcontinue.ftl') == true
      puts "Goodbye!"
      exit 0
    end

    Iontemplate::display('signup_aup.ftl', {"bbs"=>$cfg['bbs']})
    if Ionio::Input::inputyn('signup_accept_aup.ftl') == true
      # Ask required questions
      login     = Ionlogin::Signup::asklogin
      firstname = Ionlogin::Signup::askfirstname
      lastname  = Ionlogin::Signup::asklastname
      email     = Ionlogin::Signup::askemail
      password  = Ionlogin::Signup::askpassword
      pwhint_question  = Ionlogin::Signup::askpwhint_question
      pwhint_answer    = Ionlogin::Signup::askpwhint_answer
      address1  = Ionlogin::Signup::askaddress1
      address2  = Ionlogin::Signup::askaddress2
      city      = Ionlogin::Signup::askcity
      state     = Ionlogin::Signup::askstate
      postal    = Ionlogin::Signup::askpostal
      country   = Ionlogin::Signup::askcountry
      phone     = Ionlogin::Signup::askphone
      gender    = Ionlogin::Signup::askgender
      bday      = Ionlogin::Signup::askbday
      custom1   = Ionlogin::Signup::askcustom(1) if cfg['ask_custom1'] == true
      custom2   = Ionlogin::Signup::askcustom(2) if cfg['ask_custom2'] == true
      custom3   = Ionlogin::Signup::askcustom(3) if cfg['ask_custom3'] == true

      # Send to review screen
      signupdata = {
        "login" => login,
        "password" => password,
        "pwhint_question" => pwhint_question,
        "pwhint_answer" => pwhint_answer,
        "firstname" => firstname,
        "lastname" => lastname,
        "email" => email,
        "city" => city,
        "state" => state,
        "country" => country,
        "address1" => address1,
        "address2" => address2,
        "postal" => postal,
        "phone" => phone,
        "gender" => gender,
        "bday" => bday.to_s,
        "custom1" => custom1,
        "custom2" => custom2,
        "custom3" => custom3,
        "pwexpires" => (Time.now + 90.days).to_s
      }
      Ionlogin::Signup::confirm(signupdata)
    else
      puts "Goodybye!"
      exit 0
    end
    # Send login name back to login. 
    return login
  end #/dev signup



end # => /module

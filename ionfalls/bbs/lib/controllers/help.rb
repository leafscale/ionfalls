=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/help.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Help Menu controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= HelpController
The Help Menu controller.
=end

# Help Controller Class
class HelpController < Ioncontroller

  def initialize
    if $session
      $session.current_area = "Help Menu"
      $session.save
    end  
  end
  
  def menu
    done = false
    validkeys=['A','B','C','F','H','M','P','T','U','S', 'X']
    until done == true
      key = Ionio::Input.menuprompt('menu_help.ftl',validkeys, nil) # nil is for tpl vars hash
      test_session
      print "\n"
      case key
        when "A" # About Telegard         
          Iontemplate.display('help_about.ftl', nil)
        when "B" # BBS Lists Help
          Iontemplate.display('help_bbslists.ftl', nil)          
        when "C" # Chat Rooms Help
          Iontemplate.display('help_chat.ftl', nil)
        when "F" # File Areas Help
          Iontemplate.display('help_fileareas.ftl', nil)
        when "H" # Call History Help
          Iontemplate.display('help_callhistory.ftl', nil)
        when "M" # Messages Areas Help
          Iontemplate.display('help_msgareas.ftl', nil)
        when "P" # Page Sysop Help
          Iontemplate.display('help_pagesysop.ftl', nil)
        when "T" # Timebank Help
          Iontemplate.display('help_timebank.ftl', nil)
        when "U" # User Menu Help
          Iontemplate.display('help_useraccount.ftl', nil)
        when "S" # Command Summary
          Iontemplate.display('help_commands.ftl', nil)
        when "X" # Return to Main
          done = true          
      end #/case
      test_session
    end #/until
    return 0
  end #/def menu
 
end

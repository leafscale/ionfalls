=begin
(*
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/announcement.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Announcement Menu controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= AnnouncementController
The Announcement Menu controller.
=end

# Help Controller Class
class AnnouncementController < Ioncontroller

  def initialize
    if $session
      $session.current_area = "Announcements"
      $session.save
    end
  end
  
  # Pull the announcements and iterate through them
  def read
    Iontemplate.display('announcement_header.ftl')
    # old method of calling the DB directly
    #announcements = Iontemplate::Template.parse_dataset($db[:announcements].order(:priority).all)
    # new method of using the model
    announcements = Iontemplate::Template.parse_modeldata(Ionannouncement.order(:priority).all)
    # iterate over the announcements and pause between them
    announcements.each do |a|
      # Test if an announcement is expired, if so skip it
      if expired?(a['expiration'])
        # If announcements are set for auto_purge in the configuration file, then delete it.          
        if $cfg['announcements']['auto_purge'] == true
            # TODO: Delete from database            
            Ionannouncement.where(:id => a['id']).delete
        end #/auto_purge
      else
        Iontemplate.display('announcement_read.ftl', {'a' => a})
        if $cfg['announcements']['pause_between'] == true
          Ionio.pause
        end #/pause_between      
      end #/expired?
     
    end #/each do
  end #/def

  # Test if an announcement has expired
  def expired?(expiration)
      if expiration.nil?
          return false
      end      
      if expiration > Date.today.to_java # Sequel will returns H2 dates as type JavaUtil::Date         
      return false
    else
      return true
    end
  end
  
  
  #TODO: Define this section for Announcement as it is was copy of the Help controller
  def menu
    done = false
    validkeys=['R','X']
    until done == true
      key = Ionio::Input.menuprompt('menu_announcement.ftl',validkeys, nil) # nil is for tpl vars hash
      test_session
      print "\n"
      case key
        when "R" # Read
          self.read
        when "X" # Return to Main
          done = true          
      end #/case
      test_session
    end #/until
    return 0
  end #/def menu
 
end

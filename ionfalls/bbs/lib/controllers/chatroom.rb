=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/chatroom.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: ChatRoom controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= ChatRoom Controller
The Chat Room Controller
=end

class ChatroomController < Ioncontroller

  def initialize
    if $session
      $session.current_area = "Chat Rooms"
      $session.save
    end
  end
  
  def menu
    done = false
    validkeys=['X','L']
    until done == true
      key = Ionio::Input.menuprompt('menu_chatroom.ftl',validkeys, nil) # nil is for tpl vars hash
      print "\n"
      case key
        when "X"  # Exit menu
          done = true
          return 0
        when "L"  # List rooms
          rooms = Iontemplate::Template.parse_modeldata(Ionchatroom.roomlist)
          Iontemplate.display('chatrooms_list_rooms.ftl', {'rooms' => rooms})
      end #/case
    end #/until
  end #/def menu
 
end

=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/message.rb
#     Version: 0.06
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Message controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= MessageController
The Message controller.
=end


class MessageController < Ioncontroller
 
  def initialize(curarea)
    @areameta = Ionmsgarea.where(:id => curarea).first
    # Create instance of Area's DB
    dbcfg = $cfg['database']
    @areadb = Sequel.connect("jdbc:h2:tcp://localhost/msgareas/#{@areameta[:id]}", :user => dbcfg['user'], :password=>Security::ConfigPassword.new.decrypt(dbcfg['pass']))
    unless @areadb.table_exists?(:msgs) == true
      @areadb.create_table :msgs do
        primary_key  :id              # Message ID
        integer      :msgarea_id      # Message Area ID
        integer      :from_id         # ID of sender
        String       :from            # From Header
        String       :to              # To Header
        String       :cc              # CC Header
        String       :subject         # Subject Header
        text         :body            # Message
        TimeStamp    :composed        # Message composed timestamp
        TimeStamp    :received        # Message received timestamp
        array        :read_by         # List of user_id's that have read this item
      end
    end
    if $session
      $session.current_area = "Message area #{@areameta['name']}"
      $session.save
    end  
  end

  def uninitialize
    # Disconnect and destroy instance
    @areadb.disconnect
    @areadb = nil
  end

  def msgcount
    @areadb[:msgs].count
  end
  
  
  # Display summary listing of ALL message areas.
  def listall
    # Connect to message area database
    #areadb = Sequel.connect("jdbc:h2:tcp://localhost/msgareas/#{areameta[:id]}", :user => db['user'], :password=>Security::ConfigPassword.new.decrypt(db['pass']))
    # Check if message table exists. If not, create it
    msglist = Iontemplate::Template.parse_dataset(@areadb[:msgs].select(:id, :subject, :from, :composed).all)
    Iontemplate.display('msgarea_list_msgs.ftl', {'msgs' => msglist, 'areaname' => @areameta[:name].capitalize})
  end

  # View current message
  def view(msgmeta)
    Iontemplate.display('msg_display.ftl', {"from"=> msgmeta[:from],
                                           "subject"=>msgmeta[:subject],
                                           "composed" => msgmeta[:composed],
                                           "body" => msgmeta[:body]})
  end

  # Post a new message 
  def post
    Iontemplate.display('msgarea_confirm_postnew.ftl', {'areaname'=>@areameta[:name].capitalize})
    if Ionio::Input::inputyn == true
      Iontemplate.display('msgarea_post_asksubject.ftl')
      subject = Ionio::Input.inputform(70)
      body = Ionio::Ionedit::edit(nil, subject)
      # TODO : Enable error checking via try/catch
      unless body == nil
        #TODO: Ask user to confirm they are happy with the post before saving
        msg = @areadb[:msgs]
        msg.insert(:msgarea_id => @areameta[:id],
                   :from_id => $session.user_id,
                   :from => $session.username,
                   :subject => subject,
                   :body => body,
                   :composed => Time.now
                )
        Iontemplate.display('msgarea_post_success.ftl')
        # TODO: Update msgarea table with posting user and timestamp
      end
    end
  end

  # Add quote notation to original message for use in reply
  def quote(original)
    quoted = "> ---------------------------------------------------------------------\n> Original Message : #{original[:composed]} by #{original[:from]}\n\n"
      original[:body].split("\n").each do |line|
      quoted = quoted + "> "+line + "\n"
      end
    quoted = quoted + "> ---------------------------------------------------------------------\n"
    return quoted
  end

    # Reply to a new message
  def reply(msgid)
    Iontemplate.display('msgarea_confirm_postreply.ftl')
    if Ionio::Input::inputyn == true
      original = @areadb[:msgs].where(:id=>msgid).first
      # See if the "RE: " has been prepended already, if not add it.
      unless original[:subject][0,4] == "RE: "
        subject = "RE: "+original[:subject]
      else
        subject = original[:subject]
      end

      Iontemplate.display('msgarea_reply_include_original.ftl')
      if Ionio::Input::inputyn == true
        body = Ionio::Ionedit::edit(quote(original), subject)
      else
        body = Ionio::Ionedit::edit(nil, subject)
      end

      # TODO : Enable error checking via try/catch  (same as 'post')
      unless body == nil
        #TODO: Ask user to confirm they are happy with the post before saving
        msg = @areadb[:msgs]
        msg.insert(:msgarea_id => @areameta[:id],
                   :from_id => $session.user_id,
                   :from => $session.username,
                   :subject => subject,
                   :body => body,
                   :composed => Time.now
                )

      end
      Iontemplate.display('msgarea_post_success.ftl')
      # TODO: Update msgarea table with posting user and timestamp
    end
  end

  # Allows a user to navigate (read) message area.
  def browse
    done = false
    validkeys=['?','J','L','P','R','S','V',']','[','X','1','0']
    msgindex = 0
    msglist = @areadb[:msgs].map(:id).reverse
    # If the msglist is empty, the reader will crash, so we either ask user to post or quit
    if msglist.empty? == true
      Iontemplate.display('msgarea_empty.ftl')
      post
      return 0
    end

    # Start with most recent post first
    curmsg = msglist.first
    Iontemplate.display('menu_msgarea_browser.ftl')
    until done == true
      msglist = @areadb[:msgs].map(:id).reverse
      msgcount = msglist.count
      msgmeta = @areadb[:msgs].where(:id => curmsg).first
      key = Ionio::Input.menuprompt('menu_msgarea_summary.ftl',validkeys, {"msgnum" => msgmeta[:id].to_s, "subject" => msgmeta[:subject].to_s, "msgdate" => msgmeta[:composed].to_s})
      print "\n"
      case key
        when 'V' # View message
          view(msgmeta)
        when 'R' # Reply
          reply(curmsg)
        when ']' # Goto Next
          if msgindex > (msgcount - 2)
            msgindex = 0
          else
            msgindex += 1
          end
          curmsg = msglist.at(msgindex)

        when '[' # Goto Previous
          if msgindex <= (0)
            msgindex = (msglist.count - 1)
          else
            msgindex -= 1
          end
          curmsg = msglist.at(msgindex)

        when 'P' # Post new message in area
          post
        when 'L' # List Messages
          listall
        when 'J' # Jump to messages Num
          Iontemplate.display('msg_jumpto.ftl', nil)
          jumper = Ionio::Input::inputform(4).to_i
          # Validate input for correctness before switching areas
          if msglist.include?(jumper) == true
            curmsg = jumper
          else
            Iontemplate.display('msg_jumpto_invalid.ftl', nil)
          end

        when '1' # Goto First Message in Area
          curmsg = msglist.last  # yes this is ''last'' because we get the msglist in reverse sorting
        when '0' # Goto Last Message in Area
          curmsg = msglist.first # yes this is ''first'' because we get the msglist in reverse sorting
        when '?'
          Iontemplate.display('menu_msgarea_browser.ftl')
        when 'S'
          Ionfalls.unimplemented
        when 'X'
          done=true
          uninitialize
      end

    end
  end

  

end

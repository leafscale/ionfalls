=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/main.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Main Menu controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= MainController
The Main Menu master controller.
=end

# Main Controller Class
class MainController < Ioncontroller

  def initialize
    if $session
      $session.current_area = "Main Menu"
      $session.save
    end  
  end
  
  def menu
    done = false
    validkeys=['A','B','C','E','F','G','H','M','P','T','U','?']
    until done == true
      key = Ionio::Input.menuprompt('menu_main.ftl',validkeys, nil) # nil is for tpl vars hash
      test_session
      if $session
        $session.current_area = "Main Menu"
        $session.save
      end  
      print "\n"
      case key
        when "A" # AutoMessage
          AnnouncementController.new.menu
        when "B" # BBS Lists
          BBSlistController.new.menu
        when "C" # Chat Rooms
          ChatroomController.new.menu
        when "E" # Email Tools
          #EmailController.new.menu
          Ionfalls.unimplemented
        when "F" # File Areas
          FileAreaController.new.menu
        when "G" # Good Bye
          Ionfalls::goodbye          
        when "H" # Call History
          CallHistoryController.new.menu
        when "M" # Messages Areas
          MsgareaController.new.menu
        when "P" # Page Sysop
          Ionfalls.unimplemented
        when "T" # Timebank
          TimebankController.new.menu
        when "U" # User Menu
          UserController.new.menu
        when "?" # User Help
          HelpController.new.menu
      end #/case
      test_session
    end #/until
  end #/def menu
 
end

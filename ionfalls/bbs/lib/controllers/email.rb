=begin

===============================================================================
                 IonFalls (Ionfalls/2)  http://www.ionfalls.org                    
===============================================================================

See "LICENSE" file for distribution and copyright information. 
 
---[ File Info ]-------------------------------------------------------------

 Source File: /lib/controllers/email.rb
     Version: 1.00
   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
 Description: Email controller.

-----------------------------------------------------------------------------
=end

=begin rdoc
= EmailController
The Email controller.
=end

class EmailController < Ioncontroller

  # Email main menu handler
  def menu
    done = false
    validkeys=['A','C','I','O','S','X']
    until done == true
      unread = Ionemail.filter(:user_id => $session.user_id).count
      key = Ionio::Input.menuprompt('menu_email.ftl',validkeys, {"unread" => unread.to_s}) 
      print "\n"
      case key
        when "A" # List archived mail
          Ionfalls.unimplemented
        when "C" # Compose new mail
          Ionfalls.unimplemented
        when "I" # Inbox navigator
          self.navigator('inbox')
        when "O" # List Outbox
          Ionfalls.unimplemented
        when "S" # Search Email
          Ionfalls.unimplemented
        when "X" # Return to main
          done = true
          return 0
      end #/case
    end #/until
  end #/def menu

  # Email Navigator - a generic interface to reading email messages. Function takes a "folder" name
  def navigator(folder)
    done = false
    validkeys=['A','D','F','H','L','R','V','X', '?']
    until done == true
      unread = Ionemail.filter(:user_id => $session.user_id).count
      key = Ionio::Input.menuprompt('menu_email_inbox.ftl',validkeys, {"unread" => unread.to_s}) 
      print "\n"
      case key
        when "A" # Archive
        when "L" # List Inbox
          self.inbox_list
        when "X" # Return to email menu
          done = true
          return 0
      end #/case
    end #/until
  end #/def

  # List contents of the user's Inbox
  def inbox_list
    inbox =  Iontemplate::Template.parse_dataset($db[:email].filter(:user_id => $session.user_id).all)
    Iontemplate.display('email_list_inbox.ftl', {'emails' => inbox})
  end
  
  # Interactive Message Reader
  def read_msg

  end

end

=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/chat.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Chat controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= ChatController
The Chat Menu controller.
=end

class ChatController < Ioncontroller

  def menu
    done = false
    validkeys=['X']
    until done == true
      key = Ionio::Input.menuprompt('menu_chat.ftl',validkeys, nil) # nil is for tpl vars hash
      print "\n"
      case key
        when "X"
          done = true
          return 0
      end #/case
    end #/until
  end #/def menu
  
end

=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/bbslist.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: BBSList Controller
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= HelpController
The BBS Lists menu controller.
=end

# Help Controller Class
class BBSlistController < Ioncontroller

  def initialize
    if $session
      $session.current_area = "BBS Lists"
      $session.save
    end  
  end
 
  
  def menu
    done = false
    validkeys=['A','L','S','X']
    until done == true
      key = Ionio::Input.menuprompt('menu_bbslist.ftl',validkeys, nil) # nil is for tpl vars hash
      test_session
      print "\n"
      case key
        when "A" # Add to local directory
          if Ionio::Input::inputyn('bbslist_askstart.ftl') == true
            self.add
          end
        when "L" # List local directory
          #bbslist = Iontemplate::Template.parse_dataset($db[:bbslist].all)
          bbslist = Iontemplate::Template.parse_modeldata(Ionbbslist.all)  
          Iontemplate.display('bbslist_list_local.ftl', {'bbslist' => bbslist})
        when "S" # Search
          Ionfalls.unimplemented

        when "X" # Return to Main
          done = true          
      end #/case
      test_session
    end #/until
    return 0
  end #/def menu

  # Adds an entry to the local bbs list.
  def add
     listdata = { 'bbsname' => askbbsname,
                  'description' => askdescription,
                  'sysopname' => asksysopname,
                  'bbsurl' => askbbsurl,
                  'homepage' => askhomepage }
     done = false
     validkeys=['B','D','H','S','U','X','.']
     until done == true
       key = Ionio::Input.menuprompt('bbslist_confirmadd.ftl',validkeys, listdata) # nil is for tpl vars hash
       print "\n"
       case key
         when "B" # BBS Name
           listdata['bbsname'] = askbbsname
         when "D" # Description
           listdata['description'] = askdescription
         when "H" # Homepage
           listdata['homepage'] = askhomepage
         when "S" # Sysop Name
           listdata['sysopname'] = asksysopname
         when "U" # URL
           listdata['bbsurl'] = askbbsurl
         when "." # Quit NO SAVE
           return 0
         when "X" # Quit + SAVE
           Ionbbslist.create(
                   :bbsname => listdata['bbsname'],
                   :description => listdata['description'],
                   :sysopname => listdata['sysopname'],
                   :bbsurl => listdata['bbsurl'],
                   :homepage => listdata['homepage'],
                   :submitted_by => $session.username,
                   :created => Time.now 
                   )
           return 0

       end
     end
  end #/def add

  private
  # Input: BBS Name
  # TODO: Check for duplicates.
  def askbbsname(val=nil)
      complete = false
      until complete == true
        bbsname = Ionio::question('bbslist_askbbsname.ftl', 30)
        unless bbsname.is_blank? || bbsname.minlength?(3) == false
          complete = true if bbsname.is_spaced_alphanumeric?
        end #/if
      end #/until
      return bbsname
    end #/def askbbsname

  # Input: BBS Description
  def askdescription
    complete = false
    until complete == true
      description = Ionio::question('bbslist_askdescription.ftl', 75)
      unless description.is_blank? || description.minlength?(3) == false
        complete = true
      end #/if
    end #/until
    return description
  end #/def askbbsdescription

  # Input: BBS Sysop Name
  def asksysopname(val=nil)
      complete = false
      until complete == true
        sysopname = Ionio::question('bbslist_asksysop.ftl', 30)
        unless sysopname.is_blank? || sysopname.minlength?(3) == false
          complete = true if sysopname.is_spaced_alphanumeric?
        end #/if
      end #/until
      return sysopname
    end #/def asksysopname

  # Input: BBS Homepage - where information about the BBS can be found online
  def askhomepage(val=nil)
      complete = false
      until complete == true
        homepage = Ionio::question('bbslist_askhomepage.ftl', 75)
        # Homepage can be Optional, so either allow blank or validurl
        if homepage.is_blank? || homepage.is_url? == true
          complete = true
        end #/if
      end #/until
      return homepage
    end #/def askhomepage

  # Input: BBS URL format of  proto://user:pass@server 
  # example: (ssh://bbs:bbs@prod.ionfalls.org)
  def askbbsurl
      complete = false
      until complete == true
        bbsurl = Ionio::question('bbslist_askbbsurl.ftl', 75)        
        unless bbsurl.is_blank? || bbsurl.minlength?(10) == false
          complete = true
        end #/if
      end #/until
      return bbsurl
    end #/def askbbsurl


end

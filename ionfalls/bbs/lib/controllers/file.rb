=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/file.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: File controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= FileController
The File controller.
=end

class FileController < Ioncontroller

  def initialize(curarea)
    @areameta = Ionfilearea.where(:id => curarea).first
    # Create instance of Area's DB
    dbcfg = $cfg['database']
    @areadb = Sequel.connect("jdbc:h2:tcp://localhost/fileareas/#{@areameta[:id]}", :user => dbcfg['user'], :password=>Security::ConfigPassword.new.decrypt(dbcfg['pass']))
    unless @areadb.table_exists?(:files) == true
      @areadb.create_table :files do
        primary_key  :id              # File ID
        integer      :ionfilearea_id   # Area ID
        integer      :owner_id        # File Owner (maintainer)
        integer      :uploaded_by     # User who submitted file
        integer      :approved_by     # User who approved file (if any)
        boolean      :enabled         # File available for download?
        String       :filename        # Filename
        String       :name            # Friendly Short Name
        text         :description     # Long description
        text         :checksum        # Some Checksum Value (MD5/SHA,etc)
        String       :version         # Version of file (optional)
        String       :vendor          # Vendor of the file (optional)
        String       :license         # License file distributed under (optional)
        String       :url             # URL for more information (optional)
        integer      :size            # Size of file in bytes on storage
        integer      :downloaded      # Number of downloads
        TimeStamp    :mtime           # File's mtime on storage
        TimeStamp    :created_at      # Time file added to DB
        TimeStamp    :modified_at     # Time file modified in DB
      end
    end     
    $session.current_area = "File Area - #{@areameta[:name]}"
    $session.save  
  end

  def uninitialize
    # Disconnect and destroy instance
    @areadb.disconnect
    @areadb = nil
  end

  # Get a listing of files in this area
  def listfiles
    filelisting =  @areadb[:files].filter(:enabled => true).all
    return filelisting
  end

# Commented out menu, does a menu need to be here? probably not
=begin
  def menu
    done = false
    validkeys=['X']
    until done == true
      key = Ionio::Input.menuprompt('menu_file.ftl',validkeys, nil) # nil is for tpl vars hash
      print "\n"
      case key
        when "X"
          done = true
          return 0
      end #/case
    end #/until
  end #/def menu
=end

end

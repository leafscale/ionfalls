=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/user.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: User controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= UserController
The User controller handles user self-management functions.
=end

class UserController < Ioncontroller

  def initialize
    if $session  
      $session.current_area = "User Menu"
      $session.save
    end  
  end
 
    
  # Master menu for User Controller
  def menu
    done = false
    validkeys=['A','E','S','P','R','X']
    until done == true
      key = Ionio::Input.menuprompt('menu_user.ftl',validkeys, nil) # nil is for tpl vars hash
      print "\n"
      case key
        when "A" # Account summary (statistics)
          self.summary
        when "E" # Edit Account
          self.edit
        when "S" # Search for Users
          Ionfalls.unimplemented
        when "P" # Edit Preferences
          self.preferences
        when "R" # Reset Password
          require 'lib/ionlogin/signup'  
          newpw = Ionlogin::Signup.askpassword
          User.filter(:id => $session.user_id).update(:password => User.cryptpassword(newpw))
        when "X" # Return to main
          return 0
      end #/case
    end #/until
  end #/def menu

  # Edit account menu
  def edit
    require 'lib/ionlogin/signup'
    curuser = User.where(:id => $session.user_id).first
    done = false
    validkeys=['A','C','D','E','F','L','Q','S','Y','Z','.','X']
    until done == true
      key = Ionio::Input.menuprompt('user_edit_account.ftl',validkeys, { "user"=> Iontemplate::Template.parse_hash(curuser.values)})
      print "\n"
      case key
        when "A" # Password Hint Answer
          curuser.pwhint_answer = Ionlogin::Signup.askpwhint_answer
        when "C" # City
          curuser.city = Ionlogin::Signup.askcity
        when "D" # Address lines 1 & 2
          curuser.address1 = Ionlogin::Signup.askaddress1
          curuser.address2 = Ionlogin::Signup.askaddress2
        when "E" # Email
          curuser.email = Ionlogin::Signup.askemail
        when "F" # Firstname
          curuser.firstname = Ionlogin::Signup.askfirstname
        when "L" # Lastname
          curuser.lastname = Ionlogin::Signup.asklastname
        when "Q"
          curuser.pwhint_question = Ionlogin::Signup.askpwhint_question
        when "S" # State
          curuser.state = Ionlogin::Signup.askstate
        when "Y" # Country
          curuser.country = Ionlogin::Signup.askcountry
        when "Z" # Postal Code
          curuser.postal = Ionlogin::Signup.askpostal
        when "X" # Quit without saving
          return 0
        when "." # Return to main
          # Save user changes
          curuser.save
          return 0
      end #/case
    end #/until
  end #/def edit

  # User Preferences menu
  def preferences
    curuser = User.where(:id => $session.user_id).first
    done = false
    validkeys=['C','E','H','W','P','M','F','X','.']
    until done == true
      key = Ionio::Input.menuprompt('user_preferences.ftl',validkeys, { "user"=> Iontemplate::Template.parse_hash(curuser.values)})
      print "\n"
      case key
        when "C" # Terminal Color
          curuser.pref_term_color = curuser.pref_term_color.toggle
        when "E" # Editor
          #curuser.pref_editor = select_editor
          Ionfalls.unimplemented
        when "H" # Terminal Height
          Ionfalls.unimplemented
          #curuser.pref_term_height = Ionlogin::Signup.asktermheight
        when "W" # Terminal Width
          Ionfalls.unimplemented
          #curuser.pref_term_width = Ionlogin::Signup.asktermwidth
        when "P" # Pager
          curuser.pref_term_pager = curuser.pref_term_pager.toggle
        when "M" # Menus
          curuser.pref_show_menus = curuser.pref_show_menus.toggle
        when "F"
          curuser.pref_fastlogin = curuser.pref_fastlogin.toggle
        when "X" # Quit without saving
          return 0
        when "." # Return to main
          # Save user changes
          curuser.save
          return 0
      end #/case
    end #/until
  end #/def preferences

  # Lets the user select their preferred Text Editor
  def select_editor
    done = false
    validkeys=['1']
    until done == true
      key = Ionio::Input.menuprompt('user_select_editor.ftl',validkeys,nil)
      print "\n"
      case key
        # Ionfalls Edit (this is deprecated)
        when "0" # IonFalls AnsiEdit (unfeatured line editor)
          $session.pref_editor = 'ansiedit'
          $session.save
          return 'ansiedit'

        when "1" # IonFalls IonEditor (FullScreen Editor)
          $session.pref_editor = 'ioneditor'
          $session.save
          return 'ioneditor'
        #when "2" # reserved
        #when "3" # reserved
        #when "4" # reserved
        #when "5" # reserved
      end #/case
      return 0
    end #/until

  end

  # Displays the user's account summary & statistics information.
  def summary
    curuser = User.where(:id => $session.user_id).first

    done = false
    validkeys=['X']
    until done == true
      key = Ionio::Input.menuprompt('user_account_summary.ftl',validkeys, { "user"=> Iontemplate::Template.parse_hash(curuser.values), "groupname" => $group.name })
      print "\n"
      case key
        # Ionfalls Edit (this is deprecated)
      when "0" # Ionfalls Editor
      end #/case
      return 0
    end #/until
  end
end

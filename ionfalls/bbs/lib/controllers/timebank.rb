=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/timebank.rb
#     Version: 0.06
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Timebank controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= TimebankController
The Timebank controller manipulates the user's timebank balance. It provides
a menu for the user to check balance, withdraw, deposit and offer time to 
other users. 

The Timebank is stored in 'Users'.'timebank' in the database.

=end

class TimebankController < Ioncontroller

  def initialize
    if $session  
      $session.current_area = "Timebank"
      $session.save
    end  
  end
  
  def menu
    done = false
    validkeys=['B','D','O','W','X']
    until done == true
      key = Ionio::Input.menuprompt('menu_timebank.ftl',validkeys, nil) # nil is for tpl vars hash
      print "\n"
      case key
        when "B" # Show Time Bank Balance
          self.showbalance
        when "D" # Deposit
          self.deposit
        when "G" # Give time slices to other users
        when "W" # Withdraw from timebank
          self.withdraw
        when "X" # Quit time bank menu
          return 0
      end #/case
    end #/until
  end #/def menu

  # Show balance for the current user 
  def showbalance
    curuser = User.where(:id => $session.user_id).first
    Iontemplate.display('timebank_balance.ftl', {'balance' => curuser.timebank.to_s})
  end 

  # User makes a deposit in the bank
  def deposit
    # TODO: Check the GROUP limits and enforce
    maxbalance = $cfg['limits']['timebank_max']
    curuser = User.where(:id => $session.user_id).first
    balance = curuser.timebank
    remain = $session.timeremain
    amount = self.askamount

    # Verify the amount deposited is available from the current session and will not exceed the system defined limit
    if (balance + amount > maxbalance)
      Iontemplate.display('timebank_limit_exceeded.ftl', {'amount' => amount.to_s, 'maxbalance'=>maxbalance.to_s})
    elsif (amount > remain )
      Iontemplate.display('timebank_insufficent_funds.ftl', {'amount' => amount.to_s, 'remain'=>remain.to_s})
    else
      # If ok, then deduct from the session & update the user's timebank balance.
      $session.timeadjust(-amount)
      curuser.timebank = (curuser.timebank + amount)
      Iontemplate.display('timebank_deposit.ftl', {'amount' => amount.to_s, 'remain'=>$session.timeremain.to_s})
      curuser.save
    end
  end

  # User withdraws from the bank
  def withdraw
    curuser = User.where(:id => $session.user_id).first
    balance = curuser.timebank
    amount = self.askamount
    # Verify the user has enough available balance
    if amount > balance
      Iontemplate.display('timebank_insufficent_funds.ftl', {'amount' => amount.to_s, 'remain'=>balance.to_s})
    else
      # If ok, then withdraw from the bank balance & update the user's session time remaining.
      curuser.timebank = (curuser.timebank - amount)
      $session.timeadjust(+amount)
      Iontemplate.display('timebank_withdraw.ftl', {'amount' => amount.to_s, 'remain'=>$session.timeremain.to_s})
      curuser.save
    end
  end

  # Asks user for the amount to be deposit/withdraw/lend
  def askamount(val=nil)
    complete = false
    until complete == true
      amount = Ionio::question('timebank_askamount.ftl', 3, 'posint')
      unless amount.is_blank?
        complete = true if amount.is_numeric?
      end #/if
    end #/until
    return amount.to_i
end #/def askpostal

end

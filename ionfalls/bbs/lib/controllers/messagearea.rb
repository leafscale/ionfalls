=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/messagearea.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: MessageArea controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= MessageAreaController
The Message Area controller.
=end


class MsgareaController < Ioncontroller

  def initialize
    if $session  
      $session.current_area = "Message Areas"
      $session.save
    end  
  end
     

  def menu
    done = false
    validkeys=['A','G','I','J','L','O','R','S',']','[','X']
    until done == true
      # Retrieve a list and count of Message Areas - This is done each menu loop in the event soemthing changes
      # by an admin during the user's session. Potentially, a change could cause the controller to crash.
      arealist  = Ionmsgarea.where(:enabled => true).select(:id).order(:id).map(:id)
      areacount =  Ionmsgarea.count
      # Track the user's current area from the session table.
      if $session.msgarea.nil? == true
        curarea = arealist.first
        $session.msgarea = curarea
        $session.save
      else
        curarea = $session.msgarea
      end
      # Set the array iterator index location
      areaindex = arealist.index(curarea)
      # Get area's metadata info
      areameta = Ionmsgarea.where(:id => curarea).first
      # Get a count of total msgs in this area
      msgcount = MessageController.new(curarea).msgcount
      # Display menu
      key = Ionio::Input.menuprompt('menu_msgarea.ftl',validkeys, {"areanum" => areameta[:id].to_s,"areaname" => areameta[:name].capitalize, "areadesc" => areameta[:description], "msgcount" => msgcount })
      print "\n"
      case key
        when "A" # List all Message Area Names/Descriptions
          areas = Iontemplate::Template.parse_modeldata(Ionmsgarea.where(:enabled => true).all)
          Iontemplate.display('msgarea_list_areas.ftl', {'areas' => areas})

        when "G" # Message Reader
          Ionfalls.goodbye

        when "R" # Message Reader
          MessageController.new(curarea).browse

        when "I" # Area Information
          Iontemplate.display('msgarea_area_meta.ftl', {'area' => Iontemplate::Template.parse_hash(areameta.values), 'msgcount' => msgcount})
        when "J" # Jump to another area
          Iontemplate.display('msgarea_jumpto.ftl', nil)
          jumper = Ionio::Input::inputform(4).to_i
          # Validate input for correctness before switching areas
          if arealist.include?(jumper) == true
            $session.msgarea = jumper
            $session.save
          else
            Iontemplate.display('msgarea_jumpto_invalid.ftl', nil)
          end

        when "L" # List messages in current area
          if $session.level >= areameta[:minlevel_read]
            MessageController.new(curarea).listall
          else
            Iontemplate.display('msgarea_read_forbidden.ftl', {'areaname'=>areameta[:name].capitalize})
          end

        when "P" # Post Area
            MessageController.new(curarea).post

        when "S" # Search for Message in All Areas
          Ionfalls.unimplemented

        when "]" # Next Area
          if areaindex > (areacount - 1)
            $session.msgarea = arealist.first
          else
            $session.msgarea = arealist.at(areaindex + 1)
          end
          $session.save

        when "[" # Previous Area
          if areaindex <= 0
            $session.msgarea = arealist.last
          else
            $session.msgarea = arealist.at(areaindex - 1)
          end
          $session.save

        when "X"
          return 0

      end #/case
    end #/until
  end #/def menu

end

=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/admin.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Administration Menu controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= AdminController
The Administration Menu controller.
=end

# Help Controller Class
class AdminController < Ioncontroller

  def initialize
    if $session
      $session.current_area = "System Administration Menu"
      $session.save
    end  
  end

  # Creates a list of group names and creates a selectable menu array
  def selectgroup
    # Generates a list of groups in an array, and numerically presents them for selection
    data = Iontemplate::Template.parse_modeldata(Group.select(:name).all)
    grouplist = Array.new
    data.each do |g|
      grouplist.push(g["name"])
    end
    done = false
    until done == true
      Iontemplate.display('admin_select_group.ftl', { 'groups' => grouplist} )
      value = Ionio::Input.inputform(2,'posint').to_i
      if value >= 0 and value < grouplist.count
        done = true
      end
    end
    return grouplist[value]
  end

  # Main Menu for Admin panel  
  def menu
    done = false
    validkeys=['A','B','C','F','G','H','M','U','X']
    until done == true
      key = Ionio::Input.menuprompt('admin_menu.ftl',validkeys, nil)
      print "\n"
      case key
        when "A" # BBS Configuration
          self.announcements
        when "B" # BBS Configuration
          self.bbsconfig
        when "C" # Chat configuration
          self.chatconfig
        when "F" # Files Area configuration
          self.fileareasconfig
        when "H" # Help on admin tool
          Iontemplate.display('admin_help.ftl', nil)
          Ionio::pause
        when "M" # Message Area configuration
          self.msgareasconfig
        when "U" # User Manager
          self.usermanager
        when "G" # Group Manager          
          self.groupmanager
        when "X" # Return to Main
          done = true          
      end #/case
      #test_session
    end #/until
    return 0
  end #/def menu

  
#=============================================================================#
# BBS Configuration Menu                                                      #
#=============================================================================#    
  def bbsconfig
    done = false
    validkeys=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','X']
    until done == true
      key = Ionio::Input.menuprompt('admin_bbsconfig.ftl',validkeys, $cfg)
      print "\n"
      case key
        when "A" # Toggle System Enable/Disable
          self.bbsconfig_enabled
        when "B" # Set BBS Name
          self.bbsconfig_name
        when "C" # Set BBS Name
          self.bbsconfig_tagline
        when "D" # Set Theme
          self.bbsconfig_theme
        when "E" # Login: Attempts
          self.bbsconfig_login_attempts
        when "F" # Login: Lockout
          self.bbsconfig_login_lockout
        when "G" # Login: Hints
          self.bbsconfig_login_hints
        when "H" # Login: Allow New Users
          self.bbsconfig_login_allownew
        when "I" # Signup: Default Group
          self.bbsconfig_signup_group
        when "J" # Signup: Customize Form
          self.bbsconfig_signup_form
        when "K" # Feature: Announcement
          self.bbsconfig_feature_announcements
        when "L" # Feature: BBS listings
          self.bbsconfig_feature_bbslists
        when "M" # Feature: Time Bank
          self.bbsconfig_feature_timebank
        when "N" # Limit: Time Bank Max
          self.bbsconfig_limit_timebank
        when "O" # Editors: Default
          self.bbsconfig_editor_default
        when "P" # Editors: Allow ansiedit
          self.bbsconfig_editor_ansiedit
#        when "Q" # System Limits menu
#          self.bbsconfig_limits          
        when "Q" # Notifications
          self.bbsconfig_notifications
        when "X" # Return to Main
          done = true          
      end #/case
      # Refresh the configuration file.
      $cfg = Ionconfig.load
      #test_session
    end #/until
    return 0
  end
  
  # Toggle the BBS
  def bbsconfig_enabled
    if Ionio::Input.inputyn('admin_bbsconfig_enabled.ftl') == true
      $cfg['bbs']['enabled'] = $cfg['bbs']['enabled'].toggle
      Ionconfig.save($cfg)    
    end   
  end

  # Change the BBS name
  def bbsconfig_name
    $cfg['bbs']['name'] = Ionio::Input.inputform(25)
    Ionconfig.save($cfg)  
  end

  # Change the BBS Tagline
  def bbsconfig_tagline
    $cfg['bbs']['tagline'] = Ionio::Input.inputform(64)
    Ionconfig.save($cfg)
  end

  # Choose the BBS Theme  
  def bbsconfig_theme
    # Discover theme directories
    tpls = Array.new
    dirlist = Dir.new(TPL_DIR)
    dirlist.each do |d|
      if File.directory?(TPL_DIR+d)
        unless d == "." or d == ".."  # Ignore hierarchy shortcuts
          tpls.push(d)
        end        
      end
    # TODO: Generate a template and input method to select the theme  
    end #/ do
    hashdata = { "tpls" => tpls }
    Iontemplate.display('admin_bbsconfig_theme.ftl', hashdata)    
    value = Ionio::Input.inputform(2,'posint').to_i
    if value >= 0 and value < tpls.count
      $cfg['bbs']['theme'] = tpls[value]
      Ionconfig.save($cfg)
    end
  end
  
  # Set the max number of login attempts per connection
  def bbsconfig_login_attempts
    value = Ionio::Input.inputrange('admin_bbsconfig_loginattempts.ftl', RANGE_LOGIN_ATTEMPTS) 
    if value != nil
      $cfg['login']['attempts'] = value.to_i
      Ionconfig.save($cfg)
    end    
  end
  
  # Set the number of failed logins per user before account gets locked
  def bbsconfig_login_lockout
    value = Ionio::Input.inputrange('admin_bbsconfig_loginattempts.ftl', RANGE_LOGIN_LOCKOUT) 
    if value != nil
      $cfg['login']['lockout'] = value.to_i
      Ionconfig.save($cfg)
    end  
  end
  
  # Toggle whether to allow login hints for failed attempts
  def bbsconfig_login_hints
    $cfg['login']['usehint'] = $cfg['login']['usehint'].toggle
    Ionconfig.save($cfg)    
  end
  
  # Toggle if this system allows new user signups
  def bbsconfig_login_allownew
    $cfg['login']['allownew'] = $cfg['login']['allownew'].toggle
    Ionconfig.save($cfg)    
  end
  
  # Which group do new users get placed into by default
  def bbsconfig_signup_group
      $cfg['signup']['default_group'] = selectgroup
      Ionconfig.save($cfg)
  end
  
  # Enable/Disable Announcements feature
  def bbsconfig_feature_announcements
    $cfg['feature']['announcements'] = $cfg['feature']['announcements'].toggle
    Ionconfig.save($cfg)    
  end
  
  # Enable/Disable BBS Listings Feature
  def bbsconfig_feature_bbslists
    $cfg['feature']['bbslist'] = $cfg['feature']['bbslist'].toggle
    Ionconfig.save($cfg)    
  end
  
  # Enable/Disable Time Banking feature
  def bbsconfig_feature_timebank
    $cfg['feature']['timebank'] = $cfg['feature']['timebank'].toggle
    Ionconfig.save($cfg)    
  end
  
  # Set timebank maximum limit
  def bbsconfig_limit_timebank
    value = Ionio::Input.inputrange('admin_bbsconfig_timebankmax.ftl', RANGE_TIMEBANK_MAX) 
    if value != nil
      $cfg['limits']['timebank_max'] = value.to_i
      Ionconfig.save($cfg)
    end #/if 
  end #/def
  
  # Default editor for writing messages
  def bbsconfig_editor_default
    # TODO: 
    # This feature cannot be changed in this version.    
  end
  
  # Allow the ansiedit
  def bbsconfig_editor_ansiedit
    $cfg['editors']['allow_ansiedit'] = $cfg['editors']['allow_ansiedit'].toggle
    Ionconfig.save($cfg)    
  end

  # Signup Form Customization menu
  def bbsconfig_signup_form
    done = false
    validkeys=['A','B','C','D','E','F','G','H','I','1','2','3','X']
    until done == true
      key = Ionio::Input.menuprompt('admin_signup_form.ftl',validkeys, $cfg)
      print "\n"
      case key
        when "A" # Ask Address
          $cfg['signup']['ask_address'] = $cfg['signup']['ask_address'].toggle
          Ionconfig.save($cfg)
        when "B" # Ask Postal
          $cfg['signup']['ask_postal'] = $cfg['signup']['ask_postal'].toggle
          Ionconfig.save($cfg)
        when "C" # Ask Country
          $cfg['signup']['ask_country'] = $cfg['signup']['ask_country'].toggle
          Ionconfig.save($cfg)
        when "D" # Ask Phone
          $cfg['signup']['ask_phone'] = $cfg['signup']['ask_phone'].toggle
          Ionconfig.save($cfg)
        when "E" # Ask Gender
          $cfg['signup']['ask_gender'] = $cfg['signup']['ask_gender'].toggle
          Ionconfig.save($cfg)
        when "F" # Ask Birthdate
          $cfg['signup']['ask_bday'] = $cfg['signup']['ask_bday'].toggle
          Ionconfig.save($cfg)
        when "G" # Ask Custom 1
          $cfg['signup']['ask_custom1'] = $cfg['signup']['ask_custom1'].toggle
          Ionconfig.save($cfg)
        when "H" # Ask Custom 2
          $cfg['signup']['ask_custom2'] = $cfg['signup']['ask_custom2'].toggle
          Ionconfig.save($cfg)
        when "I" # Ask Custom 3
          $cfg['signup']['ask_custom3'] = $cfg['signup']['ask_custom3'].toggle
          Ionconfig.save($cfg)
        when "1" # Change custom 1 text
          $cfg['signup']['custom1'] = Ionio::Input.inputform(64)
          Ionconfig.save($cfg)
        when "2" # Change custom 1 text
          $cfg['signup']['custom2'] = Ionio::Input.inputform(64)
          Ionconfig.save($cfg)
        when "3" # Change custom 1 text          
          $cfg['signup']['custom3'] = Ionio::Input.inputform(64)
          Ionconfig.save($cfg)
        when "X" # Return to Main
          done = true
      end #/case
      # Refresh the configuration file.
      $cfg = Ionconfig.load
    end #/until
    return 0    
  end
  
  # System Limits configuration menu
  def bbsconfig_limits
    # TODO: Reserved until more limits are integrated.
  end
 
  # Notifications configuration menu
  def bbsconfig_notifications
    done = false
    validkeys=['A','B','C','D','E','F','G','H','X']
    until done == true
      key = Ionio::Input.menuprompt('admin_notifications.ftl',validkeys, $cfg)
      print "\n"
      case key
        when "A" # Boolean: Notify User?
          $cfg['notify']['notify_user'] = $cfg['notify']['notify_user'].toggle
          Ionconfig.save($cfg)
        when "B" # Boolean: Notify Group?
          $cfg['notify']['notify_group'] = $cfg['notify']['notify_group'].toggle
          Ionconfig.save($cfg)
        when "C" # User to Notify
          $cfg['notify']['user_name'] = Ionio::Input.inputform(16)
          Ionconfig.save($cfg)
        when "D" # Group to Notify
          $cfg['notify']['group_name'] = Ionio::Input.inputform(16)
          Ionconfig.save($cfg)
        when "E" # Boolean: 
          $cfg['notify']['on_signup'] = $cfg['notify']['on_signup'].toggle
          Ionconfig.save($cfg)
        when "F" # Boolean: 
          $cfg['notify']['on_bbslist'] = $cfg['notify']['on_bbslist'].toggle
          Ionconfig.save($cfg)
        when "G" # Boolean: 
          $cfg['notify']['on_lockout'] = $cfg['notify']['on_lockout'].toggle
          Ionconfig.save($cfg)
        when "H" # Boolean: 
          $cfg['notify']['on_upload'] = $cfg['notify']['on_upload'].toggle
          Ionconfig.save($cfg)
        when "X" # Return to Main
          done = true
      end #/case
      # Refresh the configuration file.
      $cfg = Ionconfig.load
    end #/until
    return 0    
  end


  
#=============================================================================#
# User Manager                                                                #
#=============================================================================#    
  def usermanager
    require 'lib/ionlogin/signup'
    idx = 0 
    userids = User.getuids  # Collect the list of userids into an array    
    done = false
    validkeys=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',\
               'Q','R','S','T','U','V','W','Y','!','*','#','$','+','[',']','X']
    until done == true      
      curuser = User.where(:id => userids[idx]).first
      groupname = Group.select(:name).where(:id => curuser.group_id).first.name
      hashdata = { "user" => Iontemplate::Template.parse_hash(curuser.values), "usercount" => User.count, "groupname" => groupname }
      key = Ionio::Input.menuprompt('admin_usermanager.ftl',validkeys, hashdata)
      print "\n"
      case key
        when "A" # Change Login User Name
          curuser.login = Ionlogin::Signup.asklogin
          curuser.save
        when "B" # Change Group
          curuser.group_id = Group.select(:id).where(:name => selectgroup).first.id
          curuser.save
        when "C" # Change Real Name (Ask first and last)
          curuser.firstname = Ionlogin::Signup.askfirstname
          curuser.lastname = Ionlogin::Signup.asklasstname
          curuser.save
        when "D" # Change Email
          curuser.email = Ionlogin::Signup.askemail
          curuser.save
        when "E" # Change Mailing Address
          curuser.address1 = Ionlogin::Signup.askaddress1
          curuser.address2 = Ionlogin::Signup.askaddress2
          curuser.save
        when "F" # Change City
          curuser.city = Ionlogin::Signup.askcity
          curuser.save
        when "G" # Change State
          curuser.state = Ionlogin::Signup.askstate
          curuser.save
        when "H" # Change Country
          curuser.country = Ionlogin::Signup.askcountry
          curuser.save
        when "I" # Change Postal
          curuser.postal = Ionlogin::Signup.askpostal
          curuser.save
        when "J" # Change Phone
          curuser.phone = Ionlogin::Signup.askphone
          curuser.save
        when "K" # Change Gender
          curuser.gender = Ionlogin::Signup.askgender
          curuser.save
        when "L" # Change Birthday
          curuser.bday = Ionlogin::Signup.askbday
          curuser.save
        when "M" # Change Custom 1
          curuser.custom1 = Ionlogin::Signup.askcustom(1)
          curuser.save
        when "N" # Change Custom 2
          curuser.custom1 = Ionlogin::Signup.askcustom(2)
          curuser.save
        when "O" # Change Custom 3
          curuser.custom1 = Ionlogin::Signup.askcustom(3)
          curuser.save
        when "P" # Change Pref: Fast Login
          curuser.pref_fastlogin = curuser.pref_fastlogin.toggle
          curuser.save
        when "Q" # Change Pref: Show Menus
          curuser.pref_show_menus = curuser.pref_show_menus.toggle
          curuser.save
        when "R" # Change Pref: Term Pager
          curuser.pref_term_pager = curuser.pref_term_pager.toggle
          curuser.save
        when "S" # Change Pref: Term Height
          extradata = { "sizeof"=> "height" }
          value = Ionio::Input.inputrange('admin_usermanager_termsize.ftl', RANGE_TERM_HEIGHT, extradata) 
          if value != nil
            curuser.pref_term_height = value.to_i
            curuser.save
          end    
        when "T" # Change Pref: Term Width
          extradata = { "sizeof"=> "width" }
          value = Ionio::Input.inputrange('admin_usermanager_termsize.ftl', RANGE_TERM_WIDTH, extradata) 
          if value != nil
            curuser.pref_term_width = value.to_i
            curuser.save
          end
        when "U" # Change Pref: Term Color
          curuser.pref_term_color = curuser.pref_term_color.toggle
          curuser.save
        when "V" # Change Pref: Timebank
          curuser.timebank = TimebankController.new.askamount
          curuser.save
        when "W" # Change Pref: Editor
          curuser.pref_editor = 'ioneditor'
          curuser.save
        when "!" # Lockout User
          curuser.islocked = curuser.islocked.toggle
          curuser.save
        when "#" # Edit SysOp Note
          curuser.sysopnote = Ionio::Input.inputform(79) 
          curuser.save
        when "$" # Reset Password  
        when "]" # Next user  
          if idx >= (userids.count - 1) 
            idx = 0
          else
            idx = idx + 1
          end          
        when "[" # Previous user
          if idx <= 0 
            idx = (userids.count - 1)
          else
            idx = idx - 1
          end
        when "*" # Delete User
          if Ionio::Input.inputyn('admin_usermanager_delete.ftl') == true
            if curuser.id == 1
              puts "#{ANSI_BLINK}#{ANSI_BRIGHT_RED}}\n!!! FAILED :: YOU CANNOT DELETE USER ID 1 :: FAILED!!!#{ANSI_RESET}"
              Ionio.pause
            else           
              User.where(:id => userids[idx]).delete
              if idx <= 0 
                idx = (userids.count - 1)
              else
                idx = idx - 1
              end
              userids = User.getuids  # Collect the list of userids into an array
            end
          end     
        when "+" # Add new user
          newuser = User.new do |u|
            u.login             = 'NEW'
            u.islocked          = true
            u.group_id          = 4
            u.password          = User.cryptpassword('locked')
            u.firstname         = ' '
            u.lastname          = ' '
            u.address1          = ' '
            u.address2          = ' '
            u.city              = ' '
            u.state             = ' '
            u.country           = ' '
            u.phone             = ' '
            u.gender            = ' '
            u.bday              = '01/01/1970'
            u.pwexpires         = Time.now + 90.days
            u.created           = Time.now
            u.login_last        = Time.now
            u.sysopnote         = ' '
            u.login_failures    = 0
            u.login_total       = 0
            u.timebank          = 0
            u.total_files_up    = 0
            u.total_files_down  = 0
            u.total_messages    = 0
            u.pwhint_question   = ""
            u.pwhint_answer     = ""
            u.pref_fastlogin    = false
            u.pref_term_height  = 24
            u.pref_term_width   = 80
            u.pref_term_pager   = true
            u.pref_term_color   = true
            u.pref_show_menus   = true
            u.pref_editor       = 'ioneditor'
          end
          newuser.save          
          newpass = Ionlogin::Signup.askpassword
          newuser.password = User.cryptpassword(newpass)
          newuser.login = "USER-#{newuser.id}"
          newuser.sysopnote = "NEW User ##{newuser.id}"
          newuser.save
          userids = User.getuids  # Collect the list of userids into an array
          idx = userids.count - 1          
        when "X" # Return to Main
          done = true
      end #/case
    end #/until
    return 0    
  end



#=============================================================================#  
# Group Manager                                                               #
#=============================================================================#  
  def groupmanager
    idx = 0 
    groupids = Group.getgids  # Collect the list of userids into an array    
    done = false
    validkeys=['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P',\
               'Q','R','S','T','U','!','1','2','3','4','5','6','7','8',\
               '*','#','+','[',']','X']
    until done == true
      curgroup = Group.where(:id => groupids[idx]).first
      hashdata = { "group" => Iontemplate::Template.parse_hash(curgroup.values), "groupcount" => Group.count }      
      key = Ionio::Input.menuprompt('admin_groupmanager.ftl',validkeys, hashdata)
      print "\n"
      case key
        when "A" # Change Group name
          curgroup.name = Ionio::Input.inputform(16).upcase
          curgroup.save
        when "B" # Change Level
          value = Ionio::Input.inputrange('admin_groupmanager_level.ftl', RANGE_GROUP_LEVEL) 
          if value != nil
            curgroup.level = value.to_i
            curgroup.save
          end
        when "C" # Change Daily Time Limit
          value = Ionio::Input.inputrange('admin_groupmanager_dailytimelimit.ftl', RANGE_GROUP_DAILYTIME) 
          if value != nil
            curgroup.dailytimelimit = value.to_i
            curgroup.save
          end
        when "D" # Change Time Deposit Limit
          extradata = { "bank" => "deposit" }
          value = Ionio::Input.inputrange('admin_groupmanager_timebank.ftl', RANGE_GROUP_TIMEBANK, extradata) 
          if value != nil
            curgroup.max_timedeposit = value
            curgroup.save
          end  
        when "E" # Change Time Withdraw Limit
          extradata = { "bank" => "withdraw" }
          value = Ionio::Input.inputrange('admin_groupmanager_timebank.ftl', RANGE_GROUP_TIMEBANK, extradata) 
          if value != nil
            curgroup.max_timewithdraw = value
            curgroup.save
          end  
        when "F" # Change Max D/L in file numbers
          curgroup.max_downloads = Ionio::Input.inputform(3,'int')
          curgroup.save
        when "G" # Change Max D/L in bytes
          # TODO: Determine Kb, MB, GB, and convert to bytes
          curgroup.max_downbytes = Ionio::Input.inputform(8,'int')
          curgroup.save
        when "H" # Change Max message posts
          curgroup.max_posts = Ionio::Input.inputform(2,'int')
          curgroup.save
        when "I" # Change Max U/L in file numbers
          curgroup.max_uploads = Ionio::Input.inputform(2,'int')
          curgroup.save
        when "J" # Change Max U/L in kilobyutes
          # TODO: Determine Kb, MB, GB, and convert to bytes
          curgroup.max_upbytes = Ionio::Input.inputform(2,'int')
          curgroup.save
        when "K" # Change <reserved>
        when "L" # Toggle Permission: Message Areas 
          curgroup.allow_msgareas = curgroup.allow_msgareas.toggle
          curgroup.save
        when "M" # Toggle Permission: Read Posts
          curgroup.allow_readpost = curgroup.allow_readpost.toggle
          curgroup.save
        when "N" # Toggle Permission: Write Posts
          curgroup.allow_writepost = curgroup.allow_writepost.toggle
          curgroup.save
        when "O" # Toggle Permission: File Areas
          curgroup.allow_fileareas = curgroup.allow_fileareas.toggle
          curgroup.save
        when "P" # Toggle Permission: Download
          curgroup.allow_download = curgroup.allow_download.toggle
          curgroup.save
        when "Q" # Toggle Permission: Upload
          curgroup.allow_upload = curgroup.allow_upload.toggle
          curgroup.save
        when "R" # Toggle Permission: Chat Rooms
          curgroup.allow_chatrooms = curgroup.allow_chatrooms.toggle
          curgroup.save
        when "S" # Toggle Permission: External Programs
          curgroup.allow_extprogs = curgroup.allow_extprogs.toggle
          curgroup.save
        when "T" # Toggle Permission: Page Sysop
          curgroup.allow_pagesysop = curgroup.allow_pagesysop.toggle
          curgroup.save
        when "U" # Toggle Permission: Allow Login
          curgroup.allow_login = curgroup.allow_login.toggle
          curgroup.save
        when "!" # Toggle Permission: IsAdmin?
          # when setting to false, turn off all admin flags
          if curgroup.is_admin == true
            curgroup.admin_system         = false
            curgroup.admin_announcements  = false
            curgroup.admin_users          = false
            curgroup.admin_groups         = false
            curgroup.admin_files          = false
            curgroup.admin_msgs           = false
            curgroup.admin_chat           = false
            curgroup.admin_extprogs       = false
          end
            curgroup.is_admin = curgroup.is_admin.toggle
            curgroup.save
        when "1" # Admin: System Configuration
          curgroup.admin_system = curgroup.admin_system.toggle
          curgroup.save
        when "2" # Admin: User Manager
          curgroup.admin_users = curgroup.admin_users.toggle
          curgroup.save
        when "3" # Admin: Group Manager
          curgroup.admin_groups = curgroup.admin_groups.toggle
          curgroup.save
        when "4" # Admin: File Areas
          curgroup.admin_files = curgroup.admin_files.toggle
          curgroup.save
        when "5" # Admin: Message Areas
          curgroup.admin_msgs = curgroup.admin_msgs.toggle
          curgroup.save
        when "6" # Admin: Chat Rooms
          curgroup.admin_chat = curgroup.admin_chat.toggle
          curgroup.save
        when "7" # Admin: External Programs
          curgroup.admin_extprogs = curgroup.admin_extprogs.toggle
          curgroup.save
        when "8" # Admin: Announcements
          curgroup.admin_announcements = curgroup.admin_announcements.toggle
          curgroup.save
        when "#" # Change Note
          curgroup.sysopnote = Ionio::Input.inputform(79)
          curgroup.save
        when "]" # Next group  
          if idx >= (groupids.count - 1) 
            idx = 0
          else
            idx = idx + 1
          end          
        when "[" # Previous group
          if idx <= 0 
            idx = (groupids.count - 1)
          else
            idx = idx - 1
          end
        when "*" # Delete Group
          if Ionio::Input.inputyn('admin_groupmanager_delete.ftl') == true
            if RANGE_PROTECTED_GROUP.member?(curgroup.id) == true
              puts "#{ANSI_BLINK}#{ANSI_BRIGHT_RED}}\n!!! FAILED :: YOU CANNOT DELETE A PROTECTED GROUP :: FAILED!!!#{ANSI_RESET}"
              Ionio.pause
            else           
              Group.where(:id => groupids[idx]).delete
              if idx <= 0 
                idx = (groupids.count - 1)
              else
                idx = idx - 1
              end
              groupids = Group.getgids  # Collect the list of userids into an array
            end
          end     
        when "+" # Add new group
          newgroup = Group.new do |g|
            g.name                = 'NEW'
            g.sysopnote           = 'NEW GROUP'
            g.level               = 0
            g.dailytimelimit      = 0
            g.max_timedeposit     = 0
            g.max_timewithdraw    = 0
            g.max_downloads       = 0
            g.max_downbytes       = 0
            g.max_uploads         = 0
            g.max_upbytes         = 0
            g.max_posts           = 0
            g.allow_login         = false
            g.allow_msgareas      = false
            g.allow_readpost      = false
            g.allow_writepost     = false
            g.allow_pagesysop     = false
            g.allow_chatrooms     = false
            g.allow_fileareas     = false
            g.allow_download      = false
            g.allow_upload        = false
            g.allow_extprogs      = false
            g.is_admin            = false
            g.admin_system        = false
            g.admin_announcements = false
            g.admin_files         = false
            g.admin_msgs          = false
            g.admin_users         = false
            g.admin_groups        = false
            g.admin_chat          = false
            g.admin_extprogs      = false
            g.created             = Time.now
          end
          newgroup.save
          newgroup.name = "GROUP-#{newgroup.id}"
          newgroup.sysopnote = "NEW Group ##{newgroup.id}"
          newgroup.save
          groupids = Group.getgids  # Collect the list of userids into an array
          idx = groupids.count - 1
        when "X" # Return to Main
          done = true
      end #/case
    end #/until
    return 0    
  end



#=============================================================================#
# Message Areas                                                               #
#=============================================================================#    
  def msgareasconfig
    idx = 0 
    areaids = Ionmsgarea.getareaids  # Collect the list of area ids into an array    
    done = false
    validkeys=['A','B','C','D','E','F','G','H','[',']','*','+','!','X']
    until done == true
      curarea = Ionmsgarea.where(:id => areaids[idx]).first
      hashdata = { "area" => Iontemplate::Template.parse_hash(curarea.values), "areacount" => Ionmsgarea.count }      
      key = Ionio::Input.menuprompt('admin_msgareas.ftl',validkeys, hashdata)
      print "\n"
      case key
        when "A" # Change area name
          curarea.name = Ionio::Input.inputform(24)
          curarea.save
        when "B" # Change area Description
          curarea.description = Ionio::Input.inputform(79)
          curarea.save
        when "C" # Change read level
          value = Ionio::Input.inputrange('admin_groupmanager_level.ftl', RANGE_GROUP_LEVEL) 
          if value != nil
            curarea.minlevel_read =  value.to_i
            curarea.save
          end
        when "D" # Change write level
          value = Ionio::Input.inputrange('admin_groupmanager_level.ftl', RANGE_GROUP_LEVEL) 
          if value != nil
            curarea.minlevel_write =  value.to_i
            curarea.save
          end
        when "!" # Enable/Disable
          curarea.enabled = curarea.enabled.toggle
          curarea.save
        when "+" # Add new area
          newarea = Ionmsgarea.new do |a|
            a.name            = 'NEWAREA'
            a.description     = 'Description'
            a.minlevel_read   = '100'
            a.minlevel_write  = '100'
            a.enabled         = false
            a.created         = Time.now
          end
          newarea.save
          newarea.name = "MSGAREA-#{newarea.id}"
          newarea.description = "NEW Message Area ##{newarea.id}"
          newarea.save
          areaids = Ionmsgarea.getareaids  # Collect the list of userids into an array
          idx = areaids.count - 1          
        when "*" # Delete area
          if Ionio::Input.inputyn('admin_area_delete.ftl') == true
            Ionmsgarea.where(:id => areaids[idx]).delete
            if idx <= 0 
              idx = (areaids.count - 1)
            else
              idx = idx - 1
            end
            areaids = Ionmsgarea.getareaids  # Collect the list of userids into an array
          end
        when "]" # Next Area
          if idx >= (areaids.count - 1) 
            idx = 0
          else
            idx = idx + 1
          end          
        when "[" # Previous Area
          if idx <= 0 
            idx = (areaids.count - 1)
          else
            idx = idx - 1
          end          
        when "X" # Return to Main
          done = true
      end #/case
    end #/until
    return 0
  end
  
  
  
#=============================================================================#
# File Areas Configuration                                                    #
#=============================================================================#    
  def fileareasconfig
    idx = 0 
    areaids = Ionfilearea.getareaids  # Collect the list of area ids into an array    
    done = false
    validkeys=['A','B','C','D','E','F','G','H','[',']','*','+','!','X']
    until done == true
      curarea = Ionfilearea.where(:id => areaids[idx]).first
      hashdata = { "area" => Iontemplate::Template.parse_hash(curarea.values), "areacount" => Ionfilearea.count }      
      key = Ionio::Input.menuprompt('admin_fileareas.ftl',validkeys, hashdata)
      print "\n"
      case key
        when "A" # Change area name
          curarea.name = Ionio::Input.inputform(24)
          curarea.save
        when "B" # Change area Description
          curarea.description = Ionio::Input.inputform(79)
          curarea.save
        when "C" # Change read level
          value = Ionio::Input.inputrange('admin_groupmanager_level.ftl', RANGE_GROUP_LEVEL) 
          if value != nil
            curarea.minlevel_read =  value.to_i
            curarea.save
          end
        when "D" # Change write level
          value = Ionio::Input.inputrange('admin_groupmanager_level.ftl', RANGE_GROUP_LEVEL) 
          if value != nil
            curarea.minlevel_write =  value.to_i
            curarea.save
          end
        when "E" # Toggle Read access
          curarea.read = curarea.read.toggle
          curarea.save
        when "F" # Toggle Write access
          curarea.write = curarea.write.toggle
          curarea.save
        when "P" # Path location
          curarea.path = Ionio::Input.inputform(79)
          curarea.save
          #TODO: Verify the directory exists and is writeable
          #      or ask to create it if not.
        when "!" # Enable/Disable
          curarea.enabled = curarea.enabled.toggle
          curarea.save
        when "+" # Add new area
          newarea = Ionfilearea.new do |a|
            a.name            = 'NEWAREA'
            a.description     = 'Description'
            a.minlevel_read   = '100'
            a.minlevel_write  = '100'
            a.read            = false
            a.write           = false
            a.enabled         = false
            a.path            = '/dev/null'
            a.created         = Time.now
          end
          newarea.save
          newarea.name = "FILEAREA-#{newarea.id}"
          newarea.description = "NEW File Area ##{newarea.id}"
          newarea.save
          areaids = Ionfilearea.getareaids  # Collect the list of userids into an array
          idx = areaids.count - 1          
        when "*" # Delete area
          if Ionio::Input.inputyn('admin_area_delete.ftl') == true
            Ionfilearea.where(:id => areaids[idx]).delete
            if idx <= 0 
              idx = (areaids.count - 1)
            else
              idx = idx - 1
            end
            areaids = Ionfilearea.getareaids  # Collect the list of userids into an array
          end
        when "]" # Next Area
          if idx >= (areaids.count - 1) 
            idx = 0
          else
            idx = idx + 1
          end          
        when "[" # Previous Area
          if idx <= 0 
            idx = (areaids.count - 1)
          else
            idx = idx - 1
          end          
        when "X" # Return to Main
          done = true
      end #/case
    end #/until
    return 0
  end



#=============================================================================#
# Chat Rooms                                                                  #
#=============================================================================#    
def chatconfig
  idx = 0 
  idlist = Ionchatroom.getids
  # If there are no announcements, ask if one should be created
  if idlist.empty? == true
    if Ionio::Input.inputyn('admin_chatrooms_empty.ftl') == true
      self.chatroom_new
      idx = 0 
      idlist = Ionchatroom.getids
    else
      return 0
    end
  end
  done = false
  validkeys=['A','B','C','+','[',']','*','!','X']
  until done == true
    curchatroom = Ionchatroom.where(:id => idlist[idx]).first
    hashdata = { "room" => Iontemplate::Template.parse_hash(curchatroom.values), "roomcount" => Ionchatroom.count }         
    key = Ionio::Input.menuprompt('admin_chat.ftl',validkeys, hashdata)
    print "\n"
    case key
      when "A" # Room Name
      when "B" # Room Description
      when "C" # Minimum level to join
      when "!" # Toggle Enable/Disable
        curchatroom.enabled = curchatroom.enabled.toggle
        curchatroom.save            
      when "]" # Next chatroom  
        if idx >= (idlist.count - 1) 
          idx = 0
        else
          idx = idx + 1
        end          
      when "[" # Previous chatroom
        if idx <= 0 
          idx = (idlist.count - 1)
        else
          idx = idx - 1
        end
      when "*" # Delete chatroom
        if Ionio::Input.inputyn('admin_chatroom_delete.ftl') == true
          Ionchatroom.where(:id => idlist[idx]).delete
          if idx <= 0 
            idx = (idlist.count - 1)
          else
            idx = idx - 1
          end
          idlist = Ionchatroom.getids  # Collect the list of userids into an array
        end
      when "+"
        self.chatroom_new
        idlist = Ionchatroom.getids    # Refresh listing after adding 
        idx = idlist.count - 1         # see the new item instantly
      when "X" # Return to Main
        done = true
    end #/case
  end #/until
  return 0    
end #/chatconfig

# Create a new chatroom
def chatroom_new
  roomname = self.chatroom_ask_name
  if roomname.size > 0  # If name is blank, assume user is aborting.
    roomdesc = self.chatroom_ask_description
    roomlevel = self.chatroom_ask_minlevel
    roomenabled = self.chatroom_ask_enabled(false)
      # Save the item to the database
      newroom = Ionchatroom.new do |a|
        a.name         = roomname
        a.description  = roomdesc
        a.minlevel     = roomlevel
        a.enabled      = roomenabled
        a.created      = Time.now
      end #/newroom
      newroom.save
    end #/if roomname.size > 0
end  

def chatroom_ask_name
  done = false
  until done == true
    Iontemplate.display('admin_chatroom_name.ftl')
    result =  Ionio::Input.inputform(20)
    if result.is_alphanumeric? == true
      done = true
    end  
  end
  return result
end

def chatroom_ask_description
  Iontemplate.display('admin_chatroom_description.ftl')
  return Ionio::Input.inputform(64)
end

def chatroom_ask_minlevel
  value = Ionio::Input.inputrange('admin_chatroom_level.ftl', RANGE_GROUP_LEVEL) 
  if value != nil
    return value.to_i
  end  
end

def chatroom_ask_enabled(curstate)
  hashdata = {'room' => {'enabled' => curstate } }
  Iontemplate.display('admin_chatroom_enabled.ftl', hashdata)
  return Ionio::Input.inputyn()
end


#=============================================================================#
# Announcements                                                               #
#=============================================================================#
  def announcements
    idx = 0 
    idlist = Ionannouncement.getids
    # If there are no announcements, ask if one should be created
    if idlist.empty? == true
      if Ionio::Input.inputyn('admin_announcement_empty.ftl') == true
        self.announcement_new
        idx = 0 
        idlist = Ionannouncement.getids
      else
        return 0
      end
    end
    done = false
    validkeys=['T','E','D','P','[',']','*','+','!','X']
    until done == true
      curitem = Ionannouncement.where(:id => idlist[idx]).first
      hashdata = { "announcement" => Iontemplate::Template.parse_hash(curitem.values), "itemcount" => Ionannouncement.count }      
      key = Ionio::Input.menuprompt('admin_announcements.ftl',validkeys, hashdata) 
      print "\n"
      case key
        when "T" # Title
          Iontemplate.display('admin_announcement_title.ftl')
          curitem.title = Ionio::Input.inputform(64)
          curitem.save
        when "E" # Edit body with IonEditor
          newbody = Ionio::Ionedit::edit(curitem.body, curitem.subject)
          unless newbody == nil
            curitem.body = newbody 
            curitem.save
          end           
        when "P" # View priority
          Iontemplate::display('admin_announcement_priority.ftl')
          curitem.priority = Ionio::Input.inputform(2,'posint')
          curitem.save
        when "D" # Change expiration date if expires is true
          if curitem.expires == true
             # TODO: Ask for a date
             curitem.expiration = Ionio::Dates::expire
             curitem.save
          end
        when "!" # Toggle Expires
           if curitem.expires == true
             # If the expiration flag is set, clear the expiration date field
             curitem.expiration = nil
             curitem.expires = curitem.expires.toggle
           else
             # Set the date field when turning on the expiration flag
             # TODO: Ask for a date
             curitem.expiration = Ionio::Dates::expire
             curitem.expires = curitem.expires.toggle
           end
           curitem.save           
        when "*" # Delete this announcement
          if Ionio::Input.inputyn('admin_announcement_delete.ftl') == true
            Ionannouncement.where(:id => idlist[idx]).delete
            idx = 0 # Reset to first announcement when deleting, fixes a bug
            idlist = Ionannouncement.getids
            if idlist.empty? == true
              return 0
            end #/idlist.empty
          end
        when "+" # Add new announcement
          self.announcement_new
          idlist = Ionannouncement.getids 
          idx = idlist.count - 1 
        when "]" # Next Item
          if idx >= (idlist.count - 1) 
            idx = 0
          else
            idx = idx + 1
          end          
        when "[" # Previous Item
          if idx <= 0 
            idx = (idlist.count - 1)
          else
            idx = idx - 1
          end          
      
        when "X" # Return to Main
          done = true
      end #/case
    end #/until
    return 0    
  end

def announcement_new
         Iontemplate.display('admin_announcement_title.ftl')
          newtitle = Ionio::Input.inputform(64)
          if newtitle.size > 0  # If subject is blank, assume user is aborting.
            newbody = Ionio::Ionedit::edit(nil, newtitle)
            Iontemplate.display('admin_announcement_priority.ftl')
            newpriority = Ionio::Input.inputform(2,'posint')
            if Ionio::Input.inputyn('admin_announcement_expires.ftl') == true
              newexpires = true
              newexpiration = Ionio::Dates::expire
            else
              newexpires = false
              newexpiration = nil  
            end #/if expires
            # Save the item to the database
            newitem = Ionannouncement.new do |a|
              a.title           = newtitle
              a.body            = newbody
              a.submitted_by    = 'SYSOP'
              a.priority        = newpriority
              a.expires         = newexpires
              a.expiration      = newexpiration
              a.created         = Time.now
            end #/newitem
            newitem.save
          end #/if title.size > 0
end
#=============================================================================#


end
#EOL

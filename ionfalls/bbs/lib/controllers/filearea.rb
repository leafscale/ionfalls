=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/filearea.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: FileArea controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= FileAreaController
The File Areas Menu controller.
=end


class FileAreaController < Ioncontroller

  def initialize
    if $session
      $session.current_area = "File Areas"
      $session.save
    end    
  end    
    
  # FileArea Menu Handler
  def menu
    done = false
    validkeys=['A','D','J','L','N','S','U','V',']','[','X']
    until done == true

      # Retrieve a list and count of File Areas - This is done each menu loop in the event soemthing changes
      # by an admin during the user's session. Potentially, a change could cause the controller to crash.
      #arealist  = $db[:fileareas].select(:id).order(:id).map(:id)
      #areacount =  $db[:fileareas].count
      arealist  = Ionfilearea.select(:id).order(:id).map(:id)
      areacount =  Ionfilearea.count

      # Track the user's current area from the session table.
      if $session.filearea.nil? == true
        curarea = arealist.first
        $session.filearea = curarea
        $session.save
      else
        curarea = $session.filearea
      end
      # Set the array iterator index location
      areaindex = arealist.index(curarea)
      # Get area's metadata info
      areameta = Ionfilearea.where(:id => curarea).first
      # Get a count of total files in this area
      filecount = Ionfile.filter(:ionfilearea_id => $session.filearea).count

      # Display menu
      key = Ionio::Input.menuprompt('menu_filearea.ftl',validkeys, {"areanum" => areameta[:id].to_s,"areaname" => areameta[:name].capitalize, "areadesc" => areameta[:description], "areacount" => filecount })
      print "\n"
      case key
        when "A" # List all File Area Names/Descriptions
          areas = Iontemplate::Template.parse_modeldata(Ionfilearea.all)          
          Iontemplate.display('filearea_list_areas.ftl', {'areas' => areas})

        when "D" # Prompt user to download a file by file.id
          Ionfalls.unimplemented

        when "J" # Jump to another area
          Iontemplate.display('filearea_jumpto.ftl', nil)
          jumper = Ionio::Input::inputform(4).to_i
          # Validate input for correctness before switching areas
          if arealist.include?(jumper) == true
            $session.filearea = jumper
            $session.save
          else
            Iontemplate.display('filearea_jumpto_invalid.ftl', nil)
          end

        when "L" # List files in current area
          if areameta[:read] == true
            filelist = Iontemplate::Template.parse_modeldata(FileController.new($session.filearea).listfiles)
            Iontemplate.display('filearea_list_files.ftl', {'files' => filelist, 'areaname' => areameta[:name].capitalize})
          else                                                                                        
            Iontemplate.display('filearea_read_forbidden.ftl', {'areaname'=>areameta[:name].capitalize})
          end

        when "N"
          Ionfalls.unimplemented

        when "S" # Search for Files
          Ionfalls.unimplemented

        when "T" # Tag file for download queue
          Ionfalls.unimplemented

        when "U" # Upload a File
          Ionfalls.unimplemented

        when "V" # View File Metadata
          filenum = Ionio::Input::inputform(6).to_i
          metaset = Ionfile.where(:id => filenum).first
          unless metaset.nil? == true # Check if result set is valid
            if metaset[:enabled] == true # Only show data if file is enabled              
              metaset[:uploaded_by] =  $db[:users].select(:login).where(:id => metaset[:uploaded_by]).first[:login]
              metaset[:approved_by] = $db[:users].select(:login).where(:id => metaset[:approved_by]).first[:login]
              metaset[:owner_id] = $db[:users].select(:login).where(:id => metaset[:owner_id]).first[:login]
              Iontemplate.display('filearea_file_metadata.ftl', {'file'=> Iontemplate::Template.parse_hash(metaset)})
            else
              Iontemplate.display('filearea_file_invalid.ftl',nil)
            end
          else
            Iontemplate.display('filearea_file_invalid.ftl', nil)
          end

        when "]" # Next Area
          if areaindex > (areacount - 1)
            $session.filearea = arealist.first
          else
            $session.filearea = arealist.at(areaindex + 1)
          end
          $session.save

        when "[" # Previous Area
          if areaindex <= 0
            $session.filearea = arealist.last
          else
            $session.filearea = arealist.at(areaindex - 1)
          end
          $session.save
        when "X"
          return 0
      end #/case
    end #/until
  end #/def menu
 
end

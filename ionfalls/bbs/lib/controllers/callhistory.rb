=begin
(*
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/controllers/callhistory.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Call History controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= CallHistoryController
The CallHistory menu controller.
=end

class CallHistoryController < Ioncontroller
  
  def initialize
    if $session
      $session.current_area = "Call History"
      $session.save
    end  
  end

  def menu    
    done = false
    validkeys=['A', 'D', 'F', 'M', 'T', 'X']
    until done == true      
      key = Ionio::Input.menuprompt('menu_callhistory.ftl',validkeys, nil) # nil is for tpl vars hash
      test_session
      print "\n"
      case key
        when "A" # View All
          #callers = Iontemplate::Template.parse_dataset($db[:callhistory].all)
          callers = Iontemplate::Template.parse_modeldata(Ioncallhistory.all)  
          Iontemplate.display('callhistory.ftl', {'title'=> 'All Logged Calls','callers'=>callers})
        when "D" # Show by date
          searchdate = Ionio::Dates::inputdate
          #callers    = Iontemplate::Template.parse_dataset($db[:callhistory].filter(:time_login => (DateTime.parse(searchdate.asctime))..(DateTime.parse(searchdate.asctime) +1)).all)
          callers    = Iontemplate::Template.parse_modeldata(Ioncallhistory.filter(:time_login => (DateTime.parse(searchdate.asctime))..(DateTime.parse(searchdate.asctime) +1)).all)
          Iontemplate.display('callhistory.ftl', {'title' => "#{searchdate} History",'callers'=>callers})
        when "M" # Show history for current user
          callers = Iontemplate::Template.parse_modeldata(Ioncallhistory.filter(:user_id => $session.user_id).all)
          Iontemplate.display('callhistory.ftl', {'title' => "#{$callerid.alias}'s History",'callers'=>callers})
        when "F" # Search history by some username
          #Telegard.unimplemented
          searchuser = Ionio::Input::inputform(16).upcase
          callers = Iontemplate::Template.parse_modeldata(Ioncallhistory.filter(:alias => searchuser).all)          
          Iontemplate.display('callhistory.ftl', {'title'=> "Calls for #{searchuser}",'callers'=>callers})
        when "T" # Show Today's callers
          searchdate = Date.today
          callers    = Iontemplate::Template.parse_modeldata(Ioncallhistory.filter(:time_login => (DateTime.parse(searchdate.asctime))..(DateTime.parse(searchdate.asctime) +1)).all)
          Iontemplate.display('callhistory.ftl', {'title' => "Callers Today", 'callers'=>callers})
        when "X" # "Return to Main"
          return 0
      end #/case
      test_session
    end #/until
  end #/def menu
 
end

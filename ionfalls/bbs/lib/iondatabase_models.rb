=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/iondatabase_models.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Define Database Models. This file can only be called after a
#              Sequel database instance has been created.
#
#-----------------------------------------------------------------------------

=begin rdoc               
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== Database Models loader
This file simply loads the database models.  The order matters in some cases
=end
require 'lib/dbmodels/group'
require 'lib/dbmodels/user'
require 'lib/dbmodels/filearea'
require 'lib/dbmodels/file'
require 'lib/dbmodels/msgarea'
require 'lib/dbmodels/chatroom'
require 'lib/dbmodels/extprogs'
require 'lib/dbmodels/session'
require 'lib/dbmodels/callhistory'
require 'lib/dbmodels/bbslist'
require 'lib/dbmodels/announcement'
#!EOF

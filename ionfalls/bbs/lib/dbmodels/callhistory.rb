=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/dbmodels/callhistory.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: External Programs Schema for Database
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== Callhistory Structure

=end


class Ioncallhistory < Sequel::Model(:callhistory)
  Ionio.printstart " DB Model: callhistory"

# Calculates the total time a specific user by id has spent in callhistory in a 24hour period
def thisuser_time_today(uid = self.user_id)
  today = 0
  dataset = $db[:callhistory].where(:user_id => uid).filter(:time_login =>(Time.now - 24.hours)..(Time.now))
  dataset.each do |ds|
    unless ds[:time_logout].nil?
      timediff = (ds[:time_logout] - ds[:time_login]).to_i.sec_to_min
      today += timediff
    end #/unless
  end #/do
  return today
end #/def

# Calculates the total login count for a specific user by id in a 24hour period
def thisuser_logincount_today(uid = self.user_id)
  today = 0
  dataset = $db[:callhistory].where(:user_id => uid).filter(:time_login =>(Time.now - 24.hours)..(Time.now))
  return dataset.count
end #/def


  Ionio.printreturn(0)
end #/class
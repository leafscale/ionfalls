=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/dbmodels/msgarea.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: MsgArea Schema for Database
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== File Areas Structure

=end

# Messsages Area Structure
class Ionmsgarea < Sequel::Model(:msgareas)
  Ionio.printstart " DB Model: msgareas"
  
  # Returns an array of User ID's   
  def self.getareaids
    result = Array.new    
    areaids = self.select(:id).order(:id).all
    areaids.each do |a|
      result.push a[:id]
    end
    return result
  end
  
  
  Ionio.printreturn(0)
end

=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/dbmodels/announcement.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Announcement database handlers
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== Announcement Structure
 Announcement or news table. (aka: Automessage)
=end


# Email Structure (must be named Ionemail)
class Ionannouncement < Sequel::Model(:announcements)
  Ionio.printstart " DB Model: announcements"

  # Returns an array of ID's   
  def self.getids
    result = Array.new    
    idlist = self.select(:id).order(:id).all
    idlist.each do |a|
      result.push a[:id]
    end
    return result
  end

  Ionio.printreturn(0)
end

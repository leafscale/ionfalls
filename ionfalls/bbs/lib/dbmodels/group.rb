=begin
(*
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/dbmodels/group.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Group Schema for Database
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== Group Structure
The Users & Group models are designed to work as follows:
*  a User holds only the personal data.
*  a Group holds the permissions & limits.
*  a User can only belong to one group.
*  a User with "ADMIN" rights to Users or Groups cannot view or modify
   entries with greater privilege level (Security).
*  a Group has a "level" field which defines the security level. This
   allows the admin to create "custom" groups with varying levels of
   access. The lower the level value, the higher the access
   (integer >= [1..100]).

=end

# Group Structure
class Group < Sequel::Model(:groups)
  Ionio.printstart " DB Model: groups"
  # => Create association of ONE Group to Many Users
  #one_to_many :users

  # Test if the group exists in the database
  def self.exists?(groupname)
    group = self[:name=>groupname]
    if group.nil?
      return false
    else
      return true
    end
  end
  
  # Returns an array of Group ID's   
  def self.getgids
    result = Array.new    
    gids = self.select(:id).order(:id).all
    gids.each do |g|
      result.push g[:id]
    end
    return result
  end

  Ionio.printreturn(0)
end #/class Group

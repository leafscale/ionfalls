=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/dbmodels/session.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Session Schema for Database
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== Session Structure

=end


# Session Structure
class Session < Sequel::Model(:sessions)
  Ionio.printstart " DB Model: sessions"
  
  # Get current user by login name and return instance.
  def getcuruser
    $db[:users].where(:id => $session.user_id).first
  end

  # Get group of current user and return instance.
  def getcurgroup
    $db[:groups].where(:id => $session.group_id).first
  end

  # Set the session expiration time. This is determined
  # Based on the amount of user's daily alloted time,
  # substracted by the amount of time used so far today,
  # then compared to the current time to create a time value
  # of when the expiration will occur.
  def setexpiration
    return -1
  end

  # Returns value of session's remaining time in minutes
  def timeremain
      #return ($session.expires - Time.now).to_i.sec_to_min
      return (self.expires - Time.now).to_i.sec_to_min
  end

  # Adjust the session remaining time by a specific value (an integer of minutes)
  # a negative value will decrease the time, whereas a positive will increase
  def timeadjust(val=0)
    #$session.expires = $session.expires + (val * 60)
    #$session.save
    self.expires = self.expires + (val * 60)
    self.save
  end

  # Check if the session is valid.
  def is_valid?
    #if $session && $session.expires >= Time.now || self.timeremain >= 0
    if self.expires >= Time.now || self.timeremain >= 0
      return true
    end
  end
  
  # Cleanup stale sessions
  def cleanup
  end
  

  Ionio.printreturn(0)
end


=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/dbmodels/user.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: User Schema for Database
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Iondatabase_models (Database Models)
Iondatabase_models defines the Sequel Model classes for the data structures.

== User Structure
The Users & Group models are designed to work as follows:
*  a User holds only the personal data.
*  a Group holds the permissions & limits.
*  a User can only belong to one group.
*  a User with "ADMIN" rights to Users or Groups cannot view or modify
   entries with greater privilege level (Security).
*  a Group has a "level" field which defines the security level. This
   allows the admin to create "custom" groups with varying levels of
   access. The lower the level value, the higher the access
   (integer >= [1..100]).

=end


# User Class
class User < Sequel::Model(:users)
  require 'bcrypt'

  # Encrypt Password with BCrypt
  def self.cryptpassword(cleartxt)
    password = BCrypt::Password.create(cleartxt)
    return password
  end

  Ionio.printstart " DB Model: users"

  # Test if the user exists in the database
  # Use-case: Validation for login or other routines to see if a specified user exists
  def self.exists?(username)
    user = self[:login=>username]
    if user.nil?
      return false
    else
      return true
    end
  end

  # Authorize a user by name. Passes in cleartxt version of password
  def self.authorize(username, cleartxtpw)
    user = self[:login=>username]
    unless user.nil?
      # Reject user if Group level is 255 or higher, or account is set to locked
      unless user.islocked == true || Group.find(:id => user.group_id).level <= 1
        dbpw = BCrypt::Password.new("#{user.password}")
        if dbpw == "#{cleartxtpw}"
          return {:result=>"true"}
        else
          user.set(:login_failures => user.login_failures+1)
          if user.login_failures >= $cfg['login']['lockout']
            user.set(:islocked => true)
          end
          user.save
          return {:result => "false", :failures => user.login_failures, :islocked=> user.islocked}
        end
      else
        return {:result => "locked"}
      end
    else
        return {:result=>"invalid"}
    end #/unless
  end

  # Clear login_failures count on a user
  def self.clearfailed(username)
    user = self[:login=>username]
    user.set(:islocked=>false, :login_failures=>0)
    user.save
  end

  # Count the login by incrementing the DB login_count and timestamp of login_last
  def self.countlogin(username)
    user = self[:login=>username]
    user.set(:login_total=>user.login_total+1, :login_last => Time.now)
    user.save
  end

  # Simple validator to compare 2 passwords in any presented format
  def self.validatepw(pw1, pw2)
    if pw1 == pw2
      return true
    else
      return false
    end
  end

  # Returns an array of User ID's   
  def self.getuids
    result = Array.new    
    uids = self.select(:id).order(:id).all
    uids.each do |u|
      result.push u[:id]
    end
    return result
  end

Ionio.printreturn(0)
end #/class User

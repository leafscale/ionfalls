=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2019 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/dbmodels/preload.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: This file holds the default data for preloading the database
#  tables needed to run a minimal install.
#
#-----------------------------------------------------------------------------

=begin rdoc               
= Database Preloader
the default data for preloading the database tables needed to run a minimal install
=end

Ionio.printstart "DB Preloading data"
Ionio.printreturn(3)

#============================================================================
# Table: BBSLists 
#============================================================================
if Ionbbslist.empty?
  Ionio.printstart " inserting: Ionbbslist"
    Ionbbslist.create  :bbsname => 'IonFalls Official BBS',
      :description => 'The official IonFalls BBS. This server runs official releases and is a full-production system.',
      :sysopname => 'TELEGARDIAN',
      :bbsurl => 'ssh://ionfalls:ionfalls@bbs.ionfalls.com',
      :homepage => 'http://www.ionfalls.org/bbs',
      :submitted_by => 'Chris Tusa',
      :created => Time.now
  Ionio.printreturn(0)
end

#============================================================================
# Table: Callhistory
#============================================================================
#if Ioncallhistory.empty?
#  Ionio.printstart " inserting: Ioncallhistory"
#    #Ioncallhistory.create  :label => 'value'
#  Ionio.printreturn(0)
#end

#============================================================================
# Table: Chatroom
#============================================================================
if Ionchatroom.empty?
  Ionio.printstart " inserting: Ionchatroom"
    Ionchatroom.create  :name => 'General',
      :description => 'Open topic chatroom',
      :minlevel => 1,
      :enabled => true,
      :created => Time.now
  Ionio.printreturn(0)
end

#============================================================================
# Table: extprogs
#============================================================================
#if Ionextprogs.empty?
#  Ionio.printstart " inserting: Extprogs"
#  #Ionextprogs.create  :label => 'value'
#  Ionio.printreturn(0)
#end

#============================================================================
# Table: filearea
#============================================================================
if Ionfilearea.empty?
  Ionio.printstart " inserting: Ionfilearea"
    Ionfilearea.create  :name => 'dropbox',
      :description => 'Uploads (write-only)',
      :created => Time.now,
      :read => false,
      :write => true,
      :minlevel_read => 255,
      :minlevel_write=> 10,
      :path => '/opt/bbsfs/dropbox',
      :enabled => true

    Ionfilearea.create  :name => 'default',
      :description => 'Default File Area',
      :created => Time.now,
      :read => true,
      :write => false,
      :minlevel_read => 0,
      :minlevel_write=> 100,
      :path => '/opt/bbsfs/default',
      :enabled => true

  Ionio.printreturn(0)
end

#============================================================================
# Table: group
#============================================================================
if Group.empty?
  Ionio.printstart " inserting: Group"
    # => Create Group: SYSOPS
    Group.create  :name => 'SYSOPS',
      :sysopnote => 'This is the default all powerful Group',
      :level => '255',
      :dailytimelimit => 1440,
      :max_timedeposit => -1,
      :max_timewithdraw => 30,
      :max_downloads => -1,
      :max_downbytes => -1,
      :max_uploads => -1,
      :max_upbytes => -1,
      :max_posts => -1,
      :allow_login => true,
      :allow_msgareas => true,
      :allow_readpost => true,
      :allow_writepost => true,
      :allow_pagesysop => true,
      :allow_chatrooms => true,
      :allow_fileareas => true,
      :allow_download => true,
      :allow_upload => true,
      :allow_extprogs => true,
      :is_admin => true,
      :admin_system => true,
      :admin_announcements => false,
      :admin_files => true,
      :admin_msgs => true,
      :admin_users => true,
      :admin_groups => true,
      :admin_chat => true,
      :admin_extprogs => true,
      :created => Time.now

    # => Create Group: COSYSOPS
    Group.create  :name => 'COSYSOPS',
      :sysopnote => 'This is the default almost all powerful Group',
      :level => '200',
      :dailytimelimit => 1440,
      :max_timedeposit => -1,
      :max_timewithdraw => 30,
      :max_downloads => -1,
      :max_downbytes => -1,
      :max_uploads => -1,
      :max_upbytes => -1,
      :max_posts => -1,
      :allow_login => true,
      :allow_msgareas => true,
      :allow_readpost => true,
      :allow_writepost => true,
      :allow_pagesysop => true,
      :allow_chatrooms => true,
      :allow_fileareas => true,
      :allow_download => true,
      :allow_upload => true,
      :allow_extprogs => true,
      :is_admin => true,            
      :admin_system => false,
      :admin_announcements => false,
      :admin_files => true,
      :admin_msgs => true,
      :admin_users => true,
      :admin_groups => false,
      :admin_chat => true,
      :admin_extprogs => true,
      :created => Time.now

    # => Create Group: USERS
    Group.create  :name => 'USERS',
      :sysopnote => 'This is the default normal user account',
      :level => '100',
      :dailytimelimit => 60,
      :max_timedeposit => 30,
      :max_timewithdraw => 30,
      :max_downloads => 10,
      :max_downbytes => -1,
      :max_uploads => 3,
      :max_upbytes => -1,
      :max_posts => 10,
      :allow_login => true,
      :allow_msgareas => true,
      :allow_readpost => true,
      :allow_writepost => true,
      :allow_pagesysop => true,
      :allow_chatrooms => true,
      :allow_fileareas => true,
      :allow_download => true,
      :allow_upload => true,
      :allow_extprogs => true,
      :is_admin => false,
      :admin_system => false,
      :admin_announcements => false,
      :admin_files => false,
      :admin_msgs => false,
      :admin_users => false,
      :admin_groups => false,
      :admin_chat => false,
      :admin_extprogs => false,
      :created => Time.now

    # => Create Group: LOCKED
    Group.create  :name => 'LOCKED',
      :sysopnote => 'User with absolutely NO privileges. Locked Out.',
      :level => '0',
      :dailytimelimit => 0,
      :max_timedeposit => 0,
      :max_timewithdraw => 30,
      :max_downloads => 0,
      :max_downbytes => 0,
      :max_uploads => 0,
      :max_upbytes => 0,
      :max_posts => 0,
      :allow_login => false,
      :allow_msgareas => false,
      :allow_readpost => false,
      :allow_writepost => false,
      :allow_pagesysop => false,
      :allow_chatrooms => false,
      :allow_fileareas => false,
      :allow_download => false,
      :allow_upload => false,
      :allow_extprogs => false,
      :is_admin => false,
      :admin_system => false,
      :admin_announcements => false,
      :admin_files => false,
      :admin_msgs => false,
      :admin_users => false,
      :admin_groups => false,
      :admin_chat => false,
      :admin_extprogs => false,
      :created => Time.now

  Ionio.printreturn(0)
end

#============================================================================
# Table: Msgarea
#============================================================================
if Ionmsgarea.empty?
  Ionio.printstart " inserting: Ionmsgarea"
    # GENERAL : A default message group
    #  Anyone can read, only members of "USERS" group (level 100) can write
    Ionmsgarea.create  :name           => 'GENERAL',
      :description    => 'General user discussion on this system.',
      :minlevel_read  => 1,
      :minlevel_write => 100,
      :enabled        => true,
      :created        => Time.now

    # IONFALLS : A default message group 
    #  Only members with group level >= 100 (USERS) can read and write
    Ionmsgarea.create  :name           => 'IONFALLS',
      :description    => 'IonFalls Software discussion.',
      :minlevel_read  => 100,
      :minlevel_write => 100,
      :enabled        => true,
      :created        => Time.now

    # SYSADMINS : A default message group
    #  Only members with group level >= 200 (COSYSOPS) can read or write
    Ionmsgarea.create  :name           => 'SYSADMINS',
      :description    => 'System Administrators Only',
      :minlevel_read  => 200,
      :minlevel_write => 200,
      :enabled        => true,
      :created        => Time.now

    # DISABLED : A default message group 
    #  set by default to "enabled == false"
    Ionmsgarea.create  :name => 'DISABLED',
      :description    => 'Sample area that should be hidden from view.',
      :minlevel_read  => 1,
      :minlevel_write => 255,
      :enabled        => false,
      :created        => Time.now
  Ionio.printreturn(0)
end

#============================================================================
# Table: session
#============================================================================
#if Session.empty?
#  Ionio.printstart " inserting: Session"
#    #Session.create  :label => 'value'
#  Ionio.printreturn(0)
#end

#============================================================================
# Table: user
#============================================================================

if User.empty?    
  Ionio.printstart " inserting: User"
    User.create  :login => 'SYSOP',
      :islocked=> false,
      :group_id => 1,
      :password => User.cryptpassword('changeme'),
      :firstname => 'System',
      :lastname => 'Operator',
      :address1 => '0 Ion Falls',
      :address2 => 'suite 1',
      :city => 'LeafScale',
      :state => 'CO',
      :country => 'USA',
      :phone => '000-000-0000',
      :gender => 'M',
      :bday => '01/01/1970',
      :pwexpires => Time.now + 90.days,
      :created => Time.now,
      :login_last => Time.now,
      :sysopnote => 'This is the all powerful admin account.',
      :login_failures => 0,
      :login_total => 0,
      :timebank => 0,
      :total_files_up => 0,
      :total_files_down => 0,
      :total_messages => 0,
      :pwhint_question => "What is the meaning of life?",
      :pwhint_answer => "42",
      :pref_fastlogin   => false,
      :pref_term_height  => 24,
      :pref_term_width   => 80,
      :pref_term_pager  => true,
      :pref_term_color  => true,
      :pref_show_menus  => true,
      :pref_editor      => 'ioneditor'

  Ionio.printreturn(0)
end


#============================================================================
# Table: Announcement
#============================================================================
if Ionannouncement.empty?
  Ionio.printstart " inserting: Ionannouncement"
    Ionannouncement.create  :title => 'A note from the IonFalls project.',
      :submitted_by => 'Chris Tusa',
      :priority => 0,
      :created => Time.now,
      :expires => false,
      :expiration => nil,
      :body => <<eol
This is IonFalls BBS software. This software is the result
of years of hard work and dedication by the project's developers.
We hope that you will enjoy the features and innovation of this product.
This is the default Announcement created during installation.

Information on using this software is always available on the main project
website: http://www.ionfalls.org  and on IRC  #ionfalls @ irc.FreeNode.net

Have fun! And be sure to stay updated on the latest releases, and changes.

Sincerely,
Chris Tusa - inet:  chris.tusa@ionfalls.org
eol
  Ionio.printreturn(0)
end
#'#fixes a display error in VS Code

#============================================================================
# DONE 
#============================================================================
Ionio.printstart "DB Preloading data"
Ionio.printreturn(0)

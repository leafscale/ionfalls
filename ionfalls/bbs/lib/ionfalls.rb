=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionfalls.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Main Library
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Ionfalls (Ionfalls)
Ionfalls is the main Ionfalls library responsible for initialization of sub libraries.
=end

module Ionfalls

# Load the constants definition
def Ionfalls.loadlib_ionconstants
  begin
    require 'lib/ionconstants'
  rescue LoadError
    puts "FATAL: cannot load library 'ionconstants'."
    exit 1
  end
end

# Load Library: Security
def Ionfalls.loadlib_security
  begin
    require 'lib/security'
  rescue LoadError
    puts "FATAL: cannot load library 'security'."
    exit 1
  end
end


# Load Library: Ionio
def Ionfalls.loadlib_ionio
  begin
    require 'lib/ionio'
    Ionio.mainbanner
  rescue LoadError
    puts "FATAL: cannot load library 'ionio'."
    exit 1
  end
end

# Load Library: Ionconfig
def Ionfalls.loadlib_ionconfig
  begin
    require 'lib/ionconfig'
  rescue LoadError
    puts "FATAL: cannot load library 'ionconfig'."
    exit 1
  end
end

# Loads configuration into a global $cfg variable
def Ionfalls.load_ionconfig
  begin
    Ionio.printstart "Loading Ionfalls configuration"
    $cfg = Ionconfig.load
    Ionio.printreturn(0)
  rescue Exception => e
    Ionio.printreturn(1)
    puts e
    exit 1
  end
end

# Load Library: Iondatabase
def Ionfalls.loadlib_iondatabase
  begin
    require 'lib/iondatabase'
  rescue LoadError
    puts "FATAL: cannot load library 'iondatabase'."
    exit 1
  end
end

# Connect to the user specified DB engine & load the Sequel models
def Ionfalls.load_databaseconn
  begin
    Ionio.printstart "Initializing database connection"
    $db = Iondatabase.connect($cfg['database'])
    Ionio.printreturn(0)
  rescue Exception => e
    Ionio.printreturn(1)
    puts e
    exit 1
  end
end

# Load Library: Iondatabase_models
def Ionfalls.loadlib_iondatabasemodels
  Ionio.printstart "Initializing Database Models"
  Ionio.printreturn(2)
  begin
    require 'lib/iondatabase_models'
  rescue LoadError
    puts "FATAL: cannot load library 'iondatabase_models'."
    exit 1
  rescue Sequel::DatabaseConnectionError => e
    puts "Unable to connect to database: #{e}"
    exit 1
  end
  Ionio.printstart "Database Models Initialized OK"
  Ionio.printreturn(0)
end

# Load Library: Iontemplate
def Ionfalls.loadlib_iontemplate
  begin
    Ionio.printstart "Loading Template Engine"
    require 'lib/iontemplate'
    Ionio.printreturn(0)
  rescue LoadError    
    puts "FATAL: cannot load library 'iontemplate'."
    Ionio.printreturn(1)
    exit 1
  end
end 

# Load Library: Ioncontroller
def Ionfalls.loadlib_ioncontroller
  begin
    Ionio.printstart "Loading Master Controller"
    require 'lib/ioncontroller'
    Ionio.printreturn(0)
  rescue LoadError
    puts "FATAL: cannot load library 'ioncontroller'."
    Ionio.printreturn(1)
    exit 1
  end
end


# Load the entire stack in the correct order. Use this method for anything that requires full init
def Ionfalls.init
  self.loadlib_ionconstants
  self.loadlib_ionio
  self.loadlib_ionconfig
  self.load_ionconfig
  self.loadlib_iondatabase
  self.load_databaseconn
  self.loadlib_iondatabasemodels
  self.loadlib_iontemplate
  self.loadlib_ioncontroller
end

# Defines normal goodbye
def Ionfalls.goodbye
  # TODO: Add askny question, and add any other items here.
  Iontemplate.display('logout_prompt_goodbye.ftl')
  askuser = Ionio::Input.inputyn
  if askuser == true
    # TODO: Print a template with session summary information
    #stats = Iontemplate::Template.parse_dataset($db[:announcements].order(:priority).all)
    #Iontemplate.display('logout_prompt_goodbye.ftl', {'stats' =>)  
    self.goodbye_fast
  end
end

# Defines fast goodbye
def Ionfalls.goodbye_fast
  puts "Goodbye!"
  # If we have reached this point, the main controller has exited.
  # Terminate the session
  if $session
    $callerid.time_logout = Time.now
    $callerid.save
    $session.delete
    $session = nil
  end
  exit 0
end  

# Who's online
def Ionfalls.who
  whodata = Iontemplate::Template.parse_modeldata(Session.select(:username, :current_area, :created, :expires).all)
  Iontemplate.display('who.ftl', {'who' => whodata})
end


# Load the help menu
def Ionfalls.help
  HelpController.new.menu
end



# Prints a message when a feature is currently not implemented
def Ionfalls.unimplemented
 puts "Ionfalls.unimplemented: Sorry! This feature is not available in this version."
end

end #/module Ionfalls

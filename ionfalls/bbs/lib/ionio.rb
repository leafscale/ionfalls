=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionio.rb
#     Version: 0.06
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Input Output Handlers
#
#-----------------------------------------------------------------------------
=end

require 'io/console'
require 'rubygems'
require 'lib/ionio/input'
require 'lib/ionio/output'
require 'lib/ionio/dates'
require 'lib/ionio/ionedit'

=begin rdoc               
= Ionio (I/O)
Ionio provides the common input/output routines for dealing with STDOUT and STDIN. This includes the ANSI color output methods, pauses, screenclear, etc.
=end


module Ionio
 
  
  # rb input using 'io/console' library
  # What this does is uses the RUBY built in of getch, which itself returns a string.
  # We need to convert this to an ASCII keycode, which is done by converting it to a
  # java string, then java character array, and getting the first value.
  def Ionio::rbinput
    return IO.console.getch.to_java.toCharArray[0]
  end

=begin  
  # Apache Commons IO Input
  def Ionio::acinput
    bci = ApacheCommonsIO::BoundedInputStream.new(JavaLang::System.in, 1)
    #aci = ApacheCommonsIO::AutoCloseInputStream.new(JavaLang::System.in)
    result = bci.read
    return result
  end
=end
  
=begin  
  # Lanterna Terminal input
  def Ionio::ltinput
    pressed = false
    while pressed == false do
      kp = $term.pollInput        # Get Keypress from terminal pollInput method
      if kp != nil
        keytype = kp.getKeyType  # Retrieve the keytype
        pressed = true
      end
    end
    case keytype.name
      when "Character"
        return kp.getCharacter
      when "Backspace"
        return 127
      when "Enter"
        return 13
    end
  end
=end  
  
  # calls one of the other input methods to be easily interchangeable for testing
  def Ionio::rawinput
   self.rbinput
  end
 
  # Print Output in a ANSI color block using Term::ANSIColor
  # (DEPRECATED)
  def Ionio::ansiprint(text, fgcolor=ANSI_WHITE, bgcolor=ANSI_ON_BLACK)
      print fgcolor+bgcolor+text+ANSI_RESET
  end #/ansiprint

  # Display the program's MAIN banner
  def Ionio.mainbanner
    ansiprint("IonFalls -  https://www.ionfalls.org", ANSI_BRIGHT_CYAN)
    ansiprint(" v", ANSI_CYAN)
    ansiprint(IONFALLS_VERSION, ANSI_BRIGHT_CYAN)
    ansiprint(" :: ", ANSI_BLUE)
    ansiprint("Copyright (c) 2008-2019, Chris Tusa\n", ANSI_BRIGHT_CYAN)
    ansiprint("All rights reserved.\n", ANSI_CYAN)
    ansiprint("Refer to the LICENSE file.\n\n", ANSI_DARK_GRAY)
  end #/def mainbanner


  # The desired effect is to clear the screen
  def Ionio::ansiclear
    puts %x{clear}
  end #/def ansiclear

  # Print an ANSI graphic file (legacy - see Iontemplate)
  def Ionio.printansifile(filename, paging=true, numlines=25)
    # =>TODO: Add Paging feature to limit number of lines with more prompt
    
    if File.exist?(filename)
      File.open(filename, "r").each { |line| puts line }
    else
      return 1
    end
  end #/def printansifile

  # Print to stdout that an item is starting
  def Ionio.printstart(txt)
    ansiprint(txt, ANSI_WHITE)
    ansiprint("."*(60-txt.length), ANSI_GRAY)
  end #/def printstart

  # Print to stdout the result
  def Ionio.printreturn(retval)
    ansiprint("[", ANSI_BLUE)
    if retval == 0
      ansiprint("DONE", ANSI_BRIGHT_CYAN)
    elsif retval == 1
      ansiprint("FAIL", ANSI_BRIGHT_RED)
    elsif retval == 2
      ansiprint("WAIT", ANSI_CYAN)
    elsif retval == 3
      ansiprint("STRT", ANSI_BRIGHT_GREEN)
    else
      ansiprint("????", ANSI_BRIGHT_MAGENTA)
    end
    ansiprint("]\n", ANSI_BLUE)
  end #/def printreturn

  # Paging Function to incorporate scrolling into program  
  # adapted from code written by : Nathan Weizenbaum
  # http://nex-3.com/posts/73-git-style-automatic-paging-in-ruby
  # * WARN * Allowing execution of an external program is not good security
  # practices. Audit this for a better method.
  def Ionio.run_pager
    return unless STDOUT.tty?
    read, write = IO.pipe
    unless Kernel.fork # Child process
      STDOUT.reopen(write)
      STDERR.reopen(write) if STDERR.tty?
      read.close
      write.close
      return
    end
    # Parent process, become pager
    STDIN.reopen(read)
    read.close
    write.close
    ENV['LESS'] = '-FSRX' # Don't page if the input is short enough
    ENV['LESSSECURE'] = '1' # Turn on secure less (see man page)
    Kernel.select [STDIN] # Wait until we have input before we start the pager
    pager = ENV['PAGER'] || 'less'
    exec pager rescue exec "/bin/sh", "-c", pager
  end #/def run_pager

  # Create a terminal
  def Ionio.createterminal
    return JLine::TerminalBuilder.builder().system(true).signalHandler(JLine::Terminal::SignalHandler.SIG_IGN).build()
#    $term = LanternaTerminal::UnixTerminal.new( \
#              JavaLang::System.in,\
#              JavaLang::System.out,\
#              JavaCharset::Charset.forName("UTF-8"),\
#              LanternaTerminal::UnixLikeTerminal::CtrlCBehaviour::TRAP)
  end


  # Terminal detection to determine support, dimensions, etc.
  # TODO: Code audit - remove checks for terminfo.support and terminfo.ansi
  def Ionio::terminaldetect
    terminfo = {
        :support => true,  # forcing true until we find a better way
        :ansi => true,     # forcing true until we find a better way
        :height => IO.console.winsize.first,
        :width => IO.console.winsize.last,
        :echo => IO.console.echo?
    }
    return terminfo
  end

  # Asks user a question. Wraps display of a template and inputform.
  def Ionio::question(template, inputsize, type='text')
    t = Iontemplate::Template.new(template)
    t.render()
    if inputsize > 1
      answer = Ionio::Input.inputform(inputsize, type)
    elsif inputsize == 1
      answer = Ionio::Input.inputkey
    end    
    return answer
  end

  # Displays menu from template and prompts user for inputkey.
  def Ionio::menu(template)
    t = Iontemplate::Template.new(template)
    t.render()
    selection = Ionio::Input.inputkey
    return selection
  end #/menu

  # Pauses for key press to continue
  def Ionio::pause
      Iontemplate::display('ionio_pause.ftl')
      Ionio.rawinput      
    return nil # We dont care about the result, so discard
  end #/pause
  

end #/module

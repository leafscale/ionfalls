=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionio/dates.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Input Output for Dates
# TODO: * Date format is adjustable by config
# TODO: * Introduce Validation of Dates into this library
#
#-----------------------------------------------------------------------------
=end


module Ionio
  module Dates
require 'date'

# Input a year
def Dates::inputyear
  char = nil
  enterkey = false
  input = []
  cursize = 0
  maxsize = 4
  vals = (48..57)
  print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+"YYYY")
  print "\b" * 4
  until cursize == maxsize
    # Use the ruby io/console library
    char = Ionio.rawinput

    if vals.include?(char)
      input.push(char.chr)
      print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
      cursize +=1
    else
      print "\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"
    end #/if
  end #/until
  print ANSI_RESET
  value = ""
  input.each do |c|
    value.concat(c)
  end
  return value
end #/def inputyear

# Input a month
def Dates::inputmonth
  char = nil
  enterkey = false
  input = []
  cursize = 0
  maxsize = 2
  vals1 = (48..49) # Month first digit is either 0 or 1
  vals2 = (48..57) # Month second digit is 0 thru 9
  print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+"MM")
  print "\b" * 2
  # TODO - Ensure val2 does not exceed 2 if val1 = 1
  until cursize == maxsize
    # Uses the ruby io/console library
    char = Ionio.rawinput
    case cursize
      when 0
        if vals1.include?(char)
          input.push(char.chr)
          cursize +=1
          print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
        end #/if
      when 1
        if vals2.include?(char)
          input.push(char.chr)
          cursize +=1
          print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
        end #/if
    end
  end #/until
  print ANSI_RESET
  value = ""
  input.each do |c|
    value.concat(c)
  end
  return value
end #/def inputmonth

# Input a day
def Dates::inputday
  char = nil
  enterkey = false
  input = []
  cursize = 0
  maxsize = 2
  vals1 = (48..51) # Month first digit is either 0 or 1
  vals2 = (48..57) # Month second digit is 0 thru 9
  print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+"MM")
  print "\b" * 2
  # TODO - Ensure val2 does not exceed 2 if val1 = 1
  until cursize == maxsize
    # Uses the ruby io/console library
    char = Ionio.rawinput
    case cursize
      when 0
        if vals1.include?(char)
          input.push(char.chr)
          cursize +=1
          print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
        end #/if
      when 1
        if vals2.include?(char)
          input.push(char.chr)
          cursize +=1
          print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
        end #/if
    end
  end #/until
  print ANSI_RESET
  value = ""
  input.each do |c|
    value.concat(c)
  end
  return value
end #/def inputday


# Inputs a date
def Dates::inputdate
  #TODO: BUGFIX REQUIRED - Fix issue with backspacing during date input causing invalid entry.
  trap("INT") do
    # ignore "CTRL-C"
  end
  print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+"MM/DD/YYYY")
  print "\b" * 10
  month = self.inputmonth
  print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+"/"
  day   = self.inputday
  print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+"/"
  year  = self.inputyear
  print ANSI_RESET
  print "\n"
  value = Date.parse("#{year}-#{month}-#{day}")
  return value
end #/def inputdate


# A method to quickly get an expiration date 
def Dates::expire
  # Display a template that asks :
  # 1. (7) days, 2. (30) days, 3. (90) days, 4. (Custom Date)
  ds = nil
  validkeys=['1','2','3','4']
  until ds != nil
    key = Ionio::Input.menuprompt('ionio_expire.ftl',validkeys, nil) 
    print "\n"
    case key
      when "1" # 7 days
        ds= Date.today + 7
      when "2" # 30 days
        ds = Date.today + 30
      when "3" # 90 days
        ds = Date.today + 90
      when "4" # ask for a specific date
        ds = Ionio::Dates::inputdate
    end #/case
  end #/until
  return ds
end #/def expire

  end #/module Dates
end #/module Ionio

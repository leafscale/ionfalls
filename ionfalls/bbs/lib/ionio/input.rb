=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionio/input.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Input Handler Library
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Input
Input provides routines to handle retrieving input from consoles.
=end

require 'io/console'

module Ionio
  module Input

    # Displays a colored input bar of length 2 and returns a value.
    # Works similar to inputform, except does not require the [ENTER] key.
    # Once user enters a valid value, returns immediately. If the / key
    # is entered as the first value, a second entry wil be permitted
    # but will return on the 2nd keypress.
    # TODO: fix buf with 'space' bar
    def Input::inputkey
      # ignore "CTRL-C"
      trap("INT") do
      end
      # initialize
      char = nil # Ensure char is set to nil
      done = false # Initialize the condition
      input = [] # Initialize the array of input chars
      cursize = 0 # Start the counter
      maxlength = 2 # Define the max length of the input box.
      print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" "*(maxlength-1))
      print "\b" * (maxlength-1)
      # Start loop until enter key is pressed
      until done == true
        print ANSI_RESET

        # Uses the ruby io/console library
        char = Ionio.rawinput.to_i

        # handle [ENTER] key
        # If enter is pressed, then no value is returned.
        # The calling function will get an empty string or nil value.
        if char == 13 # [enter]
          input = [] # since nothing happens, turn input to an empty string
          #done = true
        elsif char == 127 # [backspace]
          #print "\b\b"+ANSI_BRIGHT_CYAN+ANSI_ON_BLUE+"   \b\b\b\b \b"
          #print "\b\b"+ANSI_RESET+"  \b\b\b"
          #print "\b\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"
          #input.pop                        #remove last type value
          #cursize -=1 unless cursize <= 0  #decrement cursize counter
          # handle everything else
        elsif char == 32 # [space]
          #print "\b\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b\b"
        elsif char == 92 || char == 27 # [backslash] '\' or [esc]
          char = nil
        else
          if cursize < maxlength
            if char == 47 # the '/' key
              print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr.upcase
              cursize +=1
            else
              print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr.upcase
              cursize +=2
              done = true
            end
            input.push(char.chr) #add new value
          else
            # if the size of the current input is > length, then drop the char
            print "\b \b"
            char = nil
          end
        end
        print ANSI_RESET
      end #/until
      # convert input array of character codes back into string
      value = ""
      input.each do |c|
        value.concat(c)
      end
      return value
    end

    #/def inputkey

    # Same as 'inputkey' method but for defaultpager
    def Input::pagerkey_default
      # ignore "CTRL-C"
      trap("INT") do
      end
      # initialize
      char = nil # Ensure char is set to nil
      done = false # Initialize the condition
      input = [] # Initialize the array of input chars
      cursize = 0 # Start the counter
      maxlength = 1 # Define the max length of the input box.
      print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" "*(maxlength))
      print "\b" * (maxlength)
      # Start loop until enter key is pressed
      until done == true
        print ANSI_RESET
        char = Ionio.rawinput.to_i

        #
        # handle [ENTER] key
        # If enter is pressed, then no value is returned.
        # The calling function will get an empty string or nil value.
        if char == 13 # [enter]
          input = [] # since nothing happens, turn input to an empty string
          done = true
          return 'Y'
        elsif char == 127 # [backspace]
          #print "\b\b"+ANSI_BRIGHT_CYAN+ANSI_ON_BLUE+"   \b\b\b\b \b"
          print "\b\b"+ANSI_RESET+"  \b\b\b"
          print "\b\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"
          #input.pop                        #remove last type value
          #cursize -=1 unless cursize <= 0  #decrement cursize counter
          # handle everything else
        elsif char == 32 # [space]
          print "\b\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b\b"
        elsif char == 92 || char == 27 # [backslash] '\' or [esc]
          char = nil
        else
          if cursize < maxlength
            print "\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
            input.push(char.chr) #add new value
            cursize +=2
            done = true

          else
            # if the size of the current input is > length, then drop the char
            print "\b \b"
            char = nil
          end
        end
        print ANSI_RESET
      end #/until
      # convert input array of character codes back into string
      value = ""
      input.each do |c|
        value.concat(c)
      end
      return value
    end

    #/def pagerkey

    # Displays a colored input bar of specified length and returns a value. This
    # may sound simple, but in order to provide more precise input control,
    # the input is read character by character into an array. The array is
    # a set of numbers which represent the character code. Before returning
    # the value, it is converted to a string. (replaces ansigets)
    #   length - specify length of the variable and input form
    #   type   - type is the data type that will allow the input to prevent
    #            certain keys that are not allowed for the type. valid types:
    #            - posint (Positive integer 0..9)
    #            - int    (Allows a single minus sign at the start followed by posint)
    #            - text - default type, no restrictions
    #
    def Input::inputform(length=0, formtype='text')
      trap("INT") do        # ignore "CTRL-C"
      end

      if length < 1        # If the length is not a positive integer, return a nil value.
        return nil         # TODO: Should this condition raise an error?
      else
      char = nil           # ensure char is set to nil
      enterkey = false     # initialize enterkey as not pressed
      input = []           # initialize input as empty array
      cursize = 0          # set the current input buffer count to 0
      numberkeys = 48..57  # define the numeric values keycope range for formtype

      # ready the form input box
      # TODO: probably want to make this use a template in the future
      print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" "*length)
      print "\b" * length

      # Start loop until enter key is pressed
      until enterkey == true
        char = nil # reset char during each loop iteration
        char = Ionio.rawinput.to_i  # Uses the ruby io/console library
        case char
          when 13  # [enter]
            enterkey = true
          when 127 # [backspace]
            unless cursize == 0
              print "\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"
              input.pop                       #remove last type value
              cursize -=1 unless cursize <= 0 #decrement cursize counter
            end #/unless
        else
          if cursize < length
            case formtype
            when 'text'
              # TODO: Limit input to alphanumeric and basic symbols, trap other codes
              inputvalid = true
            when 'posint' # Only positive integers
              if numberkeys.member?(char)
                inputvalid = true
              else
                inputvalid = false
              end
            when 'int' # Allow a minus sign once at the start
              if cursize < 1
                if char == 45 or numberkeys.member?(char)
                  inputvalid = true
                end #/char
              else
                if numberkeys.member?(char)
                  inputvalid = true
                else
                  inputvalid = false
                end #/numberkeys
              end #/cursize              
            end
            if inputvalid == true
              print ""+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
              input.push(char.chr) #add new value
              cursize += 1 #increment cursize counter
            end
          end #/if cursize < length
        end #/case
      end #/until enterkey

      print ANSI_RESET+"\n"  # Reset the colors before output
      # convert input array of character codes back into string
      #print ANSI_ON_BLUE+(" "*input.size)+ANSI_RESET+"\n"
      value = ""          # empty the value string
      input.each do |c|   # iterate through the character array
        value.concat(c)   # reassemble the string
      end
      return value
    end #/end if length < 1

    end #/def inputform


    # Login Prompt
    def Input::loginform
      trap("INT") do
        # ignore "CTRL-C"
        Ionfalls.goodbye
      end
      char = nil
      enterkey = false
      input = []
      cursize = 0
      length = 16
      print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" "*length)
      print "\b" * length
      # initialize
      # Start loop until enter key is pressed
      until enterkey == true
        char = Ionio.rawinput.to_i

        # handle [ENTER] key
        if char == 13
          enterkey = true
        else
          # handle [Backspace] key
          if char == 127
            unless cursize == 0
              print "\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"
              input.pop #remove last type value
              cursize -=1 unless cursize <= 0 #decrement cursize counter
            end
            # handle everything else
          else
            if cursize < length
              print ""+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr.upcase
              input.push(char.chr) #add new value
              cursize += 1 #increment cursize counter
            else
              # if the size of the current input is > length, then drop the char
              #print "\b \b"
              char = nil
            end
          end
        end
        print ANSI_RESET
      end
      # convert input array of character codes back into string
      value = ""
      input.each do |c|
        value.concat(c)
      end
      print ANSI_RESET+"\n"
      return value.upcase
    end

    #/def loginform


    # Password Prompt character reader. Replaces input with % for output.
    def Input::passwordform
      trap("INT") do
        # ignore "CTRL-C"
      end
      char = nil
      enterkey = false
      input = []
      cursize = 0
      length = 25
      print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" "*length)
      print "\b" * length
      # initialize
      # Start loop until enter key is pressed
      until enterkey == true
        #print ANSI_RESET
        char = Ionio.rawinput.to_i

        # handle [ENTER] key
        if char == 13
          enterkey = true
        else
          # handle [Backspace] key
          if char == 127
            # TODO: bugfix - minor visual issue where additional 3 spaces
            # are added to end of a line when backspace occurs after a certain
            # point during the inputform.
            unless cursize == 0
              print "\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"
              input.pop #remove last type value
              cursize -=1 unless cursize <= 0 #decrement cursize counter
            end
            # handle everything else
          else
            if cursize < length
              print ""+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+"%"
              input.push(char.chr) #add new value
              cursize += 1 #increment cursize counter
            else
              # if the size of the current input is > length, then drop the char
              #print "\b \b"
              char = nil
            end
          end
        end
        print ANSI_RESET
      end
      # convert input array of character codes back into string
      value = ""
      input.each do |c|
        value.concat(c)
      end
      print "\n"
      return value
    end

    #/def passwordform


# Displays a colored input bar of length 1 and returns a boolean based
# on a yes/no question. Only valid characters are Y & N.
    def Input::inputyn(template=nil)
      # ignore "CTRL-C"
      trap("INT") do
      end

      # initialize
      char = nil # Ensure char is set to nil
      done = false # Initialize the condition
      # Start loop until enter key is pressed
      until done == true
        unless template.nil? == true
          Iontemplate::display(template)
        end
        char = self.inputkey.upcase
        unless char == 127
          case char.chr.upcase
            when "Y"
              value = true
              done = true
            when "N"
              value = false
              done = true
          end #/case
        end #/unless
      end #/until
      return value
    end  #/def inputyn

    
# Displays a colored input bar of length 1 and returns a boolean based
# on a yes/no question. Only valid characters are M & F.
    def Input::inputgender(template=nil)
      # ignore "CTRL-C"
      trap("INT") do
      end
      unless template.nil? == true
        t = Iontemplate::Template.new(template)
        t.render()
      end
      # initialize
      char = nil # Ensure char is set to nil
      done = false # Initialize the condition
      input = [] # Initialize the array of input chars
      cursize = 0 # Start the counter
      maxlength = 1 # Define the max length of the input box.
      print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" "*(maxlength))
      print "\b" * (maxlength)
      # Start loop until enter key is pressed
      until done == true
        print ANSI_RESET
        # Uses the ruby io/console library
        char = Ionio.rawinput.to_i
        unless char == 127
          case char.chr.upcase
            when "M"
              value = "M"
              done = true
            when "F"
              value = "F"
              done = true
            else
              #print "\b" * (maxlength)
              #print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"*(maxlength))
          end #/case
        end #/unless
      end #/until
      print ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr.upcase+ANSI_RESET+"\n"
      return value
    end
    #/def inpuionender


    # Menu Prompt: Recieve input restricted to a list of validkeys
    def Input::menuprompt(template, validkeys, menudata=nil)
      trap("INT") do
      end
      # Append globalkeys to supplied validkeys array. This is for DRY within each menu prompt.
      globalkeys = ['/?', '/F', '/G', '/H', '/M', '/P', '/T', '/W']
      validkeys = validkeys.concat(globalkeys)
      # Initialize loop condition
      done = false
      until done == true
        # Display menu template only if user preference is True
        if $session.nil? == false
          if $session.pref_show_menus == true
            Iontemplate::display(template, menudata)
          end
        elsif $session.nil? == true
          Iontemplate::display(template, menudata)
        end

        # Display the prompt
        Iontemplate::display('menu_prompt.ftl')
        selection = self.inputkey.upcase
        # Check if user supplied selection is included in validkeys array
        if validkeys.include?(selection)
          # Check for Global Input Values with prepended slash
          case selection
            when "/?" # Show the shortcut / commands menu
              Iontemplate::display('menu_shortcuts.ftl')
            when "/F" # Fast Goodybye
              Ionfalls::goodbye_fast
              return 0
            when "/G" # Goodbye from here
              Ionfalls::goodbye
            when "/H" # Help Menu - probably dont want this here
              Ionfalls::help
            when "/M" # Toggle $session.prefs_show_menus
              $session.pref_show_menus = $session.pref_show_menus.toggle
              $session.save
              Iontemplate::display('toggle_show_menus.ftl', {"togglevalue" => $session.pref_show_menus.to_s})
            when "/P" # Toggle $session.prefs_term_pager
              $session.pref_term_pager = $session.pref_term_pager.toggle
              $session.save
              Iontemplate::display('toggle_term_pager.ftl', {"togglevalue" => $session.pref_term_pager.to_s})
            when "/T" # Show time remaining
              Iontemplate::display('time_remain.ftl', {
                      'timeremain'=> $session.timeremain.to_s,
                      'curtime' => Time.now.to_s,
                      'expires' => $session.expires.to_s
              })
            when "/W"
              Ionfalls.who
            when "/X"
              puts "Fast Return to Main"
          end
          done = true
        else
          Iontemplate::display('menu_invalidkey.ftl')
        end
      end
      return selection
    end


    # Same as inputform except supports line wrapping for use in Editor
    def Input::inputeditline(length=79)
      trap("INT") do
        # ignore "CTRL-C"
      end
      char = nil
      enterkey = false
      input = []
      cursize = 0
      print ANSI_RESET+(ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" "*length)
      print "\b" * length
      # initialize
      # Start loop until enter key is pressed
      until enterkey == true
        #print ANSI_RESET
        # Uses the ruby io/console library
        char = Ionio.rawinput.to_i

        # handle [ENTER] key
        if char == 13  # 10 = Line Feed  13 = Carriage Return
           enterkey = true
          print "\n"
        else
          # handle [Backspace] key
          if char == 127
            unless cursize == 0
              print "\b"+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+" \b"
              input.pop #remove last type value
              cursize -=1 unless cursize <= 0 #decrement cursize counter
            end
            # handle everything else
          else
            if cursize < (length - 1)
              print ""+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
              input.push(char.chr) #add new value
              cursize += 1 #increment cursize counter
            else
              # if the size of the current input is >= length, then goto next line.
              print ""+ANSI_BRIGHT_WHITE+ANSI_ON_BLUE+char.chr
              input.push(char.chr)
              enterkey = true
              print ANSI_RESET+"\n"
              #char = nil
            end
          end
        end
        print ANSI_RESET
      end
      # convert input array of character codes back into string
      #print ANSI_ON_BLUE+(" "*input.size)+ANSI_RESET+"\n"
      value = ""
      input.each do |c|
        value.concat(c)
      end
      return value
    end #/def inputeditline

    # Input a positive integer and only return value IF it is in the specified range
    def Input::inputrange(templatename, rangename, extradata=nil)
      hashdata = { "range" => { "first" => rangename.first, "last" => rangename.last}  }
      if extradata != nil
        hashdata = hashdata.merge extradata
      end      
      Iontemplate.display(templatename, hashdata)
      length = rangename.last.to_s.size
      value = Ionio::Input.inputform(length,'int')
      if rangename.member?(value.to_i) == true
        return value.to_i
      else
        return nil
      end
    end

  end #/end module
end #/end module

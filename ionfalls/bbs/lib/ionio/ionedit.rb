=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
# See "LICENSE" file for distribution and copyright information. 
# 
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionio/ionedit.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: IonEdit - very basic line by line text editor.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Ionedit
An ANSI console editor for composing short, simple documents such as Ionfalls
Email or longer input fields. Editors defined by this module should all
follow the same basic principle of handling user I/O and returning a string
object as the body of the editor content to the calling function.
=end


module Ionio
  module Ionedit

    # Load the user's defined editor and returns content to the caller
    def Ionedit.edit(content=nil, subject=nil)
      # Test session first. If no session, then allow for local
      # admin and other tools.
      if $session.nil?
        whicheditor = 'ioneditor'
      else
        whicheditor = $session.pref_editor  
      end
      case whicheditor
         when 'ioneditor'
           editor = Ionio::Ionedit::IonEditor.new(content, subject)
           result = editor.start
           # If the editor returns a "1", it saved
           if result == 1
             return editor.parse_content
           else # otherwise return an empty body
             return nil
           end
           
         when 'ansiedit'
           return Ionio::Ionedit::Ansi.new.edit
         else
           # If editors do not load, fail and abort the post
           # TODO: Add a handler here for some output message
           return nil
       end
    end

    # IonFalls ANSI Editor (unfeatured line editor)
    class Ansi
      # Create a new instance of the editor
      def initialize
        @buffer = []  # Create an empty array
        @lines = 0    # Line counter starts at zero
      end

      # Displays a banner before starting the editor with information on usage, etc.
      def banner
        Iontemplate.display('ionedit_banner.ftl')        
      end

      # Input a row of text and add it to the buffer until exit is called.
      def inputrow
        skipline = false
        newline = Ionio::Input.inputeditline(79)
        if newline.length == 2
          case newline
            when "/A" # Abort
              Iontemplate.display('ionedit_abort.ftl')
              return 2
            when "/X" # Exit and Save
              return 1
            when "/H" # Help
              Iontemplate.display('ionedit_help.ftl')
              skipline = true
            when "/P" # Preview & Change
              preview
              skipline = true
          end #/case
        end #/if
        @buffer.push(newline) unless skipline == true
        return 0
      end #/def inputrow

      # Run Editor Session
      def edit
        loop = true
        banner
        while loop == true
          row = inputrow
          if row == 1
            loop = false
          elsif row == 2
            loop = false
            return nil
          end
        end #/while
        body = ""
        @buffer.each do |line|
          body = body + line + "\n"
        end
        return body
      end

      # Previews the lines, allowing the user to make a change before saving
      def preview
        linenum = 0
        puts "\n"
        @buffer.each do |line|
          linenum +=1
          puts "#{linenum.to_s}:#{line}"
        end
      end

    end #/class Editor

# IonEditor - JLine based Full Screen editor    
    class IonEditor

      def initialize(content=nil, subject=nil)
        if $session.nil?
          @filename = "/tmp/ionfalls-nosession-#{Time.now.to_i}"
        else
          @filename = "/tmp/ionfalls-#{$session.user_id}-#{$session.id}-#{Time.now.to_i}"
        end
        if content.nil? == true
          content = ""
        end
        # Create the temp file
        f = File.new(@filename, "w")
        f.puts(content)
        f.close
        @subject = subject
      end

      def start
        buffer = JavaIO::File.new(@filename)
        ionedit = JLine::IonEditor.new($term, buffer)
        ionedit.printLineNumbers=false
        ionedit.title="IonEdit :: #{@subject}"
        ionedit.open(buffer.toString)
        ionedit.run
      end

      # Parse the content of @filename and return it
      def parse_content
        body = ""
        if File.exists?(@filename)
          f = File.open("#{@filename}", "r")
            f.each_line do |line|
              body += line
            end #/do
          f.close
          File.delete(@filename)
          return body
        else
          Iontemplate.display('ionedit_abort.ftl')
          return nil
        end #/if
      end #/def

    end #/class IonEditor
    
    
=begin    
 # EXTERNAL NANO EDITOR    
    # Load GNU Nano restricted mode
    class Nano

      def initialize(content=nil)
        @filename = "/tmp/ionfalls-#{$session.user_id}-#{$session.id}-#{Time.now.to_i}"
        @nano_exec = $cfg['editors']['nano_exec']
        @nano_restrict = $cfg['editors']['nano_restrict']
        unless content.nil? == true
          f = File.new(@filename, "w")
            f.puts(content)
          f.close
        end
      end

      # Load filename using rnano

      def exec
         strptrs = []
         strptrs << FFI::MemoryPointer.from_string(@nano_exec) # Nano executable
         strptrs << FFI::MemoryPointer.from_string("-R") # Restricted Nano
         strptrs << FFI::MemoryPointer.from_string("-t")           # -t for use tmpfile (no save prompt)
         strptrs << FFI::MemoryPointer.from_string("#{@filename}") # generated tmp filename 
         strptrs << nil

         # Now load all the pointers into a native memory block
         argv = FFI::MemoryPointer.new(:pointer, strptrs.length)
         strptrs.each_with_index do |p, i|
          argv[i].put_pointer(0,  p)
         end
         if JExec.fork == 0
          JExec.execvp("#{@nano_exec}", argv)
         end
         Process.waitall
      end

      # Parse the content of @filename and return it
      def parse_content
        body = ""
        if File.exists?(@filename)
          f = File.open("#{@filename}", "r")
            f.each_line do |line|
              body += line
            end #/do
          f.close
          File.delete(@filename)
          return body
        else
          Iontemplate.display('ionedit_abort.ftl')
          return nil
        end #/if
      end #/def

    end #/class Nano
=end
 
  end #/module Ionedit

end #/module Ionio

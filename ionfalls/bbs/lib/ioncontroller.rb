=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ioncontroller.rb
#     Version: 1.00
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Master Controller.
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Ioncontroller (Main Controller)
Ioncontroller is the master application controller. Other Controllers inherit
from this SuperClass.
=end

# Main Controller Class
class Ioncontroller

  # This tests if the users' session is still good (such as: time expired)
  def test_session
    unless $session.is_valid?
      Ionfalls::goodbye_fast
    end
  end


end

#
# List inherited controllers here, order may matter:
#
require 'lib/controllers/main'
require 'lib/controllers/callhistory'
require 'lib/controllers/chat'
require 'lib/controllers/chatroom'
require 'lib/controllers/file'
require 'lib/controllers/filearea'
require 'lib/controllers/message'
require 'lib/controllers/messagearea'
require 'lib/controllers/timebank'
require 'lib/controllers/user'
require 'lib/controllers/help'
require 'lib/controllers/bbslist'
require 'lib/controllers/announcement'

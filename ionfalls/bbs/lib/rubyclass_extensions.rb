=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/rubyclass_extensions.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Extends built-in Ruby Classes to enhance functionality. 
#
#-----------------------------------------------------------------------------
=end

=begin rdoc
= Ruby:: Class Extensions
This library extends built in Ruby Classes to add additional features.
=end


#---[ Java JDK ]--------------------------------------------------------------
#
# Java Class Mix-Ins
#
require 'java'

# Imports java.lang as Ruby::JavaLang
module JavaLang
    include_package "java.lang"
end

# Imports java.io as Ruby::JavaIO
module JavaIO
    include_package "java.io"
end

# Imports java.util as Ruby::JavaUtil
module JavaUtil
    include_package "java.util"
end

module JavaCharset
 include_package "java.nio.charset"
end
#-----------------------------------------------------------------------------



#---[ JAR: Apache Commons-LANG ]----------------------------------------------
#
# Import the ApacheCommonsLang Library
#
require 'class/commons-lang3-3.7.jar'
module ApacheCommonsLang
  include_package "org.apache.commons.lang3"
end
#-----------------------------------------------------------------------------


=begin
#---[ JAR: Apache Commons-IO ]------------------------------------------------
#
# Import ApacheCommonsIO
#
require 'class/commons-io-2.6.jar'
module ApacheCommonsIO
  include_package "org.apache.commons.io"
  include_package "org.apache.commons.io.input"  
end

module ApacheCommonsInput
  include_package "org.apache.commons.io.input"
end
#-----------------------------------------------------------------------------
=end


#---[ JAR: Apache Commons-Validator ]-----------------------------------------
require 'class/commons-validator-1.6.jar'
module ApacheCommonsValidator
  include_package "org.apache.commons.validator"    
end
#-----------------------------------------------------------------------------



#---[ JAR: Jasypt ]-----------------------------------------------------------
#
# Imports the JaSypt encryption library
#
require 'class/jasypt-1.9.2.jar'
module JasyptPBE
  include_package "org.jasypt.encryption.pbe"
end

module JasyptSalt
  include_package "org.jasypt.salt"
end

module JasyptText
  include_package "org.jasypt.util.text"
end
#-----------------------------------------------------------------------------



#---[ JAR: JLine3 ]-----------------------------------------------------------
#
# Imports the JLine3 library
#
require 'class/jline-3.9.1-SNAPSHOT.jar'
module JLine
  include_package 'org.jline.builtins'
  include_package 'org.jline.terminal'
  include_package 'org.jline.utils'
end
#-----------------------------------------------------------------------------



#---[ JAR: Lanterna ]---------------------------------------------------------
#
# Imports the Lanterna Terminal UI Library
#
=begin
require 'class/lanterna-3.0.1.jar'
module Lanterna
 include_package "com.googlecode.lanterna"
end
module LanternaInput
 include_package "com.googlecode.lanterna.input"
end
module LanternaTerminal
 include_package "com.googlecode.lanterna.terminal"
 include_package "com.googlecode.lanterna.terminal.ansi"
end
=end
#-----------------------------------------------------------------------------



#---[ Ruby: Integer ]---------------------------------------------------------
#
# Extends fixunum class for some date/time shortcuts.
#
class Integer
  # Default fixnum class is in seconds (self).
  def seconds
    self
  end

  # Convert number of minutes to seconds (self).
  def minutes
    self * 60
  end

  # Convert number of hours to seconds (self).
  def hours
    self * 60 * 60
  end
  # Alias for hours when using a single hour grammar
  def hour
    self * 60 * 60
  end

  # Convert number of days to seconds (self)
  def days
    self * 60 * 60 * 24
  end
  # Convert Seconds to Minutes
  def sec_to_min
    self / 60
  end
end #/class Fixnum
#-----------------------------------------------------------------------------



#---[ Ruby: String ]----------------------------------------------------------
#
# Extend the String Class with validation methods
#
class String
  # Return true if string contains only Alpha characters
  def is_alpha?
    ApacheCommonsLang::StringUtils.isAlpha(self)
  end

  # Returns true if string constains only Numeric characters
  def is_numeric?
    ApacheCommonsLang::StringUtils.isNumeric(self)
  end

  # Returns true if string is AlphaNumeric
  def is_alphanumeric?
    ApacheCommonsLang::StringUtils.isAlphanumeric(self)
  end

  # Return true if string contains only Alpha characters
  def is_spaced_alpha?
    ApacheCommonsLang::StringUtils.isAlphaSpace(self)
  end

  # Returns true if string constains only Numeric characters
  def is_spaced_numeric?
    ApacheCommonsLang::StringUtils.isNumericSpace(self)
  end

  # Returns true if string is AlphaNumeric
  def is_spaced_alphanumeric?
    ApacheCommonsLang::StringUtils.isAlphanumericSpace(self)
  end

  # Returns true if string is Blank. Similar to empty,
  # but looks for whitespace entries. 
  def is_blank?
    ApacheCommonsLang::StringUtils.isBlank(self)
  end

  # Returns true if string is NOT Blank.
  def is_notblank?
    ApacheCommonsLang::StringUtils.isNotBlank(self)
  end

  # Returns true if string meets email address criteria
  # Pure Ruby way, replaced with Apache Commons Validator
  #def is_email?
  #  if /^([0-9a-zA-Z]+[-._+&amp;])*[0-9a-zA-Z]+@([-0-9a-zA-Z]+[.])+[a-zA-Z]{2,6}$/.match(self)
  #    return true
  #  else
  #    return false
  #  end
  #end
  def is_email?    
    ApacheCommonsValidator::GenericValidator.isEmail(self)
  end

  # Returns true if string is a valid url
  def is_url?
    ApacheCommonsValidator::GenericValidator.isUrl(self)
  end

  # Returns true if string meets minimum length
  def minlength?(size)
    ApacheCommonsValidator::GenericValidator.minLength(self,size)
  end

  # Returns true if string meets maximum length
  def maxlength?(size)
    ApacheCommonsValidator::GenericValidator.maxLength(self,size)
  end

  # Returns true if string size is within bounds.
  def inbounds(min, max)
     ApacheCommonsValidator::GenericValidator.isInRange(self.length, min, max)
  end

end #/class String
#-----------------------------------------------------------------------------



#---[ Ruby: Boolean ]---------------------------------------------------------
#
# Extends TrueClass for Boolean Functions
#
class TrueClass
  # Flip the bit on a Boolean - True becomes False
  def toggle
    if self == true
      return false
    else
      return true
    end
  end
end

# Extends FalseClass for Boolean Functions
class FalseClass
  # Flip the bit on a Boolean - False becomes True
  def toggle
    if self == false
      return true
    else
      return false
    end
  end
end
#-----------------------------------------------------------------------------



#---[ Ruby: FFI ]-------------------------------------------------------------
#
# Load FFI Support
#
require 'ffi'

# LIBC exec call.
module JExec
    extend FFI::Library
    ffi_lib("c")
    attach_function :execvp, [:string, :pointer], :int
    attach_function :fork, [], :int
end
#-----------------------------------------------------------------------------
#
#EOF

=begin
#
#{*****************************************************************************}
#{*    .___                    ___________          .__    .__                *}  
#{*    |   |   ____     ____   \_   _____/ _____    |  |   |  |     ______    *}
#{*    |   |  /  _ \   /    \   |    __)   \__  \   |  |   |  |    /  ___/    *}
#{*    |   | (  <_> ) |   |  \  |     \     / __ \_ |  |__ |  |__  \___ \     *}
#{*    |___|  \____/  |___|  /  \___  /    (____  / |____/ |____/ /____  >    *}
#{*                        \/       \/          \/                     \/     *} 
#{*                                                                           *}
#{*   (C)opyright 2018 - Chris Tusa & the IonFalls project                    *}
#{*                                                                           *}
#{* This software is protected under the LeafScale Software License           *}
#{*                                                                           *}
#{*****************************************************************************}
#
#===============================================================================
#                 IonFalls    http://www.ionfalls.org                    
#===============================================================================
#
#---[ File Info ]-------------------------------------------------------------
#
# Source File: /lib/ionconfig.rb
#     Version: 0.01
#   Author(s): Chris Tusa <chris.tusa@ionfalls.org>
# Description: Configuration Handlers
#
#-----------------------------------------------------------------------------
=end

=begin rdoc               
= Ionconfig (Configuration)
Ionconfig handles configuration file management Configuration is stored in YAML syntax formatting.
=end


module Ionconfig
  require 'yaml'
  require 'rubygems'
  require 'bcrypt'
  require 'lib/security'

  Configfile = "./conf/ionfalls.conf.yaml"

  # Builds a default YAML configuration and outputs the filename specified.
  def Ionconfig.makedefault(filename)
    cfghash = {
            "bbs" => {
                    "name"    => "IonFalls BBS",
                    "tagline" => "a NEW install of IonFalls",
                    "theme"   => "opentg",
                    "enabled" => true
            },
            "database" => {
                    "host"    => "localhost",
                    "name"    => "opentg",
                    "user"    => "sa",
                    "pass"    => Security::ConfigPassword.new.encrypt("changeme")
            },
            "login" => {
                    "attempts"=> 5,
                    "lockout" => 8,
                    "usehint" => true,
                    "allownew" => true
            },
            "signup" => {
                    "default_group" => "USERS",
                    "ask_address" => false,
                    "ask_postal" => false,
                    "ask_country" => true,
                    "ask_phone" => false,
                    "ask_gender" => false,
                    "ask_bday" => false,
                    "ask_custom1" => false,
                    "custom1" => "How did you hear about this BBS? (75 chars max)",
                    "ask_custom2" => false,
                    "custom2" => "",
                    "ask_custom3" => false,
                    "custom3" => ""
            },
            "notify" => {
                    "notify_user" => true,
                    "user_name" => "SYSOP",
                    "notify_group" => true,
                    "group_name" => "SYSOPS",
                    "on_signup" => true,
                    "on_bbslist" => true,
                    "on_lockout" => true,
                    "on_upload" => true,                         
            },
            "feature" => {                    
                    "announcements" => true,
                    "bbslist" => true,
                    "timebank" => true      
            },
            "limits" => {
                    "timebank_max" => 300,
                    "files_upload_max" => 1024 
            },
            "editors" => {
                    "default_editor" => "ioneditor",
                    "allow_ansiedit" => false
            },
            "announcements" => {
                    "pause_between" => true,
                    "auto_purge" => true
            }

    }
    f = File.open(filename, "w")
    f.puts YAML::dump(cfghash)
    f.close
  end

  # Load in the YAML configuration file and returns
  def Ionconfig.load
    cfg = File.open(Configfile)  { |yf| YAML::load( yf ) }
    # => Ensure loaded data is a hash. ie: YAML load was OK
    if cfg.class != Hash
       raise "ERROR: Ionconfig - invalid format or parsing error."
    # => If all is well, perform deeper validation
    else
      # => PARSE & CHECK: Database Section
      # TODO: Need to expand the checks
      if cfg['database'].nil?
        raise "ERROR: Ionconfig - database section not defined."
      end #-> /Parse DB

      # => PARSE & CHECK: BBS Section
      if cfg['bbs'].nil?
        raise "ERROR: Ionconfig - bbs section not defined."
      else
        raise "ERROR: Ionconfig - bbs name field missing." if cfg['bbs']['name'].nil?
        raise "ERROR: Ionconfig - bbs tagline field missing." if cfg['bbs']['tagline'].nil?        
      end #-> /Parse BBS
    end #-> /if !Hash

    # => If all is well, return the configuration
    return cfg
  end #/def

  # Save changes to the YAML file with the has passed in.
  def Ionconfig.save(cfghash)    
    f = File.open(Configfile, "w")
    f.puts YAML::dump(cfghash)
    f.close
  end  
  
end # => /module
#EOL
